<?php

function wpv_shortcode_icon( $atts, $content = null ) {
	return wpv_get_icon_html( $atts );
}
add_shortcode( 'icon', 'wpv_shortcode_icon' );

function wpv_all_icons( $atts, $content = null ) {
	$icons = array_keys( wpv_get_icons_extended() );

	$output = '<div class="vamtam-icons-demo clearfix">'; foreach ( $icons as $i => $icon ) {
		$output .= '<div class="wpv-grid grid-1-3 lowres-width-override lowres-grid-1-2"><div class="wpvid-inner">'; $output .= do_shortcode( '<span class="wpvid-the-icon">[icon name="' . $icon . '" size="24"]</span><span class="wpvid-the-icon-name">' . $icon . '</span>' ); $output .= '</div></div>'; ;
	}
	$output .= '</div>'; return $output;
}
add_shortcode( 'all_vamtam_icons', 'wpv_all_icons' );
