<?php
// inti the plugin
add_action('admin_head', 'procdure_info_cat_inti');
function procdure_info_cat_inti() {
	$args=array(
	  'object_type' => array('procedureinfo') 
	); 

	$procdure_info_cat_taxonomies = get_taxonomies($args);
	if (is_array($procdure_info_cat_taxonomies)) {
	    foreach ($procdure_info_cat_taxonomies as $procdure_info_cat_taxonomy ) {
	        add_action($procdure_info_cat_taxonomy.'_add_form_fields', 'procdure_info_cat_add_texonomy_field');
			add_action($procdure_info_cat_taxonomy.'_edit_form_fields', 'procdure_info_cat_edit_texonomy_field');
	    }
	}
}

// add image field in add form
function procdure_info_cat_add_texonomy_field() {
wp_enqueue_style('thickbox');
wp_enqueue_script('thickbox');
	echo '<div class="form-field">
		<label for="taxonomy_image">Image</label>
		<input type="text" name="taxonomy_image" id="taxonomy_image" value="" />
	</div>'.procdure_info_cat_script();
}

// add image field in edit form
function procdure_info_cat_edit_texonomy_field($taxonomy) {
wp_enqueue_style('thickbox');
wp_enqueue_script('thickbox');
	echo '<tr class="form-field">
		<th scope="row" valign="top"><label for="taxonomy_image">Image</label></th>
		<td><input type="text" name="taxonomy_image" id="taxonomy_image" value="'.get_option('procdure_info_cat_taxonomy_image'.$taxonomy->term_id).'" /><br /></td>
	</tr>'.procdure_info_cat_script();
}
// upload using wordpress upload
function procdure_info_cat_script() {
	return '<script type="text/javascript">
	    jQuery(document).ready(function() {
			jQuery("#taxonomy_image").click(function() {
				tb_show("", "media-upload.php?type=image&amp;TB_iframe=true");
				return false;
			});
			window.send_to_editor = function(html) {
				imgurl = jQuery("img",html).attr("src");
				jQuery("#taxonomy_image").val(imgurl);
				tb_remove();
			}
	    });
	</script>';
}

// save our taxonomy image while edit or save term
add_action('edit_term','procdure_info_cat_save_taxonomy_image');
add_action('create_term','procdure_info_cat_save_taxonomy_image');
function procdure_info_cat_save_taxonomy_image($term_id) {
    if(isset($_POST['taxonomy_image']))
        update_option('procdure_info_cat_taxonomy_image'.$term_id, $_POST['taxonomy_image']);
}

// output taxonomy image url for the given term_id (NULL by default)
function procdure_info_cat_taxonomy_image_url($term_id = NULL) {
	if (!$term_id) {
		if (is_category())
			$term_id = get_query_var('cat');
		elseif (is_tax()) {
			$current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
			$term_id = $current_term->term_id;
		}
	}
    return get_option('procdure_info_cat_taxonomy_image'.$term_id);
}



// Add to admin_init function
add_filter("manage_edit-procedurecat_columns", 'procedure_info_columns_head');	
 
function procedure_info_columns_head($theme_columns) {
	$new_columns = array(
		'cb' => '<input type="checkbox" />',
		'name' => __('Name'),
		'category_icon' => __('Category Image'),
		'description' => __('Description'),
		'slug' => __('Slug'),
		'posts' => __('Posts'),
		);
	return $new_columns;
}

// Add to admin_init function	
add_filter("manage_procedurecat_custom_column", 'procedure_info_columns_content_taxonomy', 10, 3);
 
function procedure_info_columns_content_taxonomy($out, $column_name, $theme_id) {
	$theme = get_term($theme_id, 'procedurecat');
	switch ($column_name) {
		case 'category_icon':	
			// get header image url
			//$data = maybe_unserialize($theme->description);
			
			$cat_img_url = procdure_info_cat_taxonomy_image_url($theme->term_id);
			
			if($cat_img_url)			
				$out .= "<img src=\"{$cat_img_url}\" width=\"110\" />"; 
			else
				$out .= 'N/A';	
 			break;
 
		default:
			break;
	}
	return $out;	
}




?>