<?php
/*
  Plugin Name: Procedure Info
  Plugin URI: 
  Description: This plugin helps manage the procedure info list.
  Version: 0.1
  Author: Anas
  Author URI:
 */

// Define contants
define('PROCEDURE_VERSION', '1.0');
define('PROCEDURE_ROOT', dirname(__FILE__));
define('PROCEDURE_URL', plugins_url('/', __FILE__));



require PROCEDURE_ROOT . '/procedure_info_category_image.php';


class procedure_info {

    /** 
     * Holds the list of statuses available for the custom post type
     * Initialized in the contstructor
     * @access private
     * @var array
     */
    private $procedure_status = array();

    /**
     * Constructor 
     */
    public function __construct() {
     
        /* Init Custom Post and Taxonomy Types */
        add_action('init', array(&$this, 'register_procedure_info_custom_post')); 
		add_action( 'init', array( &$this, 'register_procedure_info_category' ) );	 
		
		
        /* Voucher Specifications */
        add_action('add_meta_boxes', array(&$this, 'procedureinfo_specifications_metaboxes'));
        add_action( 'save_post', array( &$this, 'procedureinfo_specifications_add_or_save' ), 10, 2 );			 
		
		
		//Customize post list
		add_filter( 'manage_procedureinfo_posts_columns', array( &$this, 'add_procedure_info_column_to_procedure_info_list' ));	
		add_action( 'manage_procedureinfo_posts_custom_column', array( &$this, 'show_procedure_info_column_for_procedure_info_list' ), 10, 2 );
		
		// Add support for Featured Images
		if (function_exists('add_theme_support')) {
			add_theme_support('post-thumbnails');
			add_image_size('procedure-thumb', 100, 70, true);
		}			
		
		
    }
	

    /**
     * Load default custom post type for fca management
     */
    public function register_procedure_info_custom_post() {
        $labels = array(
            'name' => _x('Procedure Info', 'procedure_info'),
            'singular_name' => _x('Procedure Info', 'procedure_info'),
            'name_admin_bar' => __('Procedure Info', 'procedure_info'),
            'add_new' => __('Add New', 'procedure_info'),
            'add_new_item' => __('Add New Procedure', 'procedure_info'),
            'edit_item' => __('Edit Procedure', 'procedure_info'),
            'new_item' => __('New Procedure', 'procedure_info'),
            'all_items' => __('All Procedures', 'procedure_info'),
            'view_item' => __('View Procedure', 'procedure_info'),
            'search_items' => __('Search Procedures', 'procedure_info'),
            'not_found' => __('No Procedure found', 'procedure_info'),
            'not_found_in_trash' => __('No Procedure found in Trash', 'procedure_info'),
            'parent_item_colon' => '',
            'menu_name' => __('Procedure Info', 'procedure_info'),
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'has_archive' => true,
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => null,
            'supports' => array('title', 'editor', 'thumbnail')
        );
        register_post_type('procedureinfo', $args);
    }
	
	
	

	/**
	 * Load default category for instructor management
	 */	
	function register_procedure_info_category()
	{
	  // Add new taxonomy, make it hierarchical (like categories)
	  $labels = array(
		'name' => _x( 'Procedure Category', 'procedure_info' ),
		'singular_name' => _x( 'Procedure Category', 'procedure_info' ),
		'search_items' =>  __( 'Search Procedure Categories' ),
		'popular_items' => __( 'Popular Procedure Categories' ),
		'all_items' => __( 'All Procedure Categories' ),
		'parent_item' => __( 'Parent Procedure Category' ),
		'parent_item_colon' => __( 'Parent Procedure Category:' ),
		'edit_item' => __( 'Edit Procedure Category' ),
		'update_item' => __( 'Update Procedure Category' ),
		'add_new_item' => __( 'Add New Procedure Category' ),
		'new_item_name' => __( 'New  Procedure Category Name' ),
	  );
	  
	  //register_taxonomy('procedurecat',array('procedureinfo' ,'beforeandafter'), array(
	  register_taxonomy('procedurecat',array('procedureinfo'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'procedurecat' ),
	  ));
	}	












    /**
     * @procedureinfo Specification Save
     * @param type $post_id
     * @param type $post
     * @return type 
     */
    public function procedureinfo_specifications_add_or_save($post_id, $post) {
        // verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times
        if (!isset($_POST['procedureinfo_specifications_noncename']) || !wp_verify_nonce($_POST['procedureinfo_specifications_noncename'], plugin_basename(__FILE__))) {
            return $post->ID;
        }

        // Check permissions
        if ('procedureinfo' == $_POST['post_type']) {
            if (!current_user_can('edit_pages', $post_id)) {
                return;
            }
        }
		
        if ($_POST['procedure_short_description']) {
            add_post_meta($post_id, 'procedure_short_description', $_POST['procedure_short_description'], TRUE) or update_post_meta($post_id, 'procedure_short_description', $_POST['procedure_short_description']);
        } else {
            delete_post_meta($post_id, 'procedure_short_description');
        }

		
        if ($_POST['procedure_length']) {
            add_post_meta($post_id, 'procedure_length', $_POST['procedure_length'], TRUE) or update_post_meta($post_id, 'procedure_length', $_POST['procedure_length']);
        } else {
            delete_post_meta($post_id, 'procedure_length');
        }
		
		
        if ($_POST['procedure_results']) {
            add_post_meta($post_id, 'procedure_results', $_POST['procedure_results'], TRUE) or update_post_meta($post_id, 'procedure_results', $_POST['procedure_results']);
        } else {
            delete_post_meta($post_id, 'procedure_results');
        }
		
		
        if ($_POST['procedure_recovery']) {
            add_post_meta($post_id, 'procedure_recovery', $_POST['procedure_recovery'], TRUE) or update_post_meta($post_id, 'procedure_recovery', $_POST['procedure_recovery']);
        } else {
            delete_post_meta($post_id, 'procedure_recovery');
        }
		
        if ($_POST['procedure_risks']) {
            add_post_meta($post_id, 'procedure_risks', $_POST['procedure_risks'], TRUE) or update_post_meta($post_id, 'procedure_risks', $_POST['procedure_risks']);
        } else {
            delete_post_meta($post_id, 'procedure_risks');
        }	
		
		if ($_POST['user_id']) {
            add_post_meta($post_id, 'user_id', $_POST['user_id'], TRUE) or update_post_meta($post_id, 'user_id', $_POST['user_id']);
        } else {
            delete_post_meta($post_id, 'user_id');
        }	
										   										
		
    }







    /**
     * procedureinfo Application note post meta box
     * @global type $post 
     */
    public function procedureinfo_specifications_post_box() {
        
        echo '<input type="hidden" name="procedureinfo_specifications_noncename" id="procedureinfo_specifications_noncename" value="' .
wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
        global $post;

        ?>
        
        <table style="width:100%;">
		
		<tr valign="top"><td><?php _e('Procedure short descriptions', 'procedure_info');?>:</td><td><textarea class="widefat" name="procedure_short_description" cols="20" rows="3"><?php echo get_post_meta($post->ID, 'procedure_short_description', true); ?></textarea></td></tr>	
		
		<tr><td><?php _e('Procedure Length (ca)', 'procedure_info');?>:</td><td><input type="text" name="procedure_length" value="<?php echo get_post_meta($post->ID, 'procedure_length', true); ?>" class="widefat" /></td></tr>
		
		<tr><td><?php _e('Results', 'procedure_info');?>:</td><td><input type="text" name="procedure_results" value="<?php echo get_post_meta($post->ID, 'procedure_results', true); ?>" class="widefat" /></td></tr>
		
		<tr valign="top"><td><?php _e('Recovery', 'procedure_info');?>:</td><td><textarea class="widefat" name="procedure_recovery" cols="20" rows="10"><?php echo get_post_meta($post->ID, 'procedure_recovery', true); ?></textarea></td></tr>	
		
		<tr valign="top"><td><?php _e('Risks', 'procedure_info');?>:</td><td><textarea class="widefat" name="procedure_risks" cols="20" rows="10"><?php echo get_post_meta($post->ID, 'procedure_risks', true); ?></textarea></td></tr>	
		
		<tr valign="top"><td><?php _e('ID', 'procedure_info');?>:</td><td><input type="text" name="user_id" value="<?php echo get_post_meta($post->ID, 'user_id', true); ?>" class="widefat"></td></tr>	
        </table>        
        
        <?php       
        
    }
	
	
	

    /**
     * Add Voucher specifications
     */
    public function procedureinfo_specifications_metaboxes() {
        add_meta_box('procedureinfo_specifications', __('Specifications', 'voucher_management'), array(&$this, 'procedureinfo_specifications_post_box'), 'procedureinfo', 'normal', 'high');
    }











	/**
	 * @add column to fca post list page
	 */
	
	function add_procedure_info_column_to_procedure_info_list( $posts_columns ) {
		global $post,$mode;
	
		if (!isset($posts_columns['status'])) {
			$new_posts_columns = $posts_columns;
		} else {
			$new_posts_columns = array();
			//$index = 0;
			foreach($posts_columns as $key => $posts_column) {
				if ($key=='status') {
				$new_posts_columns['procedure_info_image'] = null;
				}
				$new_posts_columns[$key] = $posts_column;
			}
		}
		
		$new_posts_columns['procedure_info_image'] = 'Procedure Info Image';
		
		unset($new_posts_columns['date']);
		
		return $new_posts_columns;
	}
	
	
	
	function show_procedure_info_column_for_procedure_info_list( $column_id, $post_id ) {
			global $post,$mode;

			if ( get_post_type($post) == 'procedureinfo') {
			switch ($column_id) {

			case 'procedure_info_image':
					if( function_exists('the_post_thumbnail') ) {
						echo the_post_thumbnail( 'procedure-thumb' );					
					}
				break;

			}
		}
	}	
	
	





   
    
}

// eof class
global $procedure_info;
$procedure_info = new procedure_info();

?>