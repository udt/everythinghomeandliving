<?php
//Remove Default User Role
remove_role( 'editor' );
remove_role( 'author' );
remove_role( 'contributor' );

//remove_role( 'member_bronze' );
//remove_role( 'member_silver' );
//remove_role( 'member_gold' );


//Add 3 type of members

add_role('member_free', 'Free', array(
    'read' => true, // True allows that capability
    //'edit_posts' => true,
    //'delete_posts' => false, // Use false to explicitly deny
));

add_role('member_bronze', 'Bronze', array(
    'read' => true, // True allows that capability
    //'edit_posts' => true,
    //'delete_posts' => false, // Use false to explicitly deny
));

add_role('member_silver', 'Silver', array(
    'read' => true, // True allows that capability
    //'edit_posts' => true,
   // 'delete_posts' => false, // Use false to explicitly deny
));

add_role('member_gold', 'Gold', array(
    'read' => true, // True allows that capability
    //'edit_posts' => true,
    //'delete_posts' => false, // Use false to explicitly deny
));





add_action( 'personal_options', 'mm_prefix_hide_personal_options' );
function mm_prefix_hide_personal_options() {
?>
<script type="text/javascript">
  jQuery(document).ready(function( $ ){
    $("#your-profile .form-table:first, #your-profile h3:first").remove();
  });
</script>
<?php
}





//Display custom Contact info in contact section in user profle
//echo get_user_meta(1, 'twitter', true); 
//

add_filter('user_contactmethods', 'mm_user_contactmethods'); 		   

function mm_user_contactmethods($user_contactmethods){  
  
  unset($user_contactmethods['yim']);  
  unset($user_contactmethods['aim']);  
  unset($user_contactmethods['jabber']);  
  
    
  $user_contactmethods['facebook'] = 'Facebook URL'; 
  $user_contactmethods['twitter'] = 'Twitter URL';
  $user_contactmethods['instagram'] = 'Instagram User ID';
  $user_contactmethods['telephonehome'] = 'Telephone Number(Home)';
  $user_contactmethods['telephoneoffice'] = 'Telephone Number(Office)';
  
  return $user_contactmethods;  
}  



//Add visual editor to author meta data
class MMVisualBiographyEditor {
	private $name = 'Visual Biography Editor';
	
	/**
	 * Setup WP hooks
	 */
	public function __construct() {
		// Add a visual editor if the current user is an Author role or above and WordPress is v3.3+
		if ( function_exists('wp_editor') ) {

			// Add the WP_Editor
			add_action( 'show_user_profile', array($this, 'visual_editor') );
			add_action( 'edit_user_profile', array($this, 'visual_editor') );
			
			// Don't sanitize the data for display in a textarea
			add_action( 'admin_init', array($this, 'save_filters') );

			// Load required JS
			add_action( 'admin_enqueue_scripts', array($this, 'load_javascript'), 10, 1 );
			
			// Add content filters to the output of the description
			add_filter( 'get_the_author_description', 'wptexturize' );
			add_filter( 'get_the_author_description', 'convert_chars' );
			add_filter( 'get_the_author_description', 'wpautop' );
		}
	}
	
	
	
	/**
	 *	Create Visual Editor
	 *
	 *	Add TinyMCE editor to replace the "Biographical Info" field in a user profile
	 *
	 * @uses wp_editor() http://codex.wordpress.org/Function_Reference/wp_editor
	 * @param $user An object with details about the current logged in user
	 */
	public function visual_editor( $user ) {
		
		// Contributor level user or higher required
		if ( !current_user_can('read') )
			return;
		?>
		<table class="form-table">
			<tr>
				<th><label for="description"><?php _e('Biographical Info'); ?></label></th>
				<td>
					<?php 
					$description = get_user_meta( $user->ID, 'description', true);
					wp_editor( $description, 'description' ); 
					?>
					<p class="description"><?php _e('Share a little biographical information to fill out your profile. This may be shown publicly.'); ?></p>
				</td>
			</tr>
		</table>
		<?php
	}
	
	/**
	 * Admin JS customizations to the footer
	 *
	 * @uses wp_enqueue_script() http://codex.wordpress.org/Function_Reference/wp_enqueue_script
	 * @uses plugin_dir_path() http://codex.wordpress.org/Function_Reference/plugin_dir_path
	 */
	public function load_javascript( $hook ) {
		
		// Contributor level user or higher required
		if ( !current_user_can('read') )
			return;
		
		// Load JavaScript only on the profile and user edit pages 
		if ( $hook == 'profile.php' || $hook == 'user-edit.php' ) {
			wp_enqueue_script(
				'visual-editor-biography', 
				plugins_url('js/visual-editor-biography.js', __FILE__), 
				array('jquery'), 
				false, 
				true
			);
		}
	}
	
	/**
	 * Remove textarea filters from description field
	 */
	public function save_filters() {
		
		// Contributor level user or higher required
		if ( !current_user_can('read') )
			return;
			
		remove_all_filters('pre_user_description');
	}
}


$mm_visual_editor_biography = new MMVisualBiographyEditor();

?>