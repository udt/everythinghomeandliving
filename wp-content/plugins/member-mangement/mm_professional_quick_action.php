<?php

/***
 *Quick edit option for user list page	
***/


function mm_user_list_quick_action_link( $actions, $user_object ) {
    if ( current_user_can( 'administrator', $user_object->ID ) ){
	
		if(mm_is_display_home($user_object->ID))
     		$actions['display_home'] = "<a href='users.php?mm_quick_action=save&mm_quick_action_type=home&value=0&paged=".$_GET['paged']."&user_id=".$user_object->ID."'>Home Page Feature(No)</a>";
		else
			$actions['display_home'] = "<a href='users.php?mm_quick_action=save&mm_quick_action_type=home&value=1&paged=".$_GET['paged']."&user_id=".$user_object->ID."'>Home Page Feature(Yes)</a>";
		
	
		if(mm_is_featured($user_object->ID))
			$actions['featured'] = "<a href='users.php?mm_quick_action=save&mm_quick_action_type=featured&value=0&paged=".$_GET['paged']."&user_id=".$user_object->ID."'>Deal Feature(No)</a>";
		else
			$actions['featured'] = "<a href='users.php?mm_quick_action=save&mm_quick_action_type=featured&value=1&paged=".$_GET['paged']."&user_id=".$user_object->ID."'>Deal Feature(Yes)</a>";	
		
	
		if(mm_is_professional($user_object->ID))
			$actions['status'] = "<a href='users.php?mm_quick_action=save&mm_quick_action_type=status&value=0&paged=".$_GET['paged']."&user_id=".$user_object->ID."'>Professional(No)</a>";
		else
			$actions['status'] = "<a href='users.php?mm_quick_action=save&mm_quick_action_type=status&value=1&paged=".$_GET['paged']."&user_id=".$user_object->ID."'>Professional(Yes)</a>";	
	
	
		
	}	
		
		
		
    return $actions;
}

//add_filter( 'user_row_actions', 'mm_user_list_quick_action_link', 10, 2 );






function mm_quick_edit_admin_init(){

	if($_GET['mm_quick_action'] == 'save'){
	
		$user_id = $_GET['user_id'];
		$value = $_GET['value'];
				
		if($_GET['mm_quick_action_type'] == 'home'){
			mm_update_professional_single_data($user_id, 'home_featured', $value);
		}


		if($_GET['mm_quick_action_type'] == 'featured'){
			mm_update_professional_single_data($user_id, 'feature', $value);
		}


		if($_GET['mm_quick_action_type'] == 'status'){
			mm_update_professional_single_data($user_id, 'status', $value);
		}
		
		
		if($_GET['mm_quick_action_type'] == 'procedure'){
			update_user_meta($user_id, 'procedure_flag', $value);
		}
		
		
		if($_GET['mm_quick_action_type'] == 'allpage'){
			update_user_meta($user_id, 'allpage_flag', $value);
		}

		
	}

}
add_action( 'admin_init', 'mm_quick_edit_admin_init' );

?>