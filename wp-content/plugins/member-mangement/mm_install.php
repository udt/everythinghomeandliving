<?php
    //run when plugin will be activated
    function mm_application_install(){
      global $wpdb;
    
      $professionals_table = $wpdb->base_prefix . "professionals";
    
        $structure_professionals_table = "CREATE TABLE IF NOT EXISTS $professionals_table (
          id BIGINT(20) NOT NULL AUTO_INCREMENT,
          user_id BIGINT(20) NOT NULL,
		  address varchar(255) NOT NULL,
		  address2 varchar(255) NOT NULL,
		  address3 varchar(255) NOT NULL,
		  city varchar(64) NOT NULL,
		  county varchar(64) NOT NULL,
		  state varchar(64) NOT NULL,
		  country varchar(64) NOT NULL,
		  postcode varchar(64) NOT NULL,
		  phone_home varchar(64) NOT NULL,
		  phone_office varchar(64) NOT NULL,
		  gender varchar(16) NOT NULL,
		  status varchar(8) NOT NULL,
		  amount varchar(64) NOT NULL,
		  post varchar(255) NOT NULL,
		  profession text NOT NULL,
		  cosmetic text NOT NULL,
		  body_area text NOT NULL,
		  procedure_treatment text NOT NULL,
		  region text NOT NULL,
		  ptype varchar(255) NOT NULL,
		  addtional_info text NOT NULL,
		  qualification varchar(255) NOT NULL,
		  online int(11) NOT NULL,
		  feature int(1) NOT NULL,
		  practice text NOT NULL,
		  home_featured int(1) NOT NULL,
		  professional_name varchar(128) NOT NULL,		  		  
          PRIMARY KEY (id)
        )";
      $wpdb->query($structure_professionals_table); 
    
    }


    //run when plugin will be de-activated
    function mm_application_uninstall(){
        global $wpdb;
        $professionals_table = $wpdb->base_prefix . "professionals";
        $structure_professionals_table = "DROP TABLE IF EXISTS $professionals_table"; 
        $wpdb->query($structure_professionals_table);       
    }
?>