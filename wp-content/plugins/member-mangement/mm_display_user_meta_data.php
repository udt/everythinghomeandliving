<?php
function mm_modify_user_table( $column ) {
	$column['display_name'] = 'Display Name';
	$column['home_f'] = 'Home';
	$column['deal_f'] = 'Deal';
	$column['prof_f'] = 'Professional';
	$column['procedure_f'] = 'Procedure';
	$column['all_page_f'] = 'All Pages';
	
	unset($column['name']);
 	unset($column['role']);
	unset($column['posts']);
	
    return $column;
}
 
add_filter( 'manage_users_columns', 'mm_modify_user_table' );
 
function mm_modify_user_table_row( $val, $column_name, $user_id ) {

	//should not show this setting if this is not a professionals
	if(!mm_user_exist($user_id)) return;
	
	
	$current_page_url = ecos_get_curPageURL();
	
	$params_user = array( 'mm_quick_action' => 'save');
	$new_url_for_flag = add_query_arg( $params_user, $current_page_url );	
	
	
	
	
	
	
    $user = get_userdata( $user_id );
 
    switch ($column_name) {
	
		case 'display_name' :

				return $user->display_name;			
			
			 break;
			
        case 'home_f' :            
			if(mm_is_display_home($user_id))
				$actions_display_home = "<a href='".$new_url_for_flag."&mm_quick_action_type=home&value=0&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/no.gif' alt='No' /></a>";
			else
				$actions_display_home = "<a href='".$new_url_for_flag."&mm_quick_action_type=home&value=1&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/yes.gif' alt='Yes' /></a>";
				
				return $actions_display_home;
						
            break;
			
        case 'deal_f' :
			if(mm_is_featured($user_id))
						$actions_featured = "<a href='".$new_url_for_flag."&mm_quick_action_type=featured&value=0&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/no.gif' alt='No' /></a>";
					else
						$actions_featured = "<a href='".$new_url_for_flag."&mm_quick_action_type=featured&value=1&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/yes.gif' alt='Yes' /></a>";
						
				return $actions_featured;
            break;
			
		case 'prof_f' :

			if(mm_is_professional($user_id))
				$actions_status = "<a href='".$new_url_for_flag."&mm_quick_action_type=status&value=0&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/no.gif' alt='No' /></a>";
			else
				$actions_status = "<a href='".$new_url_for_flag."&mm_quick_action_type=status&value=1&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/yes.gif' alt='Yes' /></a>";	
				
				return $actions_status;            
            break;
			
		case 'procedure_f':
		
			$procedure_flag = get_user_meta($user_id, 'procedure_flag', true);
			if($procedure_flag==0 || !$procedure_flag)
				$actions_procedure = "<a href='".$new_url_for_flag."&mm_quick_action_type=procedure&value=1&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/yes.gif' alt='Yes' /></a>";
				
			else
				$actions_procedure = "<a href='".$new_url_for_flag."&mm_quick_action_type=procedure&value=0&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/no.gif' alt='No' /></a>";
					
				
				return $actions_procedure;
			break;				



			case 'all_page_f':
			
				$all_page_flag = get_user_meta($user_id, 'allpage_flag', true);
				
				if($all_page_flag==0 || !$all_page_flag)
					$actions_all_page = "<a href='".$new_url_for_flag."&mm_quick_action_type=allpage&value=1&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/yes.gif' alt='Yes' /></a>";
				
			else
				
				$actions_all_page = "<a href='".$new_url_for_flag."&mm_quick_action_type=allpage&value=0&paged=".$_GET['paged']."&user_id=".$user_id."'><img src='". MM_URL ."images/no.gif' alt='No' /></a>";
				
				return $actions_all_page;
			break;								


 
        default:
    }
 
    return $return;
}
 
add_filter( 'manage_users_custom_column', 'mm_modify_user_table_row', 10, 3 );

?>