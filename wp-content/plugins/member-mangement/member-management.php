<?php
/**
Plugin Name: Member Management
Plugin URI: 
Description: This plugin will work on default wp user list data to add extra item to the users
Author: Md. Anas Bin Mukim
Version: 1.0
Author URI: 
*/

// Define contants
define('MM_VERSION', '1.0');
define('MM_ROOT', dirname(__FILE__));
define('MM_URL', plugins_url('/', __FILE__));

wp_register_script('chosen_plugin', get_stylesheet_directory_uri()."/js/chosen.jquery.min.js", array('jquery'),'1.1', true);
wp_enqueue_script('chosen_plugin');
wp_enqueue_style('chosen_stylesheet', get_stylesheet_directory_uri()."/css/chosen.min.css");

require MM_ROOT . '/mm_default_update.php';
require MM_ROOT . '/mm_add_user_meta_data.php';
require MM_ROOT . '/mm_display_user_meta_data.php';

//Add quick action to user/professional list page
require MM_ROOT . '/mm_professional_quick_action.php';

//add professional custom search
require MM_ROOT . '/mm_professional_custom_search.php';
add_action('admin_menu', 'mm_professional_customsearch_plug_page');

//add checkbox to posts table, to show post links in subscribers profiles
add_filter('manage_posts_columns', 'mm_send_link_column_header');
add_action('manage_posts_custom_column', 'mm_send_link_column');
add_action('add_meta_boxes', 'mm_add_send_link_meta');
add_action('save_post', 'mm_send_link_save_data');


add_action( 'delete_user', 'delete_pro_data_from_mm_table' );

require MM_ROOT . '/mm_functions.php';

// Register the activate and deactivate hooks
require MM_ROOT . '/mm_install.php';
register_activation_hook( __FILE__, 'mm_application_install');
//register_deactivation_hook( __FILE__, 'mm_application_uninstall');
?>
