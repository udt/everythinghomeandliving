<?php

add_action( 'show_user_profile', 'mm_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'mm_show_extra_profile_fields' );

function mm_show_extra_profile_fields( $user ) { ?>

	<h3>Profession Specification</h3>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#country, #county_uk").chosen();
			jQuery("#procedure_treatment").chosen({no_results_text: "Hit enter to add"});
			jQuery("#country").change(function(){
				if(jQuery(this).val() == 'GBR')
				{
					jQuery('#county').val("");
					jQuery('#state').val("").attr('disabled', 'disabled');
					jQuery('#county_non_uk').hide();
					jQuery('#county_for_uk').show();
				}
				else
				{
					jQuery('#county_uk').val("");
					jQuery('#state').removeAttr('disabled');
					jQuery('#county_for_uk').hide();
					jQuery('#county_non_uk').show();
				}
			});
			jQuery("#country").trigger('change');
		});
		function add_pro_service_field() {
			var index = parseInt(jQuery("#pro_service_index").val(), 10);
			jQuery("#professional_service_list tbody").append(''+
			'<tr>'+
				'<td><input class="service-field required-field" type="text" name="professional_service['+index+'][service]" value="" /></td>'+
				'<td><input class="service-field required-field" type="text" name="professional_service['+index+'][category]" value="" /></td>'+
				'<td><input class="price-field required-field" type="text" name="professional_service['+index+'][price]" value="" /></td>'+
				'<td><input type="button" onclick="jQuery(this).closest(\'tr\').remove();return false;" class="hide-form" value="X" /></td>'+
			'</tr>'+
			'');
			index++;
			jQuery("#pro_service_index").val(index);
			return index - 1;
		}
		function add_pro_service_list() {
			var rows, row, index;
			rows = jQuery("#add_multiple_services_content").val().split("\n")
			for(var i = 0; i < rows.length; i++)
			{
				index = add_pro_service_field();
				row = rows[i].split(';');
				if(row[0] !== undefined)
				{
					jQuery('.service-field[name="professional_service['+index+'][service]"]').val(row[0]);
				}
				if(row[1] !== undefined)
				{
					jQuery('.service-field[name="professional_service['+index+'][category]"]').val(row[1]);
				}
				if(row[2] !== undefined)
				{
					jQuery('.price-field[name="professional_service['+index+'][price]"]').val(row[2]);
				}
			}
			jQuery("#add_multiple_services_content").val('');
		}
	</script>
	<style type="text/css">
		#professional_services table td {padding: 0;}
		#professional_services .service-field {width: 300px;}
	 </style>
	<table class="form-table">

		<tr>
			<th><label for="profession">Profession(Required)</label></th>

			<td>
			<?php $profession = mm_get_the_professional_value( 'profession', $user->ID ); ?>
			<select id="profession" name="profession">
			<option value="none">Select One</option>
				<?php
				global $wpdb;
				$table_ec_profession = $wpdb->base_prefix . "ec_profession";
				
				$sql_1 = "select * FROM `".$table_ec_profession."` ORDER BY profession_id";	
				$result_1 = $wpdb->get_results($sql_1,ARRAY_A); 
				foreach($result_1 as $result){
					$professional_name = $result['profession_name'];
				?>
					
					
				<option <?php if( $profession == $professional_name ){ ?> selected="selected" <?php } ?> value="<?php echo $result['profession_name']; ?>"><?php echo $result['profession_name']; ?></option>
				<?php } ?>
			</select>			
				<br />			
				<span class="description">Please select your Profession. Othewise this user will as a normal user. Professional must have a profession.</span>
			</td>
		</tr>
		<tr>
			<th><label for="practice_clinic_salon">Practice/Clinic/Salon</label></th>

			<td>
				<input type="text" name="practice_clinic_salon" id="practice_clinic_salon" value="<?php echo esc_attr( mm_get_the_professional_value( 'practice', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Practice/Clinic/Salon.</span>
			</td>
		</tr>		
		<tr>
			<th><label for="procedure_treatment">Procedure Treatment<br />One by one new line</label></th>
			<td>	
			<select id="procedure_treatment" name="procedure_treatment[]" multiple="multiple" data-placeholder="Choose Treatments">
				<?php 
				$procedure_treatment = esc_attr( mm_get_the_professional_value( 'procedure_treatment', $user->ID ) ); 
				$selected = explode('}', $procedure_treatment);
				global $wpdb;
				$table_ec_category = $wpdb->base_prefix . 'ec_treatment_category';
				$table_ec_list = $wpdb->base_prefix . 'ec_treatment_list';

				$sql = "select l.treatment_cat_id, treatment_name, treatment_cat_name FROM `" . $table_ec_list . "` l LEFT JOIN `" . $table_ec_category . "` c ON c.treatment_cat_id = l.treatment_cat_id ORDER BY treatment_cat_name, treatment_name";
				$result = $wpdb->get_results($sql, ARRAY_A);
				$lastCategory = 0;
				foreach ($result as $item) {
					if($item['treatment_cat_id'] != $lastCategory)
					{
						if($lastCategory != 0)
						{
							echo '</optgroup>';
						}
						echo '<optgroup label="'.$item['treatment_cat_name'].'">';
						$lastCategory = $item['treatment_cat_id'];
					}

					echo '<option value="'.$item['treatment_name'].'"'.(in_array($item['treatment_name'], $selected) ? 'selected' : '').'>'.$item['treatment_name'].'</option>';
					if(in_array($item['treatment_name'], $selected))
					{
						unset($selected[array_search($item['treatment_name'], $selected)]);
					}
				}
				if(count($selected) > 0)
				{
					echo '</optgroup>';
					echo '<optgroup label="Other" data-other="1">';
				}
				foreach($selected as $custom)
				{
					echo '<option value="'.$custom.'" selected>'.$custom.'</option>';
				}
				if(count($result) > 0)
				{
					echo '</optgroup>';
				}
				?>
			</select>
				<br />
				<span class="description">Please enter your procedure treatment. Your procedure treatment will save to database by separating "}" sign</span>
			</td>
		</tr>
		<tr>
			<th><label for="procedure_treatment">Service List<br />Invalid rows will not be saved</label></th>
			<td>
				<div id="professional_services" class="treatments profile_details_form_class">
					<input type="button" onclick="add_pro_service_field();return false;" value="Add New Row" class="add-service-button pink-submit-button" />
					<input type="button" onclick="jQuery('#add_multiple_services').toggle();return false;" value="Add Multiple Rows At Once" class="add-service-button pink-submit-button" />
					<div id="add_multiple_services" style="display: none; margin-top: 3px;">
						<textarea id="add_multiple_services_content" placeholder="Add one service per line, separate service name, category and price with ; Example: My Service;Skin treatment;15.50" style="min-height: 150px;"></textarea>
						<input type="button" onclick="add_pro_service_list();return false;" style="display: block;" value="Add Services" class="add-service-button pink-submit-button" />
					</div>
					<table id="professional_service_list" style="width: 85%">
						<thead>
							<tr>
								<th>Treatment/Product Name (cannot be empty)</th>
								<th>Treatment/Product Category (cannot be empty)</th>
								<th width="160px;">Service price in &pound; (must be number)</th>
								<th width="32px;">Del</th>
							</tr>
						</thead>
						<tbody>
					<?php 
					$current_professional_id = mm_get_the_professional_value('id', $user->ID);
					$current_services = pro_get_professional_services($current_professional_id);
					$index = 0;
					if(is_array($current_services))
					{
						foreach ($current_services as $service)
						{
						?>
							<tr>
								<td><input class="service-field required-field" type="text" name="professional_service[<?php echo $index; ?>][service]" value="<?php echo $service['service']; ?>" /></td>
								<td><input class="service-field required-field" type="text" name="professional_service[<?php echo $index; ?>][category]" value="<?php echo $service['category']; ?>" /></td>
								<td><input class="price-field required-field" type="text" name="professional_service[<?php echo $index; ?>][price]" value="<?php echo $service['price']; ?>" /></td>
								<td><input type="button" onclick="jQuery(this).closest('tr').remove();return false;" class="hide-form" value="X" /></td>
							</tr>
						<?php
							$index++;
						}
					}
					?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									<input type="hidden" id="pro_service_index" value="<?php echo $index; ?>" />
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</td>
		</tr>

	</table>
	
	<h3>Address</h3>

	<table class="form-table">

		
		<tr>
			<th><label for="address">Address Line 1</label></th>

			<td>
				<input type="text" name="address" id="address" value="<?php echo esc_attr( mm_get_the_professional_value( 'address', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="address2">Address Line 2</label></th>

			<td>
				<input type="text" name="address2" id="address2" value="<?php echo esc_attr( mm_get_the_professional_value( 'address2', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="address3">Address Line 3</label></th>

			<td>
				<input type="text" name="address3" id="address3" value="<?php echo esc_attr( mm_get_the_professional_value( 'address3', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="town_city">Town/City</label></th>

			<td>
				<input type="text" name="town_city" id="town_city" value="<?php echo esc_attr( mm_get_the_professional_value( 'city', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr id="county_non_uk" style="display: none;">
			<th><label for="county">County</label></th>

			<td>
				<input type="text" name="county" id="county" value="<?php echo esc_attr( mm_get_the_professional_value( 'county', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr id="county_for_uk">
			<th><label for="county_uk">County</label></th>

			<td>
				<select name="county_uk" id="county_uk" style="width: 350px;">
					<option value="">- Select county -</option>
				<?php echo mm_generate_county_options(mm_get_the_professional_value( 'county', $user->ID )); ?>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="state">State</label></th>

			<td>
				<input type="text" name="state" id="state" value="<?php echo esc_attr( mm_get_the_professional_value( 'state', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="country">Country</label></th>

			<td>
				<select name="country" id="country">
					<option value="">- Select country -</option>
				<?php echo mm_generate_country_options(mm_get_the_professional_value( 'country', $user->ID )); ?>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="post_code">Post code</label></th>

			<td>
				<input type="text" name="post_code" id="post_code" value="<?php echo esc_attr( mm_get_the_professional_value( 'postcode', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>	
	
	
	
<?php } 


add_action( 'personal_options_update', 'mm_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'mm_save_extra_profile_fields' );

function mm_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'read', $user_id ) )
		return false;
	
	$telephonehome = $_POST['telephonehome'];
	$telephoneoffice = $_POST['telephoneoffice'];
	
	$profession = $_POST['profession'];
	$practice_clinic_salon = $_POST['practice_clinic_salon'];
	
	$procedure_treatment = isset($_POST['procedure_treatment']) && is_array($_POST['procedure_treatment']) ? $_POST['procedure_treatment'] : array();
	
	$procedure_treatment = implode('}', $procedure_treatment);
	
	$address = $_POST['address'];
	$address2 = $_POST['address2'];
	$address3 = $_POST['address3'];
	$town_city = $_POST['town_city'];
	$country = $_POST['country'];
	$county = $country == 'GBR' ? $_POST['county_uk'] : $_POST['county'];
	$state = $_POST['state'];
	$post_code = $_POST['post_code'];
	
	
	$user_info = get_userdata($user_id);
	$professional_name = $user_info->display_name;
	
	$current_professional_id = mm_get_the_professional_value('id', $user_id);
	pro_clear_professional_services($current_professional_id);
	if(isset($_POST['professional_service']) && is_array($_POST['professional_service']))
	{
		foreach($_POST['professional_service'] as $service)
		{
			if(isset($service["service"]) && isset($service["category"]) && isset($service["price"]) && $service["service"] != "" && $service["category"] != "")
			{
				pro_add_professional_service(array(
					"professional_id" => $current_professional_id,
					"service" => sanitize_text_field($service["service"]),
					"category" => sanitize_text_field($service["category"]),
					"price" => (float)$service["price"],
				));
			}
		}
	}
	
	//if professional then update or insert otherwise delete professional data if exist.
	if($profession == 'none'){
		//delete professional data from professional table.
		delete_pro_data_from_mm_table($user_id);		
	}else{
	
		if(mm_user_exist($user_id)){
			//update professionas
			mm_update_professional_data($user_id, $profession, $telephonehome, $telephoneoffice, $practice_clinic_salon, $procedure_treatment, $address, $address2, $address3, $town_city, $county, $state, $country, $post_code, $professional_name);
		}	
		else{
			//insert new professionals
			mm_insert_professional_data($user_id, $profession, $telephonehome, $telephoneoffice, $practice_clinic_salon, $procedure_treatment, $address, $address2, $address3, $town_city, $county, $state, $country, $post_code, $professional_name);
		}	
			
	}	
	//update_usermeta( $user_id, 'job', $_POST['job'] );
	

}


?>