<?php
/**
 * @check this new or need to update to existing user
 * @Param user id If data already exist return False and for new return true
 * @Return True or False
 **/

function mm_user_exist($user_id){
	global $wpdb;
	
	$result = FALSE;
	
	$table_professionals = $wpdb->base_prefix . "professionals";
	
	$pro_id = $wpdb->get_var($wpdb->prepare("SELECT COUNT(user_id) FROM " . $wpdb->base_prefix .  "professionals WHERE user_id = %d", $user_id ) );
	

	if($pro_id)
		$result = TRUE;
	else
		$result = FALSE;
		
	return $result;				

}

/**
 * @Insert new professional data
 * @Param all user/professional that is no related to user meta data
 * @Return void
 **/
function mm_insert_professional_data($user_id, $profession, $telephonehome, $telephoneoffice, $practice_clinic_salon, $procedure_treatment, $address, $address2, $address3, $town_city, $county, $state, $country, $post_code, $professional_name){
	global $wpdb;
	$table_professionals = $wpdb->base_prefix . "professionals";
	
	$profession = esc_sql($profession);
	$telephonehome = esc_sql($telephonehome);
	$telephoneoffice = esc_sql($telephoneoffice);
	$practice_clinic_salon = esc_sql($practice_clinic_salon);
	$procedure_treatment = esc_sql($procedure_treatment);
	$address = esc_sql($address);
	$address2 = esc_sql($address2);
	$address3 = esc_sql($address3);
	$town_city = esc_sql($town_city);
	$county = esc_sql($county);
	$state = esc_sql($state);
	$country = esc_sql($country);
	$post_code = esc_sql($post_code);
	$professional_name = esc_sql($professional_name);
	
	//$wpdb->query("INSERT INTO $table_professionals (user_id, profession, phone_home, phone_office, practice, procedure_treatment, address, city, state, postcode) VALUES ('$user_id', '$profession', '$telephonehome', '$telephoneoffice', '$practice_clinic_salon', '$procedure_treatment', '$address', '$town_city', '$state', '$post_code')");
	
	
	
 	//$wpdb->insert($table_professionals, array('user_id' => $user_id, 'profession' => $profession, 'phone_home' => $telephonehome, 'phone_office' => $telephoneoffice, 'practice' => $practice_clinic_salon, 'procedure_treatment' => $procedure_treatment, 'address' => $address, 'city' => $town_city, 'state' => $state, 'postcode' => $post_code));	
	
	
	$query = "INSERT INTO $table_professionals (user_id, profession, phone_home, phone_office, practice, procedure_treatment, address, address2, address3, city, county, state, country, postcode, professional_name) VALUES (%d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)";	
	if($wpdb->query($wpdb->prepare($query, $user_id, $profession, $telephonehome, $telephoneoffice, $practice_clinic_salon, $procedure_treatment, $address, $address2, $address3, $town_city, $county, $state, $country, $post_code, $professional_name)) === FALSE)
		return FALSE;

	
}



/**
 * @Update existing professional data
 * @Param all user/professional that is no related to user meta data
 * @Return void
 **/
function mm_update_professional_data($user_id, $profession, $telephonehome, $telephoneoffice, $practice_clinic_salon, $procedure_treatment, $address, $address2, $address3, $town_city, $county, $state, $country, $post_code, $professional_name){
	global $wpdb;
	$table_professionals = $wpdb->base_prefix . "professionals";
	
	$profession = esc_sql($profession);
	$telephonehome = esc_sql($telephonehome);
	$telephoneoffice = esc_sql($telephoneoffice);
	$practice_clinic_salon = esc_sql($practice_clinic_salon);
	$procedure_treatment = esc_sql($procedure_treatment);
	$address = esc_sql($address);
	$address2 = esc_sql($address2);
	$address3 = esc_sql($address3);
	$town_city = esc_sql($town_city);
	$county = esc_sql($county);
	$state = esc_sql($state);
	$country = esc_sql($country);
	$post_code = esc_sql($post_code);
	$professional_name = esc_sql($professional_name);	
	
	//$wpdb->query("UPDATE $table_professionals SET profession = '$profession', phone_home = '$telephonehome', phone_office = '$telephoneoffice', practice = '$practice_clinic_salon', procedure_treatment = '$procedure_treatment', address = '$address', city = '$town_city', state = '$state', postcode = '$post_code', professional_name = '$professional_name' WHERE user_id = '$user_id'");	
	
	$query = "UPDATE $table_professionals SET profession = %s, phone_home = %s, phone_office = %s, practice = %s, procedure_treatment = %s, address = %s, address2 = %s, address3 = %s, city = %s, county = %s, state = %s, country = %s, postcode = %s, professional_name = %s WHERE user_id = %d";	
	
	$wpdb->query($wpdb->prepare($query, $profession, $telephonehome, $telephoneoffice, $practice_clinic_salon, $procedure_treatment, $address, $address2, $address3, $town_city, $county, $state, $country, $post_code, $professional_name, $user_id));
}


function delete_pro_data_from_mm_table($user_id){
	global $wpdb;
	$table_professionals = $wpdb->base_prefix . "professionals";
	$professional_id = mm_get_the_professional_value('id', $user_id);
	pro_clear_professional_services($professional_id);
	$wpdb->query("DELETE FROM $table_professionals WHERE user_id = '$user_id'");

}

function mm_generate_country_options($currentCountry, $searchKey = 'country_name')
{
	global $wpdb;
	$options = '';
	$sql = "select country_name, country_code FROM ".$wpdb->base_prefix ."countries ORDER BY country_name";
	$result = $wpdb->get_results($sql, ARRAY_A);
	foreach ($result as $item) {
		$options.= '<option value="'.$item['country_code'].'"'.($item[$searchKey] == $currentCountry ? 'selected' : '').'>'.$item['country_name'].'</option>';
	}
		
	return $options;
}

function mm_generate_county_options($currentCounty, $searchKey = 'county_name')
{
	global $wpdb;
	$options = '';
	$sql = "select county_name, county_code FROM ".$wpdb->base_prefix ."counties ORDER BY county_name";
	$result = $wpdb->get_results($sql, ARRAY_A);
	foreach ($result as $item) {
		$options.= '<option value="'.$item['county_code'].'"'.($item[$searchKey] == $currentCounty ? 'selected' : '').'>'.$item['county_name'].'</option>';
	}
	
	return $options;
}

function mm_get_full_formatted_professional_address($user_id)
{
	$professional_address = array();
	$professional_address[] = mm_get_the_professional_value('address', $user_id);
	$professional_address[] = mm_get_the_professional_value('address2', $user_id);
	$professional_address[] = mm_get_the_professional_value('address3', $user_id);
	$professional_address[] = mm_get_the_professional_value('city', $user_id);
	$professional_address[] = mm_get_the_professional_value('county', $user_id);
	//$professional_address[] = mm_get_the_professional_value('state', $user_id);
	$professional_address[] = mm_get_the_professional_value('country', $user_id);

	$professional_address_out = '';
	$splitter = '<br />';
	foreach ($professional_address as $addrPart)
	{
		if($addrPart != "")
			$professional_address_out .= $splitter.$addrPart;
	}

	if(strlen($professional_address_out) > strlen($splitter))
		$professional_address_out = substr($professional_address_out, strlen($splitter));
	
	return $professional_address_out;
}

/**
 * @Get professional value by key
 * @Param professioal key/field name and user id
 * @Return void
 **/
function mm_get_the_professional_value($field_name, $user_id){
	global $wpdb;

	$table_professionals = $wpdb->base_prefix . "professionals";
	$table_countries = $wpdb->base_prefix . "countries";
	$table_counties = $wpdb->base_prefix . "counties";
	
	$result = false;
	switch($field_name)
	{
		case "country":
			$data_professionals = $wpdb->get_results("SELECT country FROM $table_professionals WHERE user_id = '$user_id'");
			$exist_value = $data_professionals[0]->country;
			if($exist_value && $exist_value != "")
			{
				$country_name = $wpdb->get_results("SELECT country_name FROM $table_countries WHERE country_code = '$exist_value'");
				$exist_value = $country_name[0]->country_name;
				if($exist_value) $result = $exist_value;
			}
			break;
		case "county":
			$data_professionals = $wpdb->get_results("SELECT county, country FROM $table_professionals WHERE user_id = '$user_id'");
			$exist_value = $data_professionals[0]->county;
			$exist_c_value = $data_professionals[0]->country;
			if($exist_value && $exist_value != "" && $exist_c_value && $exist_c_value == "GBR")
			{
				$county_name = $wpdb->get_results("SELECT county_name FROM $table_counties WHERE county_code = '$exist_value'");
				$county_value = $county_name[0]->county_name;
				if($county_value) 
				{
					$result = $county_value;
				}
				else
				{
					$result = $exist_value;
				}
			}
			else
			{
				$result = $exist_value;
			}
			break;
		default:
			$data_professionals = $wpdb->get_results("SELECT {$field_name} FROM $table_professionals WHERE user_id = '$user_id'");
			$exist_value = $data_professionals[0]->$field_name;
			if($exist_value) $result = $exist_value;
			break;
	}
	return $result;
}

function pro_get_professional_services($professional_id)
{
	global $wpdb;
	$sql = "SELECT * FROM ".$wpdb->base_prefix."professional_services WHERE professional_id = ".(int)$professional_id." ORDER BY category";
	return $wpdb->get_results($sql, ARRAY_A);
}

function pro_add_professional_service($data)
{
	global $wpdb;
	$services_table = $wpdb->base_prefix."professional_services";
	$query = "INSERT INTO $services_table (professional_id, service, category, price) VALUES(%d, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $data["professional_id"], $data["service"], $data["category"], $data["price"]));
}

function pro_clear_professional_services($professional_id)
{
	global $wpdb;
	$sql = "DELETE FROM ".$wpdb->base_prefix."professional_services WHERE professional_id = ".(int)$professional_id;
	$wpdb->query($sql);
}

/**
 * @check this user should display home page or not
 * @Param user id and it retrun true if display home yes
 * @Return True or False
 **/
function mm_is_display_home($user_id){
	global $wpdb;

	$table_professionals = $wpdb->base_prefix . "professionals";
	
	$data_professionals = $wpdb->get_results("SELECT home_featured FROM $table_professionals WHERE user_id = '$user_id'");
	$exist_value = $data_professionals[0]->home_featured;
	
	if($exist_value == 1)
		$result = TRUE;
	else
		$result = FALSE;
		
	return $result;
}

/**
 * @check this user for Deal feature or not
 * @Param user id and it retrun true if user is featured
 * @Return True or False
 **/
function mm_is_featured($user_id){
	global $wpdb;

	$table_professionals = $wpdb->base_prefix . "professionals";
	
	$data_professionals = $wpdb->get_results("SELECT feature FROM $table_professionals WHERE user_id = '$user_id'");
	$exist_value = $data_professionals[0]->feature;
	
	if($exist_value == 1)
		$result = TRUE;
	else
		$result = FALSE;
		
	return $result;				

}

/**
 * @check this user is professional or not
 * @Param user id and it retrun true if user is professional
 * @Return True or False
 **/
function mm_is_professional($user_id){
	global $wpdb;

	$table_professionals = $wpdb->base_prefix . "professionals";
	
	$data_professionals = $wpdb->get_results("SELECT status FROM $table_professionals WHERE user_id = '$user_id'");
	$exist_value = $data_professionals[0]->status;
	
	if($exist_value == 1)
		$result = TRUE;
	else
		$result = FALSE;
		
	return $result;				

}

/**
 * @Update professional data
 * @Param all user/professional id, professioal field and field value
 * @param int $user_id
 * @Return void
 **/
function mm_update_professional_single_data($user_id, $profession_key, $profession_value){
	global $wpdb;
	$table_professionals = $wpdb->base_prefix . "professionals";	
	$wpdb->query("UPDATE $table_professionals SET $profession_key = '$profession_value' WHERE user_id = '$user_id'");
}

function mm_send_link_column_header($columns) {
	$columns['send_link'] = 'Send link to subscribers';
	return $columns;
}

function mm_send_link_column($column_name) {
	global $post;
	if ($column_name == 'send_link') {
		$send_link_meta = get_post_meta($post->ID, 'send-link');
		$postlink_is_sending = false;
		if (count($send_link_meta) > 0 && $send_link_meta[0] === '1')
			$postlink_is_sending = true;

		echo '<input type="checkbox" disabled', ( $postlink_is_sending ? ' checked' : ''), '/>';
	}
}

function mm_add_send_link_meta() {
	add_meta_box(
		'send-link',          // this is HTML id of the box on edit screen
		'Send link to subscribers',    // title of the box
		'mm_send_link_checkbox_content',   // function to be called to display the checkboxes, see the function below
		'post',        // on which edit screen the box should appear
		'normal',      // part of page where the box should appear
		'default'      // priority of the box
	);
}

function mm_send_link_checkbox_content($post) {
	$send_link_meta = get_post_meta($post->ID, 'send-link');
	$postlink_is_sending = false;
	if (count($send_link_meta) > 0 && $send_link_meta[0] === '1')
		$postlink_is_sending = true;

	echo '<input type="checkbox" name="send-link"', ( $postlink_is_sending ? ' checked' : ''), '/>';
}

function mm_send_link_save_data($post_id) {

	if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE)
		return;

	if (isset($_POST['send-link']))
		update_post_meta($post_id, 'send-link', '1');
	else
		update_post_meta($post_id, 'send-link', '0');
}

function mm_get_links_for_review() {
	global $wpdb;

	$posts_table = $wpdb->base_prefix . "posts";
	$postmeta_table = $wpdb->base_prefix . "postmeta";

	$data = $wpdb->get_results("SELECT p.id, p.post_title as title, subtitle_meta.meta_value as subtitle

	FROM $posts_table as p

	INNER JOIN $postmeta_table AS send_link_meta
	    ON (send_link_meta.post_id = p.id AND send_link_meta.meta_key = 'send-link' AND send_link_meta.meta_value = '1')

	LEFT JOIN $postmeta_table AS subtitle_meta
    	ON (subtitle_meta.post_id = p.id AND subtitle_meta.meta_key = 'kia_subtitle')");

	$results = array();

	foreach ($data as $obj) {
		$item = array(
			'subtitle' => $obj->subtitle,
			'title' => $obj->title,
			'url' => get_permalink($obj->id)
		);
		array_push($results, $item);
	}

	return $results;
}

