<?php
/**
 * Plugin Name: RM Reviews
 * Plugin URI: 
 * Description: rm review managements
 * Version: 1.1
 * Author: Md. Anas Bin Mukim
**/


define('RM_REVIEW_ROOT', dirname(__FILE__));
define('RM_REVIEW_PLUGIN_URL', plugins_url('/', __FILE__));

define('RM_REVIEW_HOME', home_url('/'));
function register_rm_review_script() {
	//wp_enqueue_script('jquery_review_script_rate', RM_REVIEW_PLUGIN_URL.'js/jquery-1.8.2.min.js', array('jquery'));
	//wp_enqueue_script('jquery_review_rate', RM_REVIEW_PLUGIN_URL.'js/jquery.rateit.js', array('jquery'));
	wp_enqueue_script('jquery_review_rate', RM_REVIEW_PLUGIN_URL.'js/jquery.rateit.js');
	wp_register_style( 'rm-review-style', RM_REVIEW_PLUGIN_URL.'css/rm_review.css');
    wp_enqueue_style( 'rm-review-style' );
}
add_action('wp_enqueue_scripts', 'register_rm_review_script');

require_once( RM_REVIEW_ROOT . '/include/rm_review_functions.php');

add_action('admin_menu', 'rm_review_plugin_page');

// add admin menu 
function rm_review_plugin_page(){
 //$icon_url = RM_REVIEW_PLUGIN_URL.'img/icons.png';
 add_menu_page( 'RM Review', 'Reviews', 'administrator', 'rm_all_reviews', 'rm_all_review_admin_page' );
 add_submenu_page( 'rm_all_reviews','All Reviews', 'All Reviews', 'administrator', 'rm_all_reviews', 'rm_all_review_admin_page' );
 add_submenu_page( 'rm_all_reviews','Professional Reviews', 'Professional Reviews', 'administrator', 'rm_professional_reviews', 'rm_review_by_review_type' );
 add_submenu_page( 'rm_all_reviews','Deal Reviews', 'Deal Reviews', 'administrator', 'rm_deal_reviews', 'rm_review_by_review_type' );
 add_submenu_page( 'rm_all_reviews','Procedure Reviews', 'Procedure Reviews', 'administrator', 'rm_procedure_reviews', 'rm_review_by_review_type' );
}


//start sub menu for all show review show and edit option
function rm_all_review_admin_page(){
	if(isset($_GET['action']) && $_GET['action'] == 'view_comments')
		require_once( RM_REVIEW_ROOT . '/include/rm_review_comments.php');
	
	elseif(isset($_GET['action']) && $_GET['action'] == 'edit_review_comment')
		require_once( RM_REVIEW_ROOT . '/include/rm_edit_review_comments.php');
		
	elseif(isset($_GET['action']) && $_GET['action'] == 'edit_review')
		require_once( RM_REVIEW_ROOT . '/include/rm_review_edit_page.php');
		
	else
	require_once( RM_REVIEW_ROOT . '/include/rm_show_all_reviews.php');

}//eof haitihub_group_members_admin_page

// admin menu function
function rm_review_by_review_type(){
	
	if( isset( $_GET['page'] ) && $_GET['page'] == 'rm_professional_reviews' ){
		require_once( RM_REVIEW_ROOT . '/include/rm_review_professional.php');
	}
	
	if( isset( $_GET['page'] ) && $_GET['page'] == 'rm_deal_reviews' ){
		require_once( RM_REVIEW_ROOT . '/include/rm_review_deal.php');
	}
	
	if( isset( $_GET['page'] ) && $_GET['page'] == 'rm_procedure_reviews' ){
		require_once( RM_REVIEW_ROOT . '/include/rm_review_procedure.php');
	}
	
}//eof admin menu page


// create table review....
function rm_review_plugin_install(){
  global $wpdb;
  require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
  
	$review_table_name = $wpdb->base_prefix . "rm_reviews";
	
    $review_create_table = "CREATE TABLE IF NOT EXISTS $review_table_name (
      review_id BIGINT(20) NOT NULL AUTO_INCREMENT,
	  review_title VARCHAR(100) NOT NULL,
	  review_description TEXT NOT NULL,
	  review_rating double NOT NULL,
	  recommend VARCHAR(32) NOT NULL,
	  review_type VARCHAR(32) NOT NULL,
	  review_by BIGINT(20) NOT NULL,
	  review_for BIGINT(20) NOT NULL,
	  review_status VARCHAR(32) NOT NULL COMMENT 'approved, pending, archive',
	  PRIMARY KEY (review_id)
	)";
	
	
	dbDelta($review_create_table);
  	//$wpdb->query($review_create_table); 
	
	$review_comment_table_name = $wpdb->base_prefix . "rm_reviews_comments";
	
    $review_comment_create_table = "CREATE TABLE IF NOT EXISTS $review_comment_table_name (
      review_comment_id BIGINT(20) NOT NULL AUTO_INCREMENT,
	  review_id BIGINT(20) NOT NULL,
	  review_comment TEXT,
	  review_commentator BIGINT(20) NOT NULL,
	  review_comment_status VARCHAR(32) NOT NULL COMMENT 'approved, pending, archive',
	  PRIMARY KEY (review_comment_id)
	)";
	
	dbDelta($review_comment_create_table);    
}  



function rm_review_plugins_uninstall(){
	global $wpdb;
	$table_reviews = $wpdb->base_prefix . "rm_reviews";
	$table_review_comments = $wpdb->base_prefix . "rm_reviews_comments";
	
	$rm_review_drop = "DROP TABLE IF EXISTS $table_reviews";
	$rm_review_comment_drop = "DROP TABLE IF EXISTS $table_review_comments";
	
	$wpdb->query($rm_review_drop);  
	$wpdb->query($rm_review_comment_drop); 
}

// Register the activate and deactivate hooks
register_activation_hook( __FILE__, 'rm_review_plugin_install' );
register_deactivation_hook( __FILE__, 'rm_review_plugins_uninstall' );


?>