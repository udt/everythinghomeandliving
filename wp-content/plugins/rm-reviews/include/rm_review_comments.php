<?php
global $wpdb;
$review_comment_table_name = $wpdb->base_prefix . "rm_reviews_comments";

//$group_details = haitihub_get_group_details_by_id($request_group_id);



?>

<div class="wrap">
<h2>Review Comments</h2>
<?php 
	if( isset( $_GET['change_status'] ) && isset( $_GET['review_comment_id'] ) ){
	
		$comment_status = $_GET['change_status'];
		
		$review_comment_id = $_GET['review_comment_id'];
		update_review_comment_status( $review_comment_id, $comment_status );
		echo '<div class="updated"><p>Comment Status Updated</p></div>';

	}
?>
<?php
	echo '<table width="100%" cellpadding="3" cellspacing="3" class="widefat" style="width: 100%;">';
	echo '<thead>';
	echo '<tr>';
	echo '<th scope="col">';
	echo 'Review ID';
	echo '</th>';	
	echo '<th scope="col">';
	echo 'Review Comment';
	echo '</th>';
	echo '<th scope="col">';
	echo 'Review Commentator';
	echo '</th>';	
	echo '<th scope="col">';
	echo 'Review Comment Status';
	echo '</th>';		
	echo '<th scope="col">';
	echo 'Change Status';
	echo '</th>';
	echo '<th scope="col">';
	echo 'Action';
	echo '</th>';	
	echo '</tr>';
	echo '</thead>';


// how many rows to show per page
$rowsPerPage = 25;

// by default we show first page
$pageNum = 1;

// if $_GET['page'] defined, use it as page number
if(isset($_GET['p'])){
	$pageNum = $_GET['p'];
}

// counting the offset
$offset = ($pageNum - 1) * $rowsPerPage;
global $wpdb;
$review_id = $_GET['review_id'];

$sql_1 = "SELECT * FROM $review_comment_table_name WHERE review_id = '$review_id' ORDER BY review_comment_id LIMIT $offset, $rowsPerPage";

//"SELECT * FROM $review_table_name WHERE review_type = $review_type ORDER BY review_id LIMIT $offset, $rowsPerPage";
//print_r($sql_1);
	
$result_1 = $wpdb->get_results($sql_1,ARRAY_A); 
foreach($result_1 as $result){
	
echo "<tr><td>".$result['review_id']."</td><td>".$result['review_comment']."</td><td>".$result['review_commentator'] ."</td><td>".$result['review_comment_status'] ."</td><td>";


if( $result['review_comment_status'] == 'pending'){
echo "<a href='?page=rm_all_reviews&action=view_comments&review_id=".$result['review_id']."&tags=&p=".$pageNum."&change_status=approved&review_comment_id=".$result['review_comment_id']."'><img src='".RM_REVIEW_PLUGIN_URL."/img/no.gif' alt='Pending'/></a>";
}

if( $result['review_comment_status'] == 'approved'){
echo "<a href='?page=rm_all_reviews&action=view_comments&review_id=".$result['review_id']."&tags=&p=".$pageNum."&change_status=pending&review_comment_id=".$result['review_comment_id']."'><img src='".RM_REVIEW_PLUGIN_URL."/img/yes.gif' alt='Approved'/></a>";
}

echo "</td><td><a href='?page=rm_all_reviews&action=edit_review_comment&review_comment_id=".$result['review_comment_id']."'>Edit</a></td>";

echo "</tr>";
}
if(count($result_1) == 0){ echo "<tr><td colspan='5'>Not Found!</td></tr>";}
?>
</table>

<div class="tablenav">
  <div class="tablenav-pages">
    <?php
       
	$pages = $wpdb->get_results("SELECT review_comment_id FROM $review_comment_table_name ORDER BY review_comment_id");


    $numrows = count($pages);

    // how many pages we have when using paging?
    $maxPage = ceil($numrows/$rowsPerPage);

    // print the link to access each page
    $path = '?page=rm_reviews_comments';
    $nav  = '';

    for($page = 1; $page <= $maxPage; $page++){
      if ($page == $pageNum){
        $nav .= ' <span class="page-numbers current">' . $page . '</span>'; // no need to create a link to current page
      }else{
        $nav .= ' <a href="' . $path . '&tags=' . $tag . '&p=' . $page . '" class="page-numbers">' . $page . '</a>';
      }
    }

    if ($pageNum > 1){
      $page  = $pageNum - 1;

      $prev  ='<a href="' . $path . '&tags=' . $tag . '&p=' . $page . '" class="prev page-numbers">Previous</a>';
    }else{
      $prev  = '&nbsp;'; // we're on page one, don't print previous link
      $first = '&nbsp;'; // nor the first page link
    }

    if ($pageNum < $maxPage){
      $page = $pageNum + 1;
      $next = ' <a href="' . $path . '&tags=' . $tag . '&p=' . $page . '" class="next page-numbers">Next</a>';
    }else{
      $next = '&nbsp;'; // we're on the last page, don't print next link
      $last = '&nbsp;'; // nor the last page link
    }

    // print the navigation link
    echo $prev . $nav . $next;

  ?>
  </div>
  <br class="clear"/>
</div><!--tablenav-->

</div>