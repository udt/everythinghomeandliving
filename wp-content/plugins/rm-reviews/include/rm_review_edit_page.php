<?php
global $wpdb;
$table = $wpdb->base_prefix . "rm_reviews";

if(!isset($_GET['review_id'])) return;

$request_review_id = $_GET['review_id'];

if(isset($_POST['edit_review_submit'])){
	$edit_title = $_POST['review_edit_title'];
	$edit_description = $_POST['review_edit_description'];
	$edit_recommend = $_POST['review_edit_recommend'];
	$edit_rating = $_POST['review_edit_rating'];
	$edit_status = $_POST['review_edit_status'];
	update_review_list_details($request_review_id, $edit_title, $edit_description, $edit_rating, $edit_recommend, $edit_status);
	echo '<div class="updated"><p>Review Updated</p></div>';
}


$review_details = get_rm_review_details($request_review_id);

?>
<div class="wrap">
	<h2>Review for &ldquo; <?php echo $review_details['review_title']; ?> &rdquo; </h2>
    <form action="admin.php?page=rm_all_reviews&action=edit_review&review_id=<?php echo $request_review_id;?>" method="post" enctype="multipart/form-data">

<table class="form-table">

	<tr valign="top">
    	<th scope="row"><label for="edit_review_title">Review Title</label></th>
    	<td><input type="text" class="regular-text"  name="review_edit_title" id="review_edit_title" value="<?php echo $review_details['review_title']; ?>" placeholder="type review title here"></td>
    </tr>
    <tr valign="top"><td><label for="edit_review_description">Review Description</label></td>
    	<td>
        <?php
		$content = $review_details['review_description'];
		wp_editor($content, 'review_edit_description');
		?>
        </td>
    </tr>
    <tr valign="top"><td><label for="edit_recommend">Recommend</label></td>
    	<td> <?php if($review_details['recommend']=='yes'){?>
        <input type="radio" name="review_edit_recommend" checked="checked" value="yes" />Yes
        <input type="radio" name="review_edit_recommend" value="no" />No<?php } ?>
        <?php if($review_details['recommend']=='no'){?>
        <input type="radio" name="review_edit_recommend" value="yes" />Yes
        <input type="radio" name="review_edit_recommend" checked="checked" value="no" />No
        <?php }?></td>
    </tr>
    
     <tr valign="top"><td><label for="edit_review_rating">Review Rating</label></td>
    	<td><select name="review_edit_rating" id="review_edit_rating">
        	<option <?php if($review_details['review_rating'] == '0.5') {?> selected="selected" <?php } ?>value="0.5">0.5</option>
            <option <?php if($review_details['review_rating'] == '1') {?> selected="selected" <?php } ?>value="1">1</option>
            <option <?php if($review_details['review_rating'] == '1.5') {?> selected="selected" <?php } ?>value="1.5">1.5</option>
            <option <?php if($review_details['review_rating'] == '2') {?> selected="selected" <?php } ?>value="2">2</option>
            <option <?php if($review_details['review_rating'] == '2.5') {?> selected="selected" <?php } ?>value="2.5">2.5</option>
            <option <?php if($review_details['review_rating'] == '3') {?> selected="selected" <?php } ?>value="3">3</option>
            <option <?php if($review_details['review_rating'] == '3.5') {?> selected="selected" <?php } ?>value="3.5">3.5</option>
            <option <?php if($review_details['review_rating'] == '4') {?> selected="selected" <?php } ?>value="4">4</option>
            <option <?php if($review_details['review_rating'] == '4.5') {?> selected="selected" <?php } ?>value="4.5">4.5</option>
            <option <?php if($review_details['review_rating'] == '5') {?> selected="selected" <?php } ?>value="5">5</option>
        </select>
        </td>
    </tr>
    
     <tr valign="top"><td><label for="edit_review_status">Review Status</label></td>
    	<td><select name="review_edit_status" id="review_edit_status">
        	<option <?php if($review_details['review_status'] == 'approved') {?> selected="selected" <?php } ?>value="approved">Approved</option>
            <option <?php if($review_details['review_status'] == 'pending') {?> selected="selected" <?php } ?>value="pending">Pending</option>
            <option <?php if($review_details['review_status'] == 'archive') {?> selected="selected" <?php } ?>value="archive">Archive</option>
        </select>
        </td>
    </tr>
    
</table>

<p class="submit"><input type="submit" name="edit_review_submit" value="Submit" class="button" /></p>
</form>
    
</div>