<?php
// review insert function 
function insert_review_data($review_title, $review_description, $review_rating, $recommend, $review_type, $review_by, $review_for){
	global $wpdb;
	$review_table_name = $wpdb->base_prefix . "rm_reviews";
	
	//Here insert review data
		
		$query_rm_review_insert = "INSERT INTO $review_table_name (review_title, review_description, review_rating, recommend, review_type, review_by, review_for, review_status) VALUES (%s, %s, %s, %s, %s, %d, %d, %s)";	
	
	if($wpdb->query($wpdb->prepare($query_rm_review_insert, $review_title, $review_description, $review_rating, $recommend, $review_type, $review_by, $review_for, 'pending')) === FALSE)
		return FALSE;
}

// get review details by id
function get_rm_review_details($review_id){
	global $wpdb;
	$details = array();
	
	$table = $wpdb->base_prefix . "rm_reviews";	
	
	$review_data = $wpdb->get_results("SELECT * FROM $table WHERE review_id = '$review_id'");	
	
	$details['review_title'] = $review_data[0]->review_title;
	$details['review_description'] = $review_data[0]->review_description;
	$details['review_rating'] = $review_data[0]->review_rating;
	$details['recommend'] = $review_data[0]->recommend;
	$details['review_status'] = $review_data[0]->review_status;
	
	return $details;
}

//review update 
function update_review_list_details($review_id, $edit_title, $edit_description, $edit_rating, $edit_recommend, $edit_status){
	global $wpdb;
	$table = $wpdb->base_prefix . "rm_reviews";
	
	$query = "UPDATE $table SET review_title = %s, review_description = %s, review_rating = %s, recommend = %s, review_status = %s WHERE review_id = %d";	
	
	$wpdb->query($wpdb->prepare($query, $edit_title, $edit_description, $edit_rating, $edit_recommend, $edit_status, $review_id ));
	
	
}
// status update
function update_review_status($review_id, $current_status){
	global $wpdb;
	$table = $wpdb->base_prefix . "rm_reviews";
	
	$query = "UPDATE $table SET review_status = %s WHERE review_id = %d";	
	
	$wpdb->query($wpdb->prepare($query, $current_status, $review_id ));
	
	
}


//delete reviews
function delete_rm_reviews( $review_id ){
	global $wpdb;
	$table = $wpdb->base_prefix . "rm_reviews";
	$sql = "DELETE FROM $table WHERE review_id = %d";
	$wpdb->query( $wpdb->prepare( $sql, $review_id ) );
	
	//Delete review comment	
	$review_comment_table = $wpdb->base_prefix . "rm_reviews_comments";
	$sql_review_comment = "DELETE FROM $review_comment_table WHERE review_id = %d";
	$wpdb->query( $wpdb->prepare( $sql_review_comment, $review_id ) );	
}


// total number of reviews by review by review type , review status
function count_rm_reviews( $review_for, $review_type, $review_status = '' ){

	global $wpdb;
	$table = $wpdb->base_prefix . "rm_reviews";
	
	if( $review_status == '')
		$review_status = 'approved';
	
	
	//membem status all to dispaly all member in group admin panel	
	if($review_status == 'all')	
		$total_members = $wpdb->get_var($wpdb->prepare("SELECT COUNT(review_id) FROM $table WHERE review_for = %d AND review_type = %s", $review_for, $review_type ) );
	else
		$total_members = $wpdb->get_var($wpdb->prepare("SELECT COUNT(review_id) FROM $table WHERE review_for = %d AND review_type = %s AND review_status = %s", $review_for, $review_type, $review_status ) );	
		
		
	
	return $total_members;
	
}

function rm_review_get_curPageURL() {
	 $pageURL = 'http';
	 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 } else {
	  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 }
	 return $pageURL;
}



//start rm review form 
function rm_review_form_front( $atts, $content='' ){ 
	global $current_user;
 extract(shortcode_atts(array(
	      'review_for' => $atts['review_for'],
	      'review_type' => $atts['review_type'],
     ), $atts));
	 
	 if(isset($_POST['submit_review']) && is_user_logged_in()){
		$review_title = $_POST['review_title'];
		$review_description = $_POST['review_description'];
		$review_rating = $_POST['review_rating'];
		$recommaend = $_POST['recommaend'];
		$review_type = $_POST['review_type'];
		$review_for = $_POST['review_for'];
		
		insert_review_data($review_title, $review_description, $review_rating, $recommaend, $review_type, get_current_user_id(), $review_for); // insert data into table
		$message = '<p>Review Insert Successful.</p>';
	}
	
	echo $message;
    
 	//review count, review average and lists reviews
	$review_status = 'approved';
	$review_count = count_rm_reviews( $review_for, $review_type, $review_status );
	//echo $review_count;
	//echo get_rating_stars_average( $review_count, $review_for, $review_type, $review_status );
	$reviewList = get_reviews_list_for_type( $review_for, $review_type ); 
	
	if( is_user_logged_in() ){ 
	
	//review for info	
	if($review_type == 'professional')
	{
		//for professionals review
		$professioal_user_info = get_userdata($review_for);
		$review_display_name = $professioal_user_info->display_name;
	}
	else
		$review_display_name = 'this '.$review_type;
	
	return $reviewList.'
	<div id="review_form" class="review_form">
		<form action="'.rm_review_get_curPageURL().'" method="post" name="review_form" id="review_form">
			<fieldset class="rm-review-form">
				<p>
					<label for="review_title">Title</label>
					<br />
					<input type="text" name="review_title" id="review_title" value="" placeholder="Type review title here" />
				</p>
				<p>
					<label for="review_description">Comment</label>
					<br />
					<textarea name="review_description" id="review_description" value="" placeholder="Type your comment here" rows="5" cols="5" ></textarea>
				</p>
				<div class="clearfix">
					<div class="clearfix" style="float:right;">
						<div style="width:100%; text-align:right;" class="rateit" data-rateit-backingfld="#backing2" onclick="$(\'#backing2\').toggle()"></div>
						<input type="hidden" min="0" max="5" value="0" step="0.5" id="backing2" name="review_rating">
						<p>Would You Recommend '.$review_display_name.'? 
							<input type="radio" name="recommaend" id="recommaend_yes" value="yes" checked="checked" />
							<label for="recommaend_yes">Yes</label>
							<input type="radio" name="recommaend" id="recommaend_no" value="no" />
							<label for="recommaend_no">No</label>
						</p>
						<input type="hidden" name="review_type" id="review_type" value="'.$review_type.'" />
						<input type="hidden" name="review_for" id="review_for" value="'.$review_for.'" />
					</div>
				</div>

				<input type="submit" value="submit" name="submit_review" id="submit_review" class="pink-submit-button" />

			</fieldset>
		</form>
	</div>';
	}else{ return $reviewList; } 
} 

// shortcode for rm review form 
add_shortcode( 'rm_review_form', 'rm_review_form_front' );

//get rating stars review
function get_rating_stars_average( $rating_count, $reviewfor, $reviewtype, $review_status = '' ){
	global $wpdb;
	$rating_sum = '';
	$table = $wpdb->base_prefix . "rm_reviews";	
	if( $review_status =='' ){
		$review_status = 'approved';
	}
	
	$sql = $wpdb->prepare("SELECT * FROM $table WHERE review_for = %d AND review_type = %s AND review_status = %s", $reviewfor, $reviewtype, $review_status );
	$rating_data = $wpdb->get_results($sql,ARRAY_A); 
	//print_r( $rating_data );
	if( !empty($rating_data) ){
	foreach( $rating_data as $value ){
		$rating_sum += $value['review_rating'];
	}
	$result =  $rating_sum / $rating_count ;
	}
	//echo $rating_sum;
	$rating_class = get_rating_class($result);

	$rating_result = '<span class="rating ' . $rating_class . '"></span>';
	
	return $rating_result;
} //end function get_rating_stars_average

// review insert comment
function insert_review_comment( $review_id, $review_comments, $review_commentator, $review_comment_status ){
	global $wpdb;
	$review_comment_table_name = $wpdb->base_prefix . "rm_reviews_comments";
	
	//Here insert review data
		
		$query_review_comment = "INSERT INTO $review_comment_table_name ( review_id, review_comment, review_commentator, review_comment_status ) VALUES ( %d, %s, %d, %s )";	
	
	if($wpdb->query($wpdb->prepare( $query_review_comment, $review_id, $review_comments, $review_commentator, $review_comment_status )) === FALSE)
		return FALSE;
}

//get review comments
function get_rm_review_comment($reviewer_id){
	global $wpdb;
	$review_comment_table_name = $wpdb->base_prefix . "rm_reviews_comments";
	
	$review_comment_data = $wpdb->get_results("SELECT * FROM $review_comment_table_name WHERE review_comment_id = '$reviewer_id'");	
	
	$details['review_comment'] = $review_comment_data[0]->review_comment;
	$details['review_comment_status'] = $review_comment_data[0]->review_comment_status;
	
	return $details;
}

//update review comments
function update_review_comment( $review_comment_id, $update_comment, $update_comment_status ){
	global $wpdb;
	
	$review_comment_table_name = $wpdb->base_prefix . "rm_reviews_comments";
	
	$query = "UPDATE $review_comment_table_name SET review_comment = %s, review_comment_status = %s WHERE review_comment_id = %d";	
	
	$wpdb->query($wpdb->prepare($query, $update_comment, $update_comment_status, $review_comment_id ));
}

//update review status
function update_review_comment_status( $review_comment_id, $current_status ){
	global $wpdb;
	$review_comment_table_name = $wpdb->base_prefix . "rm_reviews_comments";
	
	$query = "UPDATE $review_comment_table_name SET review_comment_status = %s WHERE review_comment_id = %d";	
	
	$wpdb->query($wpdb->prepare($query, $current_status, $review_comment_id ));
}

//get reviews for individual review type id
function get_reviews_list_for_type( $review_for, $review_type ){
	global $wpdb;
	
	$table = $wpdb->base_prefix . "rm_reviews";	
	$review_status = 'approved';
	
	$sql = $wpdb->prepare("SELECT * FROM $table WHERE review_for = %d AND review_type = %s AND review_status = %s", $review_for, $review_type, $review_status );
	$review_data = $wpdb->get_results($sql,ARRAY_A); 
	
	//$review_data = $wpdb->get_results("SELECT * FROM $table WHERE review_for = '$review_for' AND review_status = '$review_status'");
	//$result = '<div class="reviews_show">';
	$result = '<div class="vamtam-cubeportfolio cbp cbp-slider-edge vamtam-testimonials-slider" data-options="{&quot;layoutMode&quot;:&quot;slider&quot;,&quot;drag&quot;:true,&quot;auto&quot;:false,&quot;autoTimeout&quot;:5000,&quot;autoPauseOnHover&quot;:true,&quot;showNavigation&quot;:false,&quot;showPagination&quot;:true,&quot;rewindNav&quot;:true,&quot;scrollByPage&quot;:false,&quot;gridAdjustment&quot;:&quot;responsive&quot;,&quot;mediaQueries&quot;:[{&quot;width&quot;:1,&quot;cols&quot;:1}],&quot;gapHorizontal&quot;:0,&quot;gapVertical&quot;:0,&quot;caption&quot;:&quot;&quot;,&quot;displayType&quot;:&quot;default&quot;}">';
	if(count($review_data) > 0)
	{
		$i = 0;
		foreach( $review_data as $value )
		{
			$i++;
			$review_id = $value['review_id'];
			$reviewfor = $value['review_for'];
			$review_by = $value['review_by'];
			$review_date = $value['review_date'];

			$user_info = get_userdata( $review_by );

			$user_profile_url = build_users_seo_public_link($user_info->ID);


			$rating = $value['review_rating'];
			$rating_class = get_rating_class( $rating );
			$description = stripslashes($value['review_description']);

			/*$result .= '<div class="rm_reviews_comments">';	
			$result .= '<div class="rm_reviews">';
			$result .= '<div class="profile_image_display_name"><a href="' . $user_profile_url . '">'. get_avatar( $user_info->ID, 100 ). '</a>';
			$result .= '<a href="' . $user_profile_url . '" class="review_displayname">' . $user_info->display_name . '</a></div>';
			$result .= '<h3 class="review_title">' . stripslashes($value['review_title']) . '</h3>';
			$result .= '<p class="date_name_rating">';
			$result .= '<span class="review_date">' . date('M d, Y', strtotime($review_date)) . '</span> | ';
			//$result .= '<span class="review_displayname">' . $user_info->display_name . '</span> | ';
			$result .= '<span class="review_recommanded">Recommended( ' . $value['recommend'] . ')</span> | ';
			$result .= '<span class="rating ' . $rating_class . '"></span>';
			$result .= '</p>';
			//put read more link in review description
			$description = stripslashes($value['review_description']);
			if($description != '')
			{
				$readMoreLimit = 750;
				if(strlen($description) > $readMoreLimit)
				{
					$position = strpos($description, ' ', $readMoreLimit);
					if($position === false)
						$position = $readMoreLimit;
					else
						$position = $position + 1;

					$firstPart = substr($description, 0, $position);
					$lastPart = substr($description, $position);

					$description = $firstPart.'<a href="#" class="profile-read-more" onclick="javascript:$(this).remove();$(\'#read_more_content_'.$i.'\').show();return false;">Read more</a><span id="read_more_content_'.$i.'" style="display:none;">'.$lastPart.'</span>';
				}
			}
			
			$result .= '<div class="review_description">' . $description . '</div>';
			$result .= '</div>';
			$result .= '</div>';

			$result .= '<div class="rm_comments">';
			$result .= get_review_comments_for_specific_review($review_id);
			$result .= '</div>';
			if( is_user_logged_in() ){
				if( $value['review_type'] == 'professional' ){
					global $current_user;
					get_currentuserinfo();
					$user_profession_id = $current_user->ID;

					if( $user_profession_id == $reviewfor ){
						$result .= review_comment_form_for_owner( $review_id, $reviewfor );
					}
				}
			}*/
			$result .= '<div class="cbp-item">';
				$result .= '<blockquote class="clearfix small simple post-'.$review_id.' jetpack-testimonial type-jetpack-testimonial status-publish has-post-thumbnail hentry">';
					$result .= '<div class="quote-thumbnail">';
						$result .= get_avatar( $user_info->ID, 100 );
					$result .= '</div>';
					$result .= '<div class="quote-text">';
						$result .= '<h3 class="quote-summary">"'.stripslashes($value['review_title']).'"</h3>';
						$result .= '<div class="quote-title-wrapper clearfix">';
							$result .= '<div class="quote-title">';
								$result .= '<span class="rating ' . $rating_class . '"></span> &mdash; ';
								$result .= '<span class="the-title">'.$user_info->display_name.'</span>';
							$result .= '</div>';
						$result .= '</div>';
						$result .= '<div class="quote-content">';
							$result .= '<p>'.$description.'</p>';
						$result .= '</div>';
					$result .= '</div>';
				$result .= '</blockquote>';
			$result .= '</div>';
		}
	}
	else
	{
		$result .= '<div>Currently there are no reviews. Be the first.</div>';
	}
	$result .= '</div><div style="clear:both;"></div>';
	return $result;
	
}

// get review comments
function get_review_comments_for_specific_review( $reviewid ){
	global $wpdb;
	
	$review_comment_table_name = $wpdb->base_prefix . "rm_reviews_comments";
	$review_comment_status = 'approved';
	
	$review_comment = $wpdb->get_results("SELECT * FROM $review_comment_table_name WHERE review_id = '$reviewid' AND review_comment_status = '$review_comment_status'");
	
	$results = '';
	foreach( $review_comment as $value ){
		$results .= '<div class="review_comment">' . $value->review_comment . '</div>';	
	}
	return $results;
}

// review comment form for owner

function review_comment_form_for_owner( $review_id, $review_for ){
	$form_name = 'review_comment_front_'.$review_id;
	if( isset( $_POST[$form_name] ) ){
		$review_id = $_POST['review_id'];
		$review_comments = $_POST['review_comments'];
		$review_commentator = $_POST['review_commentator'];
		$review_comment_status = $_POST['review_comment_status'];
		
		insert_review_comment( $review_id, $review_comments, $review_commentator, $review_comment_status );
		$message = '<p>Comment Insert Successful</p>';
	}
	
	$form = '';
	$form .= $message;
	$form .= '<div class="review_comment_form">';
	$form .= '<form name="rm_review_comment" id="rm_review_comment" method="post" action="' . rm_review_get_curPageURL() . '">';
	$form .= '<label for="review_comments">Review Comment</label><br />';
	$form .= '<textarea name="review_comments" id="review_comments" value="" placeholder="Enter a comment" ></textarea>';
	$form .= '<input type="hidden" name="review_commentator" id="review_commentator" value="' . $review_for . '">';
	$form .= '<input type="hidden" name="review_id" id="review_id" value="' . $review_id . '" />';
	$form .= '<input type="hidden" name="review_comment_status" id="review_comment_status" value="approved" />';
	$form .= '<input type="submit" name="review_comment_front_' . $review_id . '" id="review_comment_front_' . $review_id . '" value="submit" class="pink-submit-button" />';
	$form .= '</form>';
	$form .= '</div>';
	
	return $form;
}

//rating class show
function get_rating_class( $rating_results ){
	if( $rating_results >= 0 && $rating_results < 0.5 )
		$rating_class = 'rating_star_0';
		
		if( $rating_results >= 0.5 && $rating_results < 1 )
			$rating_class = 'rating_star_half_0';
		
		if( $rating_results >= 1 && $rating_results < 1.5 )
			$rating_class = 'rating_star_1';
		
		if( $rating_results >= 1.5 && $rating_results < 2 )
			$rating_class = 'rating_star_half_1';
			
		if( $rating_results >= 2 && $rating_results < 2.5 )
		$rating_class = 'rating_star_2';
		
		if( $rating_results >= 2.5 && $rating_results < 3 )
			$rating_class = 'rating_star_half_2';
		
		if( $rating_results >= 3 && $rating_results < 3.5 )
			$rating_class = 'rating_star_3';
		
		if( $rating_results >= 3.5 && $rating_results < 4 )
			$rating_class = 'rating_star_half_3';
	
		if( $rating_results >= 4 && $rating_results < 4.5 )
			$rating_class = 'rating_star_4';
		
		if( $rating_results >= 4.5 && $rating_results < 5 )
			$rating_class = 'rating_star_half_4';
			
		if( $rating_results == 5 )
			$rating_class = 'rating_star_5';
			
		return $rating_class;
}