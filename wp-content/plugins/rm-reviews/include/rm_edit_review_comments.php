<?php
global $wpdb;
$table = $wpdb->base_prefix . "rm_reviews_comments";

if(!isset($_GET['review_comment_id'])) return;

$request_comment_id = $_GET['review_comment_id'];

if(isset($_POST['edit_review_comment_submit'])){
	$edit_comment = $_POST['review_comment'];
	$edit_comment_status = $_POST['review_comment_status'];
	update_review_comment( $request_comment_id, $edit_comment, $edit_comment_status );
	echo '<div class="updated"><p>Review Comment Updated</p></div>';
}


$review_comment_details = get_rm_review_comment($request_comment_id);

?>
<div class="wrap">
	<h2>Review comment Edit</h2>
    <form action="admin.php?page=rm_all_reviews&action=edit_review_comment&review_comment_id=<?php echo $request_comment_id;?>" method="post" enctype="multipart/form-data">

<table class="form-table">

	<tr valign="top">
    	<th scope="row"><label for="review_comment">Review Comment</label></th>
    	<td><input type="text" class="regular-text"  name="review_comment" id="review_comment" value="<?php echo $review_comment_details['review_comment']; ?>" placeholder="type review comment here"></td>
    </tr>
    
     <tr valign="top"><td><label for="review_comment_status">Review Comment Status</label></td>
    	<td><select name="review_comment_status" id="review_comment_status">
        	<option <?php if($review_comment_details['review_comment_status'] == 'approved') {?> selected="selected" <?php } ?>value="approved">Approved</option>
            <option <?php if($review_comment_details['review_comment_status'] == 'pending') {?> selected="selected" <?php } ?>value="pending">Pending</option>
            <option <?php if($review_comment_details['review_comment_status'] == 'archive') {?> selected="selected" <?php } ?>value="archive">Archive</option>
        </select>
        </td>
    </tr>
    
</table>

<p class="submit"><input type="submit" name="edit_review_comment_submit" value="Submit" class="button" /></p>
</form>
    
</div>