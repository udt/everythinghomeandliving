$reviewfor = ID for specific profession/deal/procedure/post,
$reviewtype = professional/deal/procedure/post,

for template file use this shortcode on place where the form should be generated
echo do_shortcode('[rm_review_form review_for='.$reviewfor.' review_type='.$reviewtype.'][/rm_review_form]');

for content management in admin panel page editor put this code
[rm_review_form review_for='.$reviewfor.' review_type='.$reviewtype.'][/rm_review_form]

examples
[rm_review_form review_for='867' review_type='post'][/rm_review_form]
[rm_review_form review_for='2445' review_type='post'][/rm_review_form]
[rm_review_form review_for='664' review_type='procedure'][/rm_review_form]