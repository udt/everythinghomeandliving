<?php
/**
 * Archive page template
 *
 * @package wpv
 * @subpackage hair
 */

global $wpv_title;

$wpv_title = get_the_archive_title();

get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>
	<div class="row page-wrapper">

		<?php WpvTemplates::$in_page_wrapper = true; ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class( WpvTemplates::get_layout() ); ?>>
			<?php
			global $wpv_has_header_sidebars;
			if ( $wpv_has_header_sidebars ) {
				WpvTemplates::header_sidebars();
			}
			?>
			<div class="page-content">
				<?php rewind_posts() ?>
				<?php get_template_part( 'loop', 'archive' ) ?>
				<?php get_template_part( 'templates/share' ); ?>
			</div>
		</article>

		<?php get_template_part( 'sidebar' ) ?>
	</div>
<?php endif ?>

<?php get_footer();
