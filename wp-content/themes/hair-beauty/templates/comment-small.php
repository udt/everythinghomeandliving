<div id="comment-<?php comment_ID() ?>" <?php comment_class( 'cbp-item' . ( $args['has_children'] ? 'has-children' : '' ) ) ?>>
	<div class="sep-text centered keep-always">
		<div class="sep-text-before"><div class="sep-text-line"></div></div>
		<div class="content">
			<?php echo wpv_get_icon_html( array( 'name' => 'theme-quote' ) ); // xss ok ?>
		</div>
		<div class="sep-text-after"><div class="sep-text-line"></div></div>
	</div>
	<div class="comment-inner">
		<header class="comment-header">
			<h3 class="comment-author-link"><?php comment_author_link(); ?></h3>
			<?php
				if ( ( ! isset( $args['reply_allowed'] ) || $args['reply_allowed'] ) && ( $args['type'] == 'all' || get_comment_type() == 'comment' )  ) :
					comment_reply_link( array_merge( $args, array(
						'reply_text' => esc_html__( 'Reply', 'hair-beauty' ),
						'login_text' => esc_html__( 'Log in to reply.', 'hair-beauty' ),
						'depth'      => $depth,
						'before'     => '<h6 class="comment-reply-link">',
						'after'      => '</h6>',
					) ) );
				endif;
			?>
		</header>
		<?php comment_text() ?>
		<footer class="comment-footer">
			<h6 title="<?php comment_time(); ?>" class="comment-time"><?php comment_date(); ?></h6>
			<?php edit_comment_link( sprintf( '[%s]', esc_html__( 'Edit', 'hair-beauty' ) ) ) ?>
			<?php if ( $comment->comment_approved == '0' ): ?>
				<span class='unapproved'><?php esc_html_e( "Your comment is awaiting moderation.", 'hair-beauty' ); ?></span>
			<?php endif ?>
		</footer>
	</div>
