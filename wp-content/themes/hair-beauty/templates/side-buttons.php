<?php

/**
 * Displays the scroll to top button
 *
 * @package wpv
 * @subpackage hair
 */
?>

<?php if ( rd_wpv_get_option( 'show-scroll-to-top' ) || is_customize_preview() ) : ?>
	<div id="scroll-to-top" class="icon" <?php WpvTemplates::display_none( rd_wpv_get_option( 'show-scroll-to-top' ) ) ?>><?php wpv_icon( 'arrow-up3' ) ?></div>
<?php endif ?>
