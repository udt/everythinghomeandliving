<?php

/**
 * Prev/next/view all buttons for posts and projects
 *
 * @package wpv
 * @subpackage hair
 */

?>
<span class="post-siblings">
	<?php
		previous_post_link( '%link', wpv_get_icon_html( array( 'name' => 'theme-arrow-left22', 'link_hover' => false ) ) );
		next_post_link( '%link', wpv_get_icon_html( array( 'name' => 'theme-arrow-right22', 'link_hover' => false ) ) );
	?>
</span>
