<?php
/**
 * Site sub-header. Includes a slider, page title, etc.
 *
 * @package  wpv
 */

global $wpv_title;
if ( ! is_404() ) {
	if ( wpv_has_woocommerce() ) {
		if ( is_woocommerce() && ! is_single() ) {
			if ( is_product_category() ) {
				$wpv_title = single_cat_title( '', false );
			} elseif ( is_product_tag() ) {
				$wpv_title = single_tag_title( '', false );
			} else {
				$wpv_title = woocommerce_get_page_id( 'shop' ) ? get_the_title( woocommerce_get_page_id( 'shop' ) ) : '';
			}
		} elseif ( is_cart() || is_checkout() ) {
			$cart_title     = get_the_title( wc_get_page_id( 'cart' ) );
			$checkout_title = get_the_title( wc_get_page_id( 'checkout' ) );
			$complete_title = esc_html__( 'Order Complete', 'hair-beauty' );

			$cart_state     = is_cart() ? 'active' : 'inactive';
			$checkout_state = is_checkout() && ! is_order_received_page() ? 'active' : 'inactive';
			$complete_state = is_order_received_page() ? 'active' : 'inactive';

			$wpv_title = "
				<span class='checkout-breadcrumb'>
					<span class='title-part-{$cart_state}'>$cart_title</span>" .
					wpv_get_icon_html( array( 'name' => 'arrow-right1' ) ) .
					"<span class='title-part-{$checkout_state}'>$checkout_title</span>" .
					wpv_get_icon_html( array( 'name' => 'arrow-right1' ) ) .
					"<span class='title-part-{$complete_state}'>$complete_title</span>
				</span>
			";
		}
	}
}

$page_header_bg = WpvTemplates::page_header_background();

// $has_header_bg should be true for non-transparent backgrounds
$sub_header_bg_str = str_replace(
	'background-color:transparent;background-image:none;',
	'',
	$page_header_bg . rd_wpv_get_option( 'page-title-background', 'background-image' ) . rd_wpv_get_option( 'page-title-background', 'background-color'
) );

$has_header_bg  = ! empty( $sub_header_bg_str );


if ( ( ! WpvTemplates::has_page_header() && ! WpvTemplates::has_post_siblings_buttons() ) || is_404() ) return;
if ( is_page_template( 'page-blank.php' ) ) return;


?>
<div id="sub-header" class="layout-<?php echo esc_attr( WpvTemplates::get_layout() ) ?> <?php if ( $has_header_bg  ) echo 'has-background' ?>">
	<div class="meta-header" style="<?php echo esc_attr( $page_header_bg ) ?>">
		<div class="limit-wrapper">
			<div class="meta-header-inside">
				<?php
					WpvTemplates::page_header( false, $wpv_title );
				?>
			</div>
		</div>
	</div>
</div>
