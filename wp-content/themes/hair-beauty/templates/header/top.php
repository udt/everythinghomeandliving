<?php
	/**
	 * Actual, visible header. Includes the logo, menu, etc.
	 * @package wpv
	 */

	$layout = rd_wpv_get_option( 'header-layout' );

	if ( is_page_template( 'page-blank.php' ) ) return;
?>
<div class="fixed-header-box sticky-header-state-reset">
	<header class="main-header layout-<?php echo esc_attr( $layout ) ?> <?php if ( $layout === 'logo-menu' ) echo 'header-content-wrapper' ?>">
		<?php get_template_part( 'templates/header/top/nav' ) ?>
		<?php get_template_part( 'templates/header/top/main', $layout ) ?>
	</header>

	<?php do_action( 'wpv_header_box' ); ?>
</div>
