<?php
/**
 * Header search button
 *
 * @package wpv
 * @subpackage hair
 */


if ( ! rd_wpv_get_optionb( 'enable-header-search' ) && ! is_customize_preview() ) return;

?>
<div class="search-wrapper" <?php WpvTemplates::display_none( rd_wpv_get_optionb( 'enable-header-search' ) ) ?>>
	<button class="header-search icon wpv-overlay-search-trigger"><?php wpv_icon( 'search1' ) ?></button>
</div>
