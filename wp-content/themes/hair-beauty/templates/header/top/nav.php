<?php
	/**
	 * Top bar ( above the logo )
	 * @package wpv
	 */

	$layout = rd_wpv_get_option( 'top-bar-layout' );

	$layout = ! empty( $layout ) ? explode( '-', $layout ) : null;
?>
<?php if ( $layout ) :  ?>
	<div id="top-nav-wrapper" style="<?php echo esc_attr( WpvTemplates::build_background( rd_wpv_get_option( 'top-nav-background' ) ) ) ?>">
		<?php do_action( 'wpv_top_nav_before' ) ?>
		<?php get_template_part( 'templates/header/top/nav', 'inner' ) ?>
		<?php do_action( 'wpv_top_nav_after' ) ?>
	</div>
<?php endif ?>
