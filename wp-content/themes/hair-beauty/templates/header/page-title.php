<?php $hide_lowres_bg = rd_wpv_get_optionb( 'page-title-background-hide-lowres' ) ? 'wpv-hide-bg-lowres' : ''; ?>
<header class="page-header layout-<?php echo esc_attr( $layout ) ?> <?php echo esc_attr( $hide_lowres_bg ) ?> <?php echo esc_attr( $uses_local_title_layout ) ?>">
	<h1 style="<?php echo esc_attr( $title_color ) ?>" itemprop="headline">
		<?php echo wp_kses_post( $title ) ?>
	</h1>
	<?php if ( ! empty( $description ) ) :  ?>
		<div class="desc" style="<?php echo esc_attr( $title_color ) ?>"><?php echo wp_kses_post( $description ) ?></div>
	<?php endif ?>
</header>
