<?php
$show = rd_wpv_get_optionb( 'post-meta', 'tax' );

if ( $show || is_customize_preview() ) :
?>
	<div class="wpv-meta-tax" <?php WpvTemplates::display_none( $show ) ?>><span class="icon theme"><?php wpv_icon( 'theme-layers' ); ?></span> <span class="visuallyhidden"><?php esc_html_e( 'Category', 'hair-beauty' ) ?> </span><?php the_category( ', ' ); ?></div>
<?php
	$tags = get_the_tags();

	if ( count( $tags ) ) : ?>
		<div class="the-tags wpv-meta-tax" <?php WpvTemplates::display_none( $show ) ?>>
			<?php the_tags( '<span class="icon">' . wpv_get_icon( 'tag' ) . '</span> <span class="visuallyhidden">'.esc_html__( 'Tags', 'hair-beauty' ).'</span> ', ', ', '' ); ?>
		</div>
<?php
	endif;
endif;