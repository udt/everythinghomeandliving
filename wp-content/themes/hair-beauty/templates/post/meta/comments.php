<?php
$show = rd_wpv_get_optionb( 'post-meta', 'comments' ) && comments_open();
$comment_icon = wpv_get_icon_html( array( 'name' => 'theme-bubble' ) );
?>
<?php if ( $show || is_customize_preview() ) : ?>
	<div class="comment-count wpv-meta-comments" <?php WpvTemplates::display_none( $show ) ?>>
		<?php
			comments_popup_link(
				$comment_icon . wp_kses_post( __( '0 <span class="comment-word visuallyhidden">Comments</span>', 'hair-beauty' ) ),
				$comment_icon . wp_kses_post( __( '1 <span class="comment-word visuallyhidden">Comment</span>', 'hair-beauty' ) ),
				$comment_icon . wp_kses_post( __( '% <span class="comment-word visuallyhidden">Comments</span>', 'hair-beauty' ) )
			);
		?>
	</div>
<?php endif; ?>