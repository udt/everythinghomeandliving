<?php

$show = rd_wpv_get_optionb( 'post-meta', 'author' );
if ( $show || is_customize_preview() ) :

?>
	<div class="author wpv-meta-author" <?php WpvTemplates::display_none( $show ) ?>><span class="icon theme"><?php wpv_icon( 'ink-tool' );?></span> <?php the_author_posts_link()?></div>
<?php endif ?>