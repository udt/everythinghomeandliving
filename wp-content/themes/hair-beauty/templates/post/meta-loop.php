<?php
/**
 * Post metadata template
 *
 * @package  wpv
 */

?>
<div class="post-meta">
	<nav class="clearfix">
		<?php get_template_part( 'templates/post/meta/author' ) ?>

		<?php if ( ! post_password_required() ) :  ?>
			<?php get_template_part( 'templates/post/meta/tax' ) ?>
		<?php endif ?>
		<?php include locate_template( 'templates/post/main/part-actions.php' ); ?>
	</nav>
</div>
