<?php
/**
 * Post (in loop) actions - inner part
 *
 * @package wpv
 */

if ( ! (
		( is_single() && current_user_can( 'edit_post', get_the_ID() ) ) ||
		( rd_wpv_get_optionb( 'post-meta', 'comments' ) && comments_open() ) ||
		is_customize_preview()
	)
	)
	return;
?>

<div class="post-actions">
	<?php if ( ! post_password_required() ) :  ?>
		<?php get_template_part( 'templates/post/meta/comments' ); ?>

		<?php if ( ! is_single() ) : ?>
			<?php edit_post_link( '<span class="icon">' . wpv_get_icon( 'pencil' ) . '</span><span class="visuallyhidden">'. esc_html__( 'Edit', 'hair-beauty' ) .'</span>' ) ?>
		<?php endif ?>
	<?php endif ?>
</div>
