<?php


include locate_template( 'templates/post/header.php' );

$meta_author   = rd_wpv_get_optionb( 'post-meta', 'author' );
$meta_date     = rd_wpv_get_optionb( 'post-meta', 'date' );
$meta_comments = rd_wpv_get_optionb( 'post-meta', 'comments' ) && comments_open();

?><div class="post-content-outer single-post">

	<?php if ( $meta_author || $meta_date || $meta_comments || is_customize_preview() ) : ?>
		<div class="meta-top clearfix">
			<?php if ( $meta_author || is_customize_preview() ) : ?>
				<span class="author wpv-meta-author" <?php WpvTemplates::display_none( $meta_author ) ?>><?php the_author_posts_link()?></span>
			<?php endif ?>

			<?php if ( $meta_date || is_customize_preview() ) : ?>
				<span class="post-date wpv-meta-date" itemprop="datePublished" <?php WpvTemplates::display_none( $meta_date ) ?>><?php the_time( get_option( 'date_format' ) ); ?> </span>
			<?php endif ?>

			<?php get_template_part( 'templates/post/meta/comments' ); ?>
		</div>
	<?php endif ?>

	<?php if ( isset( $post_data['media'] ) && ( rd_wpv_get_optionb( 'show-single-post-image' ) || is_customize_preview() ) ) : ?>
		<div class="post-media post-media-image" <?php WpvTemplates::display_none( rd_wpv_get_optionb( 'show-single-post-image' ) ) ?>>
			<div class='media-inner'>
				<?php echo $post_data['media']; // xss ok ?>
			</div>
		</div>
	<?php endif; ?>

	<?php include locate_template( 'templates/post/content.php' ); ?>

	<div class="post-meta">
		<?php get_template_part( 'templates/post/meta/tax' ); ?>
	</div>

	<?php get_template_part( 'templates/share' ); ?>

</div>
