<div class="post-media-date">
	<?php if ( isset( $post_data['media'] ) ) :  ?>
		<div class="thumbnail">
			<?php if ( has_post_format( 'image' ) ) :  ?>
				<a href="<?php the_permalink() ?>" title="<?php the_title_attribute()?>">
					<?php echo $post_data['media']; // xss ok ?>
					<?php echo wpv_get_icon_html( array( 'name' => 'theme-circle-post' ) ); // xss ok ?>
				</a>
			<?php else : ?>
				<?php echo $post_data['media']; // xss ok ?>
			<?php endif ?>
		</div>
	<?php endif; ?>
</div>

<?php if ( $show_content ) :  ?>
	<div class="post-content-wrapper">
		<?php include locate_template( 'templates/post/header.php' ); ?>

		<div class="post-content-outer">
			<?php echo $post_data['content']; // xss ok ?>
		</div>

		<?php get_template_part( 'templates/post/meta/author' ) ?>

		<?php if ( rd_wpv_get_optionb( 'post-meta', 'tax' ) || is_customize_preview() ) : ?>
			<div class="post-content-meta">
				<?php if ( ! post_password_required() ) :  ?>
					<?php get_template_part( 'templates/post/meta/tax' ) ?>
				<?php endif ?>
			</div>
		<?php endif ?>


		<div class="post-actions-wrapper clearfix">
		<?php if ( rd_wpv_get_optionb( 'post-meta', 'date' ) || is_customize_preview() ) : ?>
			<div class="post-date wpv-meta-date" <?php WpvTemplates::display_none( rd_wpv_get_optionb( 'post-meta', 'date' ) ) ?>>
				<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>">
					<?php the_time( get_option( 'date_format' ) ); ?>
				</a>
			</div>
		<?php endif ?>
		<?php if ( ! post_password_required() ) :  ?>
			<?php get_template_part( 'templates/post/meta/comments' ) ?>
			<?php edit_post_link( '<span class="icon">' . wpv_get_icon( 'pencil' ) . '</span><span class="visuallyhidden">'. esc_html__( 'Edit', 'hair-beauty' ) .'</span>' ) ?>
		<?php endif ?>
		</div>


	</div>
<?php endif; ?>
