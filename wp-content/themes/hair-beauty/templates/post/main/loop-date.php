<?php
/**
 * Post (in loop) date
 *
 * @package wpv
 */

$show_date = rd_wpv_get_optionb( 'post-meta', 'date' );

if ( ! $show_date && ! is_customize_preview() ) return;

$title = get_the_title();

?>
<div class="post-row-left wpv-meta-date" <?php WpvTemplates::display_none( $show_date ) ?>>
	<div class="post-date">
		<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>">
			<span class="top-part">
				<?php the_time( 'd' ) ?>
			</span>
			<span class="bottom-part">
				<?php the_time( "m 'y" ) ?>
			</span>
		</a>
	</div>
</div>
