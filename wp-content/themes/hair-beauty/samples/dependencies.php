<?php

/**
 * Declare plugin dependencies
 *
 * @package wpv
 */

/**
 * Declare plugin dependencies
 */
function wpv_register_required_plugins() {
	$plugins = array(
		array(
			'name'     => 'Jetpack',
			'slug'     => 'jetpack',
			'required' => false,
		),

		array(
			'name'     => 'Unplug Jetpack',
			'slug'     => 'unplug-jetpack',
			'required' => false,
		),

		array(
			'name'     => 'WP Retina 2x',
			'slug'     => 'wp-retina-2x',
			'required' => false,
		),

		array(
			'name'     => 'WooCommerce',
			'slug'     => 'woocommerce',
			'required' => false,
		),

		array(
			'name'     => 'Max Mega Menu',
			'slug'     => 'megamenu',
			'required' => false,
		),

		array(
			'name'     => 'Instagram Feed',
			'slug'     => 'instagram-feed',
			'required' => false,
		),

		array(
			'name'     => 'Ninja Forms',
			'slug'     => 'ninja-forms',
			'required' => false,
		),

		array(
			'name'     => 'Google Maps Easy',
			'slug'     => 'google-maps-easy',
			'required' => false,
		),

		array(
			'name'     => 'MailChimp for WordPress',
			'slug'     => 'mailchimp-for-wp',
			'required' => false,
		),

		array(
			'name'     => 'Under Construction / Maintenance Mode from Acurax',
			'slug'     => 'coming-soon-maintenance-mode-from-acurax',
			'required' => false,
		),

		// array(
		// 	'name'     => 'Ninja Forms Layout Master',
		// 	'slug'     => 'ninja-forms-layout-master',
		// 	'source'   => WPV_PLUGINS . 'ninja-forms-layout-master.zip',
		// 	'required' => false,
		// ),

		array(
			'name'     => 'Vamtam Elements (A)',
			'slug'     => 'vamtam-elements-a',
			'source'   => WPV_PLUGINS . 'vamtam-elements-a.zip',
			'required' => true,
			'version'  => '1.0.1',
		),

		array(
			'name'     => 'Vamtam Twitter',
			'slug'     => 'vamtam-twitter',
			'source'   => WPV_PLUGINS . 'vamtam-twitter.zip',
			'required' => false,
			'version'  => '1.0.3',
		),

		array(
			'name'     => 'Vamtam Importers',
			'slug'     => 'vamtam-importers',
			'source'   => WPV_PLUGINS . 'vamtam-importers.zip',
			'required' => false,
			'version'  => '2.0.0',
		),

		array(
			'name'     => 'Revolution Slider',
			'slug'     => 'revslider',
			'source'   => WPV_PLUGINS . 'revslider.zip',
			'required' => false,
			'version'  => '5.1.6',
		),

		array(
			'name'     => 'Booked',
			'slug'     => 'booked',
			'source'   => 'http://boxyupdates.com/get/?action=download&slug=booked',
			'required' => false,
			'version'  => '1.7.12',
			'external_url' => 'http://boxyupdates.com/get/?action=download&slug=booked',
		),
	);

	$config = array(
		'default_path' => '',    // Default absolute path to pre-packaged plugins
		'is_automatic' => true,  // Automatically activate plugins after installation or not
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'wpv_register_required_plugins' );
