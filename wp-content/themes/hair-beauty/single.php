<?php
/**
 * Single post template
 *
 * @package wpv
 * @subpackage hair
 */

get_header();

?>

<?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>

		<div class="row page-wrapper">
			<?php WpvTemplates::$in_page_wrapper = true; ?>

			<article <?php post_class( 'single-post-wrapper '.WpvTemplates::get_layout() )?>>
				<?php
					global $wpv_has_header_sidebars;
					if ( $wpv_has_header_sidebars ) {
						WpvTemplates::header_sidebars();
					}
				?>
				<div class="page-content loop-wrapper clearfix full">
					<?php get_template_part( 'templates/post' ); ?>
					<div class="clearboth">
						<?php comments_template(); ?>
					</div>
				</div>
			</article>

			<?php get_template_part( 'sidebar' ) ?>
		</div>

		<?php if ( ( rd_wpv_get_optionb( 'show-related-posts' ) || is_customize_preview() ) && is_singular( 'post' ) && class_exists( 'WPV_Blog' ) ) : ?>
			<?php if ( ! class_exists( 'WPV_Columns' ) || WPV_Columns::had_limit_wrapper() ) :  ?>
				</div>
			<?php endif ?>
			<?php
				$terms = array();
				$cats  = get_the_category();
				foreach ( $cats as $cat ) {
					$terms[] = $cat->term_id;
				}
			?>
			<div class="related-posts row vamtam-related-content" <?php WpvTemplates::display_none( rd_wpv_get_optionb( 'show-related-posts' ) ) ?>>
				<div class="clearfix limit-wrapper">
					<div class="grid-1-1">
						<?php echo wp_kses_post( apply_filters( 'wpv_related_posts_title', '<h2 class="related-content-title">' . rd_wpv_get_option( 'related-posts-title' ) . '</h2>' ) ); ?>
						<?php
							echo WPV_Blog::shortcode( array( // xss ok
								'count'        => 8,
								'column'       => 4,
								'cat'          => $terms,
								'layout'       => 'scroll-x',
								'show_content' => true,
								'post__not_in' => get_the_ID(),
							) );
						?>
					</div>
				</div>
			</div>
			<?php if ( ! class_exists( 'WPV_Columns' ) || WPV_Columns::had_limit_wrapper() ) :  ?>
				<div class="limit-wrapper">
			<?php endif ?>
		<?php endif ?>
	<?php endwhile;
endif;

get_footer();
