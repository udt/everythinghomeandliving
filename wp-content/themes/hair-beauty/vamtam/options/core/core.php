<?php

/**
 * Controls attached to core sections
 *
 * @package wpv
 * @subpackage hair
 */


return array(
	array(
		'label'     => esc_html__( 'Header Logo Type', 'hair-beauty' ),
		'id'        => 'header-logo-type',
		'type'      => 'switch',
		'transport' => 'postMessage',
		'section'   => 'title_tagline',
		'choices'   => array(
			'image'      => esc_html__( 'Image', 'hair-beauty' ),
			'site-title' => esc_html__( 'Site Title', 'hair-beauty' ),
		),
	),

	array(
		'label'       => esc_html__( 'Custom Logo Picture', 'hair-beauty' ),
		'description' => esc_html__( 'Please Put a logo which exactly twice the width and height of the space that you want the logo to occupy. The real image size is used for retina displays.', 'hair-beauty' ),
		'id'          => 'custom-header-logo',
		'type'        => 'image',
		'transport'   => 'postMessage',
		'section'     => 'title_tagline',
	),

	array(
		'label'       => esc_html__( 'Alternative Logo', 'hair-beauty' ),
		'description' => esc_html__( 'This logo is used when you are using the transparent sticky header. It must be the same size as the main logo.', 'hair-beauty' ),
		'id'          => 'custom-header-logo-transparent',
		'type'        => 'image',
		'transport'   => 'postMessage',
		'section'     => 'title_tagline',
	),

	array(
		'label'     => esc_html__( 'Splash Screen Logo', 'hair-beauty' ),
		'id'        => 'splash-screen-logo',
		'type'      => 'image',
		'transport' => 'postMessage',
		'section'   => 'title_tagline',
	),
);