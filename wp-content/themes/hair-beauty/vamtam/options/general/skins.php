<?php

/**
 * Theme options / Styles / Skins
 *
 * @package wpv
 * @subpackage hair
 */

return array(
	array(
		'label'       => esc_html__( 'Export/Import Skins', 'hair-beauty' ),
		'description' => esc_html__( 'If you use the same name as a previously saved skin it will overwrite the latter.', 'hair-beauty' ),
		'type'        => 'skins',
		'id'          => 'export-import-skins',
	),
);
