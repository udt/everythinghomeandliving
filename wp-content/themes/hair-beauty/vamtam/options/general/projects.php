<?php

/**
 * Theme options / General / Projects
 *
 * @package wpv
 * @subpackage hair
 */

return array(
	array(
		'label'       => esc_html__( 'Show "Related Projects" in Single Project View', 'hair-beauty' ),
		'description' => esc_html__( 'Enabling this option will show more projects from the same type in the single project.', 'hair-beauty' ),
		'id'          => 'show-related-portfolios',
		'type'        => 'switch',
		'transport'   => 'postMessage',
	),

	array(
		'label'     => esc_html__( '"Related Projects" title', 'hair-beauty' ),
		'id'        => 'related-portfolios-title',
		'type'      => 'text',
		'transport' => 'postMessage',
	),
);