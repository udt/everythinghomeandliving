<?php

/**
 * Theme options / General / Posts
 *
 * @package wpv
 * @subpackage hair
 */

return array(

	array(
		'label'       => esc_html__( 'Pagination Type', 'hair-beauty' ),
		'description' => esc_html__( 'Also used for portfolio', 'hair-beauty' ),
		'id'          => 'pagination-type',
		'type'        => 'select',
		'choices'     => array(
			'paged'              => esc_html__( 'Paged', 'hair-beauty' ),
			'load-more'          => esc_html__( 'Load more button', 'hair-beauty' ),
			'infinite-scrolling' => esc_html__( 'Infinite scrolling', 'hair-beauty' ),
		),
	),

	array(
		'label'       => esc_html__( 'Show "Related Posts" in Single Post View', 'hair-beauty' ),
		'description' => esc_html__( 'Enabling this option will show more posts from the same category when viewing a single post.', 'hair-beauty' ),
		'id'          => 'show-related-posts',
		'type'        => 'switch',
		'transport'   => 'postMessage',
	),

	array(
		'label'     => esc_html__( '"Related Posts" title', 'hair-beauty' ),
		'id'        => 'related-posts-title',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Meta Information', 'hair-beauty' ),
		'id'        => 'post-meta',
		'type'      => 'multicheck',
		'transport' => 'postMessage',
		'choices'   => array(
			'author'   => esc_html__( 'Post Author', 'hair-beauty' ),
			'tax'      => esc_html__( 'Categories and Tags', 'hair-beauty' ),
			'date'     => esc_html__( 'Timestamp', 'hair-beauty' ),
			'comments' => esc_html__( 'Comment Count', 'hair-beauty' ),
		),
	),

	array(
		'label'       => esc_html__( 'Show Featured Image on Single Posts', 'hair-beauty' ),
		'id'          => 'show-single-post-image',
		'description' => esc_html__( 'Please note, that this option works only for Blog Post Format Image.', 'hair-beauty' ),
		'type'        => 'switch',
		'transport'   => 'postMessage',
	),

	array(
		'label'       => esc_html__( 'Post Archive Layout', 'hair-beauty' ),
		'description' => '',
		'id'          => 'archive-layout',
		'type'        => 'radio',
		'choices'     => array(
			'normal' => esc_html__( 'Large', 'hair-beauty' ),
			'mosaic' => esc_html__( 'Small', 'hair-beauty' ),
		),
		'static'   => true,
	),

);
