<?php

global $vamtam_theme_customizer;

$thispath = WPV_OPTIONS . 'general/';

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'General', 'hair-beauty' ),
	'description' => '',
	'id'          => 'general',
) );

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'General', 'hair-beauty' ),
	'description' => '',
	'id'          => 'general-general',
	'subsection'  => true,
	'fields'      => include $thispath . 'general.php',
) );

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'Posts', 'hair-beauty' ),
	'description' => '',
	'id'          => 'general-posts',
	'subsection'  => true,
	'fields'      => include $thispath . 'posts.php',
) );

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'Projects', 'hair-beauty' ),
	'description' => '',
	'id'          => 'general-projects',
	'subsection'  => true,
	'fields'      => include $thispath . 'projects.php',
) );

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'Skins', 'hair-beauty' ),
	'description' => wp_kses_post( __( "You can import one of the theme's skins or create your own.<br> Please not that the options in General and Layout will not be saved and they will be the same for every skin.", 'hair-beauty' ) ),
	'id'          => 'styles-skins',
	'subsection'  => true,
	'fields'      => include $thispath . 'skins.php',
) );
