<?php

/**
 * Theme options / Footer
 *
 * @package wpv
 * @subpackage hair
 */

$horizontal_sidebar_widths = array(
	'cell-1-1' => esc_html__( 'Full', 'hair-beauty' ),
	'cell-1-2' => '1/2',
	'cell-1-3' => '1/3',
	'cell-1-4' => '1/4',
	'cell-1-5' => '1/5',
	'cell-1-6' => '1/6',
	'cell-2-3' => '2/3',
	'cell-3-4' => '3/4',
	'cell-2-5' => '2/5',
	'cell-3-5' => '3/5',
	'cell-4-5' => '4/5',
	'cell-5-6' => '5/6',
);

return array(

	array(
		'label'     => esc_html__( 'Sticky Footer', 'hair-beauty' ),
		'id'        => 'sticky-footer',
		'type'      => 'switch',
		'transport' => 'postMessage',
	),

	array(
		'label'   => esc_html__( 'Show Footer on One Page Template', 'hair-beauty' ),
		'id'      => 'one-page-footer',
		'type'    => 'switch',
	),

	array(
		'label'     => esc_html__( 'Full Width Footer', 'hair-beauty' ),
		'id'        => 'full-width-footer',
		'type'      => 'switch',
		'transport' => 'postMessage',
	),

	array(
		'label'  => esc_html__( 'Widget Areas', 'hair-beauty' ),
		'type'   => 'heading',
		'id'     => 'layout-footer-horizontal-sidebars',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 1 ),
		'id'        => 'footer-sidebars-1-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 2 ),
		'id'        => 'footer-sidebars-2-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 3 ),
		'id'        => 'footer-sidebars-3-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 4 ),
		'id'        => 'footer-sidebars-4-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 5 ),
		'id'        => 'footer-sidebars-5-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 6 ),
		'id'        => 'footer-sidebars-6-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 7 ),
		'id'        => 'footer-sidebars-7-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'label'     => sprintf( esc_html__( 'Widget Area %d', 'hair-beauty' ), 8 ),
		'id'        => 'footer-sidebars-8-width',
		'type'      => 'select',
		'choices'   => $horizontal_sidebar_widths,
		'transport' => 'postMessage',
	),

	array(
		'id'    => 'footer-bg-title',
		'label' => esc_html__( 'Backgrounds', 'hair-beauty' ),
		'type'  => 'heading',
	),

	array(
		'label'       => esc_html__( 'Widget Areas Background', 'hair-beauty' ),
		'description' => esc_html__( 'If you want to use an image as a background, enabling the cover button will resize and crop the image so that it will always fit the browser window on any resolution. If the color opacity  is less than 1 the page background underneath will be visible.', 'hair-beauty' ),
		'id'          => 'footer-background',
		'type'        => 'background',
		'transport'   => 'postMessage',
		'skin'        => true,
	),

	array(
		'label'     => esc_html__( 'Hide the Background Image on Lower Resolutions', 'hair-beauty' ),
		'id'        => 'footer-background-hide-lowres',
		'type'      => 'switch',
		'transport' => 'postMessage',
		'skin'      => true,
	),

	array(
		'id'    => 'footer-typography-title',
		'label' => esc_html__( 'Typography', 'hair-beauty' ),
		'type'  => 'heading',
	),

	array(
		'label'       => esc_html__( 'Widget Areas Text', 'hair-beauty' ),
		'description' => esc_html__( 'This is the general font used for the footer widgets.', 'hair-beauty' ),
		'id'          => 'footer-sidebars-font',
		'type'        => 'typography',
		'compiler'    => true,
		'transport'   => 'postMessage',
		'skin'        => true,
	),

	array(
		'label'       => esc_html__( 'Widget Areas Titles', 'hair-beauty' ),
		'description' => esc_html__( 'Please note that this option will override the general headings style set in the General Typography" tab.', 'hair-beauty' ),
		'id'          => 'footer-sidebars-titles',
		'type'        => 'typography',
		'compiler'    => true,
		'transport'   => 'postMessage',
		'skin'        => true,
	),

	array(
		'label'   => esc_html__( 'Links', 'hair-beauty' ),
		'type'    => 'color-row',
		'id'      => 'footer-link',
		'choices' => array(
			'regular' => esc_html__( 'Regular:', 'hair-beauty' ),
			'hover'   => esc_html__( 'Hover:', 'hair-beauty' ),
			'visited' => esc_html__( 'Visited:', 'hair-beauty' ),
			'active'  => esc_html__( 'Active:', 'hair-beauty' ),
		),
		'compiler'  => true,
		'transport' => 'postMessage',
		'skin'      => true,
	),

);
