<?php

return array(
	array(
		'label'     => esc_html__( 'Layout', 'hair-beauty' ),
		'id'        => 'top-bar-layout',
		'type'      => 'select',
		'transport' => 'postMessage',
		'choices'   => array(
			''            => esc_html__( 'Disabled', 'hair-beauty' ),
			'menu-social' => esc_html__( 'Left: Menu, Right: Social Icons', 'hair-beauty' ),
			'social-menu' => esc_html__( 'Left: Social Icons, Right: Menu', 'hair-beauty' ),
			'text-menu'   => esc_html__( 'Left: Text, Right: Menu', 'hair-beauty' ),
			'menu-text'   => esc_html__( 'Left: Menu, Right: Text', 'hair-beauty' ),
			'social-text' => esc_html__( 'Left: Social Icons, Right: Text', 'hair-beauty' ),
			'text-social' => esc_html__( 'Left: Text, Right: Social Icons', 'hair-beauty' ),
			'fulltext'    => esc_html__( 'Text only', 'hair-beauty' ),
		),
	),

	array(
		'label'       => esc_html__( 'Text', 'hair-beauty' ),
		'description' => esc_html__( 'You can place plain text, HTML and shortcodes.', 'hair-beauty' ),
		'id'          => 'top-bar-text',
		'type'        => 'textarea',
		'transport'   => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Social Text Lead', 'hair-beauty' ),
		'id'        => 'top-bar-social-lead',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Facebook Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-fb',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Twitter Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-twitter',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'LinkedIn Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-linkedin',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Google+ Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-gplus',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Flickr Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-flickr',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Pinterest Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-pinterest',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Dribbble Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-dribbble',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Instagram Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-instagram',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'YouTube Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-youtube',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Vimeo Link', 'hair-beauty' ),
		'id'        => 'top-bar-social-vimeo',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'       => esc_html__( 'Background', 'hair-beauty' ),
		'description' => wp_kses_post( __( 'If you want to use an image as a background, enabling the cover button will resize and crop the image so that it will always fit the browser window on any resolution.<br> If the color opacity is less than 1 the page background underneath will be visible.', 'hair-beauty' ) ),
		'id'          => 'top-nav-background',
		'type'        => 'background',
		'transport'   => 'postMessage',
		'skin'        => true,
		'show'        => array(
			'background-attachment' => false,
			'background-position'   => false,
		),
	),

	array(
		'label'   => esc_html__( 'Colors', 'hair-beauty' ),
		'type'    => 'color-row',
		'id'      => 'css-tophead',
		'choices' => array(
			'text-color'       => esc_html__( 'Text Color:', 'hair-beauty' ),
			'link-color'       => esc_html__( 'Link Color:', 'hair-beauty' ),
			'link-hover-color' => esc_html__( 'Link Hover Color:', 'hair-beauty' ),
		),
		'compiler'  => true,
		'transport' => 'postMessage',
		'skin'      => true,
	),

	array(
		'label'   => esc_html__( 'Sub-Menus', 'hair-beauty' ),
		'type'    => 'color-row',
		'id'      => 'submenu',
		'choices' => array(
			'background'  => esc_html__( 'Background:', 'hair-beauty' ),
			'color'       => esc_html__( 'Text Normal Color:', 'hair-beauty' ),
			'hover-color' => esc_html__( 'Text Hover Color:', 'hair-beauty' ),
		),
		'compiler'  => true,
		'transport' => 'postMessage',
		'skin'      => true,
	),
);