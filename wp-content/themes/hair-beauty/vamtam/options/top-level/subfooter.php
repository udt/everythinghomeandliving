<?php

/**
 * Theme options / Sub-footer
 *
 * @package wpv
 * @subpackage hair
 */

return array(

	array(
		'label'  => esc_html__( 'Layout', 'hair-beauty' ),
		'type'   => 'heading',
		'id'     => 'subfooter-section',
	),

	array(
		'label'     => esc_html__( 'Full Width Sub-footer', 'hair-beauty' ),
		'id'        => 'full-width-subfooter',
		'type'      => 'switch',
		'transport' => 'postMessage',
	),

	array(
		'label'       => esc_html__( 'Text Area in Footer (left)', 'hair-beauty' ),
		'description' => esc_html__( 'You can place text/HTML or any shortcode in this field. The text will appear in the  footer of your website.', 'hair-beauty' ),
		'id'          => 'subfooter-left',
		'type'        => 'textarea',
		'transport'   => 'postMessage',
	),

	array(
		'label'       => esc_html__( 'Text Area in Footer (center)', 'hair-beauty' ),
		'description' => esc_html__( 'You can place text/HTML or any shortcode in this field. The text will appear in the  footer of your website.', 'hair-beauty' ),
		'id'          => 'subfooter-center',
		'type'        => 'textarea',
		'transport'   => 'postMessage',
	),

	array(
		'label'       => esc_html__( 'Text Area in Footer (right)', 'hair-beauty' ),
		'description' => esc_html__( 'You can place text/HTML or any shortcode in this field. The text will appear in the  footer of your website.', 'hair-beauty' ),
		'id'          => 'subfooter-right',
		'type'        => 'textarea',
		'transport'   => 'postMessage',
	),

	array(
		'id'    => 'subfooter-bg-title',
		'label' => esc_html__( 'Backgrounds', 'hair-beauty' ),
		'type'  => 'heading',
	),

	array(
		'label'       => esc_html__( 'Sub-footer Background', 'hair-beauty' ),
		'description' => esc_html__( 'If you want to use an image as a background, enabling the cover button will resize and crop the image so that it will always fit the browser window on any resolution.', 'hair-beauty' ),
		'id'          => 'subfooter-background',
		'type'        => 'background',
		'transport'   => 'postMessage',
		'skin'        => true,
		'show'        => array(
			'background-attachment' => false,
			'background-position'   => false,
		),
	),

	array(
		'id'    => 'subfooter-typography-title',
		'label' => esc_html__( 'Typography', 'hair-beauty' ),
		'type'  => 'heading',
	),

	array(
		'label'       => esc_html__( 'Sub-footer', 'hair-beauty' ),
		'description' => esc_html__( 'You can place your text/HTML in the General Settings option page.', 'hair-beauty' ),
		'id'          => 'sub-footer',
		'type'        => 'typography',
		'compiler'    => true,
		'transport'   => 'postMessage',
		'skin'        => true,
	),

);
