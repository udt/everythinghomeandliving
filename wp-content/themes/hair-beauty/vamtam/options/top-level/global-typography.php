<?php
/**
 * Theme options / Styles / General Typography
 *
 * @package wpv
 * @subpackage hair
 */

return array(

array(
	'label'  => esc_html__( 'Headlines', 'hair-beauty' ),
	'type'   => 'heading',
	'id'     => 'styles-typography-headlines',
),

array(
	'label'      => esc_html__( 'H1', 'hair-beauty' ),
	'id'         => 'h1',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'      => esc_html__( 'H2', 'hair-beauty' ),
	'id'         => 'h2',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'      => esc_html__( 'H3', 'hair-beauty' ),
	'id'         => 'h3',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'      => esc_html__( 'H4', 'hair-beauty' ),
	'id'         => 'h4',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'      => esc_html__( 'H5', 'hair-beauty' ),
	'id'         => 'h5',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'      => esc_html__( 'H6', 'hair-beauty' ),
	'id'         => 'h6',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'  => esc_html__( 'Additional Fonts', 'hair-beauty' ),
	'type'   => 'heading',
	'id'     => 'styles-typography-additional',
),

array(
	'label'      => esc_html__( 'Emphasis Font', 'hair-beauty' ),
	'id'         => 'em',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'      => esc_html__( 'Style 1', 'hair-beauty' ),
	'id'         => 'additional-font-1',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'      => esc_html__( 'Style 2', 'hair-beauty' ),
	'id'         => 'additional-font-2',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
	'skin'       => true,
),

array(
	'label'  => esc_html__( 'Google Fonts Options', 'hair-beauty' ),
	'type'   => 'heading',
	'id'     => 'styles-typography-gfonts',
),

array(
	'label'      => esc_html__( 'Style 2', 'hair-beauty' ),
	'id'         => 'gfont-subsets',
	'type'       => 'multicheck',
	'transport'  => 'postMessage',
	'choices'    => wpv_get_google_fonts_subsets(),
	'skin'       => true,
),

);
