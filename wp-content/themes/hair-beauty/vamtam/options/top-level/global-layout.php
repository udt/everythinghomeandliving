<?php

/**
 * Theme options / Layout / General
 *
 * @package wpv
 * @subpackage hair
 */

return array(

	array(
		'label'       => esc_html__( 'Layout Type', 'hair-beauty' ),
		'description' => esc_html__( 'Please note that in full width layout mode, the body background option found in Styles - Body, acts as page background.', 'hair-beauty' ),
		'id'          => 'site-layout-type',
		'type'        => 'radio',
		'choices'     => array(
			'boxed' => esc_html__( 'Boxed', 'hair-beauty' ),
			'full'  => esc_html__( 'Full width', 'hair-beauty' ),
		),
		'static' => true,
	),

	array(
		'label'       => esc_html__( 'Maximum Page Width', 'hair-beauty' ),
		'description' => wp_kses_post( sprintf( __( 'If you have changed this option, please use the <a href="%s" title="Regenerate thumbnails" target="_blank">Regenerate thumbnails</a> plugin in order to update your images.', 'hair-beauty' ), 'http://wordpress.org/extend/plugins/regenerate-thumbnails/' ) ),
		'id'          => 'site-max-width',
		'type'        => 'radio',
		'choices'     => array(
			960  => '960px',
			1140 => '1140px',
			1260 => '1260px',
			1400 => '1400px',
		),
		'compiler' => true,
		'static'   => true,
	),

);
