<?php

function wpv_silent_fs() {
	add_filter( 'filesystem_method', 'wpv_filesystem_method_direct' );
}

function wpv_normal_fs() {
	remove_filter( 'filesystem_method', 'wpv_filesystem_method_direct' );
}

function wpv_filesystem_method_direct( $method ) {
	return 'direct';
}

function wpv_silent_get_contents( $path ) {
	global $wp_filesystem;

	wpv_silent_fs();

	if ( empty( $wp_filesystem ) ) {
		require_once ABSPATH . '/wp-admin/includes/file.php';
		WP_Filesystem();
	}

	$contents = $wp_filesystem->get_contents( $path );

	wpv_normal_fs();

	return $contents;
}

function wpv_silent_put_contents( $path, $contents ) {
	global $wp_filesystem;

	wpv_silent_fs();

	if ( empty( $wp_filesystem ) ) {
		require_once ABSPATH . '/wp-admin/includes/file.php';
		WP_Filesystem();
	}

	$ret = $wp_filesystem->put_contents( $path, $contents );

	wpv_normal_fs();

	return $ret;
}