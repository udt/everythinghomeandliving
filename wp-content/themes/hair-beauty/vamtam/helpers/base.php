<?php

function wpv_get_the_ID() {
	global $post;

	return (wpv_has_woocommerce() && is_woocommerce() && ! is_singular( array( 'page', 'product' ) )) ? wc_get_page_id( 'shop' ) : (isset( $post ) ? $post->ID : null);
}

/**
 * Wrapper around get_post_meta which takes special pages into account
 *
 * @uses get_post_meta()
 *
 * @param  int    $post_id Post ID.
 * @param  string $key     Optional. The meta key to retrieve. By default, returns data for all keys.
 * @param  bool   $single  Whether to return a single value.
 * @return mixed           Will be an array if $single is false. Will be value of meta data field if $single is true.
 */
function wpv_post_meta( $post_id, $meta = '', $single = false ) {
	$real_id = wpv_get_the_ID();

	if ($real_id && $post_id != $real_id)
		$post_id = $real_id;

	return get_post_meta( $post_id, $meta, $single );
}

/**
 * helper function - returns second argument when the first is empty, otherwise returns the first
 *
 */
function wpv_default( $value, $default ) {
	if (empty( $value ))
		return $default;
	return $value;
}

/*
 * gets the width in px of the central column depending on current post settings
 */

if ( ! function_exists( 'wpv_get_central_column_width' ) ) :
function wpv_get_central_column_width() {
	global $post, $content_width;

	$layout_type = WpvTemplates::get_layout();

	$central_width = $content_width;
	$left_sidebar = (float) rd_wpv_get_option( 'left-sidebar-width' );
	$right_sidebar = (float) rd_wpv_get_option( 'right-sidebar-width' );

	switch ( $layout_type ) {
		case 'left-only':
			$central_width = floor( (100 -$left_sidebar) / 100 * $central_width );
		break;

		case 'right-only':
			$central_width = floor( (100 -$right_sidebar) / 100 * $central_width );
		break;

		case 'left-right':
			$central_width = floor( (100 -$left_sidebar -$right_sidebar) / 100 * $central_width );
		break;
	}

	$column = array( 1,1 );

	if ( isset( $GLOBALS['wpv_column_stack'] ) && is_array( $GLOBALS['wpv_column_stack'] ) ) {
		foreach ( $GLOBALS['wpv_column_stack'] as $c ) {
			$c = explode( '/', $c );
			$column[0] *= $c[0];
			$column[1] *= $c[1];
		}
	}

	$column = $column[0] / $column[1];

	return round( $central_width * $column );
}
endif;

function wpv_get_portfolio_options() {
	global $post;

	$res = array();

	$res['image'] = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'full', true );
	$res['type']  = wpv_default( get_post_meta( get_the_id(), 'portfolio_type', true ), 'image' );

	$res['link_target'] = '_self';

	// calculate some options depending on the project's type
	if ( $res['type'] == 'image' || $res['type'] == 'html' ) {
		$res['href'] = $res['image'][0];
	} elseif ( $res['type'] == 'video' ) {
		$res['href'] = get_post_meta( get_the_id(), 'wpv-portfolio-format-video', true );

		if (empty( $res['href'] ))
			$res['href'] = $res['image'][0];
	} elseif ( $res['type'] == 'link' ) {
		$res['href'] = get_post_meta( get_the_ID(), 'wpv-portfolio-format-link', true );

		$res['link_target'] = get_post_meta( get_the_ID(), '_link_target', true );
		$res['link_target'] = $res['link_target'] ? $res['link_target'] : '_self';
	} elseif ( $res['type'] == 'gallery' ) {
		list($res['gallery'], ) = WpvPostFormats::get_first_gallery( get_the_content(), null, 'theme-loop-4' );
	} elseif ( $res['type'] == 'document' ) {
		if ( is_single() ) {
			$res['href'] = $res['image'][0];
		} else {
			$res['href'] = get_permalink();
		}
	}

	return $res;
}

function wpv_custom_js() {
	$custom_js = rd_wpv_get_option( 'custom-js' );

	if ( ! empty( $custom_js ) ) :
?>
	<script><?php echo $custom_js; // xss ok ?></script>
<?php
	endif;
}
add_action( 'wp_footer', 'wpv_custom_js', 10000 );

function wpv_get_attachment_file( $src ) {
	$attachment_id = attachment_url_to_postid( $src );
	$upload_dir    = wp_upload_dir();

	if ( $attachment_id !== false && wp_attachment_is_image( $attachment_id ) ) {
		$file = get_attached_file( $attachment_id );

		$file = preg_replace( '/^('. preg_quote( $upload_dir['basedir'] . '/', '/' ) .')?/', $upload_dir['basedir'].'/', $file );

		return $file;
	}

	return str_replace( $upload_dir['baseurl'], $upload_dir['basedir'], $src );
}

function wpv_url_to_image( $src, $size = 'full', $attr = '', $unlimited = false ) {
	$attachment_id = attachment_url_to_postid( $src );

	if ( $attachment_id !== false && wp_attachment_is_image( $attachment_id ) ) {
		if ( $unlimited ) {
			WpvOverrides::unlimited_image_sizes();
		}

		echo wp_get_attachment_image( $attachment_id, $size, $attr );

		if ( $unlimited ) {
			WpvOverrides::limit_image_sizes();
		}
	} else {
		// fallback, typically used on fresly imported demo content

		echo '<img src="' . esc_url( $src ) . '" alt="" />';
	}
}

function wpv_prepare_url( $url ) {
	while ( preg_match( '#/[-\w]+/\.\./#', $url ) ) {
		$url = preg_replace( '#/[-\w]+/\.\./#', '/', $url );
	}

	return $url;
}

function wpv_sanitize_portfolio_item_type( $type ) {
	if ($type == 'gallery' || $type == 'video' || $type == 'image')
		return $type;

	return 'image';
}
add_filter( 'wpv_fancy_portfolio_item_type', 'wpv_sanitize_portfolio_item_type' );

function wpv_fix_shortcodes( $content ) {
	// array of custom shortcodes requiring the fix
	$block = join( '|', apply_filters( 'wpv_escaped_shortcodes', include( WPV_THEME_METABOXES . 'shortcode.php' ) ) );

	// opening tag
	$rep = preg_replace( "/(<p>\s*)?\[($block)(\s[^\]]+)?\](\s*<\/p>|<br \/>)?/",'[$2$3]', $content );

	// closing tag
	$rep = preg_replace( "/(?:<\/p>\n*)?(?:<p>\s*)?\[\/($block)](?:\s*<\/p>|<br \/>)?/",'[/$1]', $rep );

	return $rep;
}
add_filter( 'the_content', 'wpv_fix_shortcodes' );

function wpv_get_portfolio_terms() {
	$terms_slug = $terms_name = array();

	if ( class_exists( 'Jetpack_Portfolio' ) ) {
		$terms = get_the_terms( get_the_id(), Jetpack_Portfolio::CUSTOM_TAXONOMY_TYPE );

		if ( is_array( $terms ) ) {
			foreach ( $terms as $term ) {
				$terms_slug[] = preg_replace( '/[\pZ\pC]+/u', '-', $term->slug );
				$terms_name[] = $term->name;
			}
		}
	}

	return array( $terms_slug, $terms_name );
}

function wpv_recursive_preg_replace( $regex, $replace, $subject ) {
	if ( is_array( $subject ) || is_object( $subject ) ) {
		foreach ( $subject as &$sub ) {
			$sub = wpv_recursive_preg_replace( $regex, $replace, $sub );
		}
		unset( $sub );
	}
	if ( is_string( $subject ) ) {
		$subject = preg_replace( $regex, $replace, $subject );
	}
	return $subject;
}

function wpv_get_google_fonts_subsets() {
	global $wpv_fonts;

	$subsets = array();

	foreach ( $wpv_fonts as $font ) {
		if ( isset( $font['gf'] ) && $font['gf'] ) {
			$subsets = array_merge( $subsets, $font['subsets'] );
		}
	}

	sort( $subsets );

	return array_combine( $subsets, $subsets );
}

function wpv_get_fonts_by_family() {
	global $wpv_fonts, $wpv_fonts_by_family;

	if ( ! isset( $wpv_fonts_by_family ) ) {
		$wpv_fonts_by_family = array();

		foreach ( $wpv_fonts as $id => $font ) {
			$wpv_fonts_by_family[ $font['family'] ] = $id;
		}
	}

	return $wpv_fonts_by_family;
}

function wpv_use_accent_preview() {
	return ! ( ( isset( $GLOBALS['is_IE'] ) && $GLOBALS['is_IE'] ) || ( isset( $GLOBALS['is_edge'] ) && $GLOBALS['is_edge'] ) ); // IE and Edge do not support CSS variables and do not intend to any time soon (comment added 9 June 2016)
}