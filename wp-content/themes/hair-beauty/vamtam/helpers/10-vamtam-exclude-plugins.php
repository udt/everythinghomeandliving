<?php

function wpv_exclude_plugins( $plugins ) {
	if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX || ! isset( $_POST['action'] ) || 'wpv-compile-less' !== $_POST['action'] ) {
		return $plugins;
	}

	return array();
}

add_filter( 'option_active_plugins', 'wpv_exclude_plugins' );