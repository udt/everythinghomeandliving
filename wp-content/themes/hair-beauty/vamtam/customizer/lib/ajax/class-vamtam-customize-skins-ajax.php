<?php

class Vamtam_Customize_Skins_Ajax {
	public $prefix = 'theme';

	private $options;

	public function __construct( $options ) {
		$this->options = $options;
	}

	private function respond_with_nonce( $data ) {
		header( 'Content-Type: application/json' );

		echo json_encode( array(
			'data' => $data,
			'nonce' => wp_create_nonce( 'vamtam-skins-nonce' ),
		) );

		exit;
	}

	public function ajax_import() {
		global $wpv_fonts;

		check_ajax_referer( 'vamtam-skins-nonce', 'nonce' );

		update_option( 'vamtam-last-skin', str_replace( $this->prefix . '_', '', $_POST['file'] ), false );

		$options = $this->full_skin( wpv_silent_get_contents( WPV_SAVED_OPTIONS . $_POST['file'] ) );

		// build Google fonts URL

		$fonts_by_family = wpv_get_fonts_by_family();

		$google_fonts = array();

		$fields  = $GLOBALS['vamtam_theme_customizer']->get_fields_by_id();

		foreach ( $fields as $id => $field ) {
			$full_id = 'vamtam_theme[' . $id . ']';

			// cache google fonts, so we can just load them later
			if ( 'typography' === $field['type'] ) {
				$font_id = $fonts_by_family[ $options[ $id ]['font-family'] ];
				$font    = $wpv_fonts[ $font_id ];

				if ( isset( $font['gf'] ) && $font['gf'] ) {
					$google_fonts[ $font_id ][] = $options[ $id ]['variant'];
				}
			}
		}

		$options['google_fonts'] = Vamtam_Customizer::build_google_fonts_url( $google_fonts, $options['gfont-subsets'] );

		// save options and compile
		$GLOBALS['vamtam_theme_customizer']->set_options( $options );

		WpvLessBridge::compile( $options );

		$this->respond_with_nonce( '<span class="success">'. esc_html__( 'Imported.', 'hair-beauty' ) . '</span>' );
	}

	private function full_skin( $skin ) {
		global $vamtam_theme;

		return array_merge( $vamtam_theme, json_decode( $skin, true ) );
	}

	public function ajax_delete() {
		$_POST['file'] = trim( $_POST['file'] );

		if ( @unlink( WPV_SAVED_OPTIONS . $_POST['file'] ) ) {
			$this->respond_with_nonce( '<span class="success">'. esc_html__( 'Success.', 'hair-beauty' ) . '</span>' );
		}

		$this->respond_with_nonce( '<span class="error">'. esc_html__( 'Cannot delete file.', 'hair-beauty' ) . '</span>' );
	}

	public function ajax_available() {
		check_ajax_referer( 'vamtam-skins-nonce', 'nonce' );

		$skins_dir = opendir( WPV_SAVED_OPTIONS );

		$options = '';
		if ( isset( $_POST['prefix'] ) ) {
			$prefix = $_POST['prefix'].'_';

			$options .= '<option value="">' . esc_html__( 'Available skins', 'hair-beauty' ) . '</option>';
			while ( $file = readdir( $skins_dir ) ) :
				if ( '.' !== $file && '..' !== $file && strpos( $file, $prefix ) === 0 ) :
					$options .= '<option value="' . esc_attr( $file ) . '">' . str_replace( $prefix, '', $file ) . '</option>';
				endif;
			endwhile;

			closedir( $skins_dir );
		}

		$this->respond_with_nonce( $options );
	}

	public function ajax_save() {
		global $vamtam_theme;

		check_ajax_referer( 'vamtam-skins-nonce', 'nonce' );

		$exported_options = array();

		$options = $GLOBALS['vamtam_theme_customizer']->get_fields_by_id();

		foreach ( $options as $id => $option ) {
			if ( isset( $option['skin'] ) && $option['skin'] ) {
				$exported_options[ $id ] = $vamtam_theme[ $id ];
			}
		}

		// No need to escape this, as it's been properly escaped previously and through json_encode
		$content = json_encode( $exported_options );

		$filename = sanitize_title( $_POST['file'] );

		if ( wpv_silent_put_contents( WPV_SAVED_OPTIONS . $filename, $content ) ) {
			$this->respond_with_nonce( '<span class="success">'. esc_html__( 'Success.', 'hair-beauty' ) . '</span>' );
		} else {
			$this->respond_with_nonce( '<span class="error">'. esc_html__( 'Cannot save skin file.', 'hair-beauty' ) . '</span>' );
		}
	}
}