<?php

/**
 * Basic ajax class
 *
 * @package wpv
 */

/**
 * class WpvAjax
 */
class WpvAjax {

	protected $actions;

	/**
	 * Hook ajax actions
	 */
	public function __construct() {
		if ( is_array( $this->actions ) ) {
			foreach ( $this->actions as $hook => $func ) {
				add_action( 'wp_ajax_wpv-'.$hook, array( &$this, $func ) );
			}
		}
	}
}
