<?php
	global $post;

	$video   = isset( $value['video'] ) ? ! ! $value['video'] : false;
	$button  = isset( $value['button'] ) ? $value['button'] : esc_html__( 'Insert', 'hair-beauty' );
	$remove  = isset( $value['remove'] ) ? $value['remove'] : esc_html__( 'Remove', 'hair-beauty' );
	$default = isset( $GLOBALS['wpv_in_metabox'] ) ? get_post_meta( $post->ID, $id, true ) : wpv_get_option( $id, $default );

	$name = $id;
	$id   = preg_replace( '/[^\w]+/', '', $id );
?>

<div class="upload-basic-wrapper <?php echo esc_attr( ! empty( $default ) ? 'active' : '' ) ?>">
	<div class="image-upload-controls <?php if ( $video ) echo 'wpv-video-upload-controls' ?>">
		<input type="text" id="<?php echo esc_attr( $id ) ?>" name="<?php echo esc_attr( $name ) ?>" value="<?php echo esc_attr( $default ) ?>" class="image-upload <?php wpv_static( $value )?> <?php if ( ! $video ) echo 'hidden' ?>" />

		<a class="button wpv-upload-button <?php if ( $video ) echo 'wpv-video-upload' ?>" href="#" data-target="<?php echo esc_attr( $id ) ?>">
			<?php echo esc_html( $button ) ?>
		</a>

		<a class="button wpv-upload-clear <?php if ( empty( $default ) ) echo 'hidden' ?>" href="#" data-target="<?php echo esc_attr( $id ) ?>"><?php echo esc_html( $remove ) ?></a>
		<a class="wpv-upload-undo hidden" href="#" data-target="<?php echo esc_attr( $id ) ?>"><?php echo esc_html__( 'Undo', 'hair-beauty' ) ?></a>
	</div>
	<div id="<?php echo esc_attr( $id ) ?>_preview" class="image-upload-preview <?php if ( $video ) echo 'hidden' ?>">
		<img src="<?php echo esc_url( $default ) ?>" />
	</div>
</div>
