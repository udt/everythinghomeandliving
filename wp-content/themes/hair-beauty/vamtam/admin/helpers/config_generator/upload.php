<?php
/**
 * upload field
 */
?>

<div class="wpv-config-row clearfix <?php echo esc_attr( $class ) ?>">
	<div class="rtitle">
		<h4><?php echo esc_html( $name ) ?></h4>

		<?php wpv_description( $id, $desc ) ?>
	</div>

	<div class="rcontent">
		<?php include WPV_ADMIN_CGEN . 'upload-basic.php' ?>
	</div>
</div>
