<?php

$fields = array(
	'color'      => esc_html__( 'Color:', 'hair-beauty' ),
	'opacity'    => esc_html__( 'Opacity:', 'hair-beauty' ),
	'image'      => esc_html__( 'Image / pattern:', 'hair-beauty' ),
	'repeat'     => esc_html__( 'Repeat:', 'hair-beauty' ),
	'attachment' => esc_html__( 'Attachment:', 'hair-beauty' ),
	'position'   => esc_html__( 'Position:', 'hair-beauty' ),
	'size'       => esc_html__( 'Size:', 'hair-beauty' ),
);

$sep = isset( $sep ) ? $sep : '-';

$current = array();

if ( ! isset( $only ) ) {
	if ( isset( $show ) ) {
		$only = explode( ',', $show );
	} else {
		$only = array();
	}
} else {
	$only = explode( ',', $only );
}

$show = array();

global $post;
foreach ( $fields as $field => $fname ) {
	if ( isset( $GLOBALS['wpv_in_metabox'] ) ) {
		$current[ $field ] = get_post_meta( $post->ID, "$id-$field", true );
	} else {
		$current[ $field ] = wpv_get_option( "$id-$field" );
	}
	$show[ $field ] = ( in_array( $field, $only ) || sizeof( $only ) == 0 );
}

$selects = array(
	'repeat' => array(
		'no-repeat' => esc_html__( 'No repeat', 'hair-beauty' ),
		'repeat-x'  => esc_html__( 'Repeat horizontally', 'hair-beauty' ),
		'repeat-y'  => esc_html__( 'Repeat vertically', 'hair-beauty' ),
		'repeat'    => esc_html__( 'Repeat both', 'hair-beauty' ),
	),
	'attachment' => array(
		'scroll' => esc_html__( 'scroll', 'hair-beauty' ),
		'fixed'  => esc_html__( 'fixed', 'hair-beauty' ),
	),
	'position' => array(
		'left center'   => esc_html__( 'left center', 'hair-beauty' ),
		'left top'      => esc_html__( 'left top', 'hair-beauty' ),
		'left bottom'   => esc_html__( 'left bottom', 'hair-beauty' ),
		'center center' => esc_html__( 'center center', 'hair-beauty' ),
		'center top'    => esc_html__( 'center top', 'hair-beauty' ),
		'center bottom' => esc_html__( 'center bottom', 'hair-beauty' ),
		'right center'  => esc_html__( 'right center', 'hair-beauty' ),
		'right top'     => esc_html__( 'right top', 'hair-beauty' ),
		'right bottom'  => esc_html__( 'right bottom', 'hair-beauty' ),
	),
);

?>

<div class="wpv-config-row background clearfix <?php echo esc_attr( $class ) ?>">

	<div class="rtitle">
		<h4><?php echo esc_html( $name ) ?></h4>

		<?php wpv_description( $id, $desc ) ?>
	</div>

	<div class="rcontent">
		<div class="bg-inner-row">
			<?php if ( $show['color'] ) : ?>
				<div class="bg-block color">
					<div class="single-desc"><?php esc_html_e( 'Color:', 'hair-beauty' ) ?></div>
					<input name="<?php echo esc_attr( $id . $sep . 'color' ) ?>" id="<?php echo esc_attr( $id ) ?>-color" type="color" data-hex="true" value="<?php echo esc_attr( $current['color'] ) ?>" class="" />
				</div>
			<?php endif ?>

			<?php if ( $show['opacity'] ) : ?>
				<div class="bg-block opacity range-input-wrap clearfix">
					<div class="single-desc"><?php esc_html_e( 'Opacity:', 'hair-beauty' ) ?></div>
					<span>
						<input name="<?php echo esc_attr( $id . $sep . 'opacity' ) ?>" id="<?php echo esc_attr( $id ) ?>-opacity" type="range" value="<?php echo esc_attr( $current['opacity'] )?>" min="0" max="1" step="0.05" class="wpv-range-input" />
					</span>
				</div>
			<?php endif ?>
		</div>

		<div class="bg-inner-row">
			<?php if ( $show['image'] ) : ?>
				<div class="bg-block bg-image">
					<div class="single-desc"><?php esc_html_e( 'Image / pattern:', 'hair-beauty' ) ?></div>
					<?php $_id = $id;	$id .= $sep.'image'; // temporary change the id so that we can reuse the upload field ?>
					<div class="image <?php wpv_static( $value ) ?>">
						<?php include WPV_ADMIN_CGEN . 'upload-basic.php'; ?>
					</div>
					<?php $id = $_id; unset( $_id ); ?>
				</div>
			<?php endif ?>

			<?php if ( $show['size'] ) : ?>
				<div class="bg-block bg-size">
					<div class="single-desc"><?php esc_html_e( 'Cover:', 'hair-beauty' ) ?></div>
					<label class="toggle-radio">
						<input type="radio" name="<?php echo esc_attr( $id.$sep ) ?>size" value="cover" <?php checked( $current['size'], 'cover' ) ?>/>
						<span><?php esc_html_e( 'On', 'hair-beauty' ) ?></span>
					</label>
					<label class="toggle-radio">
						<input type="radio" name="<?php echo esc_attr( $id.$sep ) ?>size" value="auto" <?php checked( $current['size'], 'auto' ) ?>/>
						<span><?php esc_html_e( 'Off', 'hair-beauty' ) ?></span>
					</label>
				</div>
			<?php endif ?>

			<?php foreach ( $selects as $s => $options ) : ?>
				<?php if ( $show[ $s ] ) : ?>
					<div class="bg-block bg-<?php echo esc_attr( $s )?>">
						<div class="single-desc"><?php echo wp_kses_post( $fields[ $s ] ) ?></div>
						<select name="<?php echo esc_attr( $id.$sep.$s ) ?>" class="bg-<?php echo esc_attr( $s ) ?>">
							<?php foreach ( $options as $val => $opt ) : ?>
								<option value="<?php echo esc_attr( $val ) ?>" <?php selected( $val, $current[ $s ] ) ?>><?php echo esc_html( $opt ) ?></option>
							<?php endforeach ?>
						</select>
					</div>
				<?php endif ?>
			<?php endforeach ?>
		</div>
	</div>
</div>
