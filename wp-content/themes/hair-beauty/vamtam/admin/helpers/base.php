<?php

/**
 *
 * @desc registers a theme activation hook
 * @param string $code : Code of the theme. This can be the base folder of your theme. Eg if your theme is in folder 'mytheme' then code will be 'mytheme'
 * @param callback $function : Function to call when theme gets activated.
 */
function wpv_register_theme_activation_hook( $code, $function ) {
	$optionKey = 'theme_is_activated_' . $code;
	if ( ! get_option( $optionKey ) ) {
		call_user_func( $function );
		update_option( $optionKey , 1 );
	}
}

/**
 * @desc registers deactivation hook
 * @param string $code : Code of the theme. This must match the value you provided in wpv_register_theme_activation_hook function as $code
 * @param callback $function : Function to call when theme gets deactivated.
 */
function wpv_register_theme_deactivation_hook( $code, $function ) {
	// store function in code specific global
	$GLOBALS[ 'wpv_register_theme_deactivation_hook_function' . $code ] = $function;

	// create a runtime function which will delete the option set while activation of this theme and will call deactivation function provided in $function
	$fn = create_function( '$theme', ' call_user_func( $GLOBALS["wpv_register_theme_deactivation_hook_function' . $code . '"] ); delete_option( "theme_is_activated_' . $code. '" );' );

	// add above created function to switch_theme action hook. This hook gets called when admin changes the theme.
	// Due to wordpress core implementation this hook can only be received by currently active theme ( which is going to be deactivated as admin has chosen another one.
	// Your theme can perceive this hook as a deactivation hook.
	add_action( 'switch_theme', $fn );
}

// wpv activation hook
function wpv_theme_activated() {
	if ( wpv_validate_install() ) {
		wpv_register_theme_activation_hook( 'wpv_'.THEME_NAME, 'wpv_theme_activated' );

		// disable jetpack likes & comments modules on activation
		$jetpack_opt_name = 'jetpack_active_modules';
		update_option( $jetpack_opt_name, array_diff( get_option( $jetpack_opt_name, array() ), array( 'likes', 'comments' ) ) );

		wp_redirect( admin_url( 'admin.php?page=tgmpa-install-plugins' ) );
	}
}

wpv_register_theme_activation_hook( 'wpv_'.THEME_NAME, 'wpv_theme_activated' );

// wpv deactivation hook
function wpv_theme_deactivated() {
}
wpv_register_theme_deactivation_hook( 'wpv_'.THEME_NAME, 'wpv_theme_deactivated' );

add_action( 'admin_init', 'wpv_validate_install' );
function wpv_validate_install() {
	global $wpv_errors, $wpv_validated;
	if ( $wpv_validated )
		return;

	$wpv_validated = true;
	$wpv_errors    = array();

	if ( strpos( str_replace( WP_CONTENT_DIR.'/themes/', '', get_template_directory() ), '/' ) !== false ) {
		$wpv_errors[] = esc_html__( 'The theme must be installed in a directory which is a direct child of wp-content/themes/', 'hair-beauty' );
	}

	if ( ! is_writable( WPV_CACHE_DIR ) ) {
		$wpv_errors[] = sprintf( esc_html__( 'You must set write permissions (755 or 777) for the cache directory (%s)', 'hair-beauty' ), WPV_CACHE_DIR );
	}

	if ( ! extension_loaded( 'gd' ) || ! function_exists( 'gd_info' ) ) {
		$wpv_errors[] = esc_html__( "It seems that your server doesn't have the GD graphic library installed. Please contact your hosting provider, they should be able to assist you with this issue", 'hair-beauty' );
	}

	if ( count( $wpv_errors ) ) {
		if ( ! function_exists( 'wpv_invalid_install' ) ) {
			function wpv_invalid_install() {
				global $wpv_errors;
				?>
					<div class="updated fade error" style="background: #FEF2F2; border: 1px solid #DFB8BB; color: #666;"><p>
						<?php esc_html_e( 'There were some some errors with your Vamtam theme setup:', 'hair-beauty' )?>
						<ul>
							<?php foreach ( $wpv_errors as $error ) : ?>
								<li><?php echo wp_kses_post( $error ) ?></li>
							<?php endforeach ?>
						</ul>
					</p></div>
				<?php
			}
			add_action( 'admin_notices', 'wpv_invalid_install' );
		}
		switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
		return false;
	}

	return true;
}

function wpv_static( $option ) {
	if ( isset( $option['static'] ) && $option['static'] ) {
		echo 'static'; }
}

function wpv_description( $id, $desc ) {
	if ( ! empty( $desc ) ) : ?>
		<div class="row-desc">
			<a href="#" class="va-icon va-icon-info desc-handle"></a>
			<div>
				<section class="content"><?php echo wp_kses_post( $desc ) ?></section>
				<footer><a href="<?php echo esc_attr( 'http://support.vamtam.com' ) ?>" title="<?php esc_attr_e( 'Read more on our Help Desk', 'hair-beauty' ) ?>" target="_blank"><?php esc_html_e( 'Read more on our Help Desk', 'hair-beauty' ) ?></a></footer>
			</div>
		</div>
	<?php endif;
}

function wpv_compile_less_ajax() {
	if ( ! wp_verify_nonce( $_POST['_nonce'], 'wpv-compile-less' ) ) {
		exit;
	}

	$error = WpvLessBridge::basic_compile( $_POST['input'], $_POST['output'] );

	if ( $error ) {
		echo json_encode( array( 'status' => 'error', 'message' => $error, 'memory' => memory_get_peak_usage() / 1024 / 1024 ) );
	} else {
		echo json_encode( array( 'status' => 'ok', 'memory' => memory_get_peak_usage() / 1024 / 1024 ) );
	}

	exit;
}
add_action( 'wp_ajax_wpv-compile-less', 'wpv_compile_less_ajax' );
