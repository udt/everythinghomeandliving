<?php
return array(
	'name' => esc_html__( 'Help', 'hair-beauty' ),
	'auto' => true,
	'config' => array(

		array(
			'name' => esc_html__( 'Help', 'hair-beauty' ),
			'type' => 'title',
			'desc' => '',
		),

		array(
			'name' => esc_html__( 'Help', 'hair-beauty' ),
			'type' => 'start',
			'nosave' => true,
		),
//----
		array(
			'type' => 'docs',
		),

			array(
				'type' => 'end',
			),
	),
);
