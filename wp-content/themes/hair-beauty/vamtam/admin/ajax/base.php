<?php

function wpv_shortcode_generator() {
	$config = array(
		'title' => esc_html__( 'Shortcodes', 'hair-beauty' ),
		'id' => 'shortcode',
	);

	$shortcodes = apply_filters( 'wpv_shortcode_'.$_GET['slug'], include( WPV_SHORTCODES_GENERATOR . $_GET['slug'] .'.php' ) );
	$generator  = new WpvShortcodesGenerator( $config, $shortcodes );

	$generator->render();
}
add_action( 'wp_ajax_wpv-shortcode-generator', 'wpv_shortcode_generator' );
