(function( $, undefined ) {
	"use strict";

	$( function() {
		if ( WPV_HIDDEN_WIDGETS !== undefined && WPV_HIDDEN_WIDGETS.length > 0 ) {
			var width = -1;
			var win = $( window );

			win.smartresize( function() {
				if ( width !== win.width() ) {
					width = win.width();

					for ( var i = 0; i < WPV_HIDDEN_WIDGETS.length; i++ ) {
						$( '#' + WPV_HIDDEN_WIDGETS[i] ).toggleClass( 'hidden', $.WPV.MEDIA.layout["layout-below-max"] );
					}
				}
			} );
		}
	} );
} )( jQuery );