( function( $, undefined ) {
	'use strict';

	$(function() {
		var body = $('body');
		var admin_bar_fix = body.hasClass('admin-bar') ? 32 : 0;
		var win = $(window);

		win.smartresize(function() {
			var header_fix = body.hasClass( 'sticky-header' ) && body.hasClass( 'no-sticky-header-animation' ) ? $( 'header.main-header' ).height() : 0;
			// var header_fix = body.hasClass( 'sticky-header' ) ? $( 'header.main-header' ).height() : 0;

			body.trigger('wpv-content-resized');

			var wheight = win.height() - admin_bar_fix - header_fix;

			$('.wpv-grid[data-padding-top]').each(function() {
				var col = $(this);

				col.css('padding-top', 0);
				col.css('padding-top', wheight - col.outerHeight() + parseInt(col.data('padding-top'), 10));
			});

			$('.wpv-grid[data-padding-bottom]:not([data-padding-top])').each(function() {
				var col = $(this);

				col.css('padding-bottom', 0);
				col.css('padding-bottom', wheight - col.outerHeight() + parseInt(col.data('padding-bottom'), 10));
			});

			$('.wpv-grid[data-padding-top][data-padding-bottom]').each(function() {
				var col = $(this);

				col.css('padding-top', 0);
				col.css('padding-bottom', 0);

				var new_padding = (wheight - col.outerHeight() + parseInt(col.data('padding-top'), 10))/2;

				col.css({
					'padding-top': new_padding,
					'padding-bottom': new_padding
				});
			});

			win.trigger( 'vamtam-force-parallax-repaint' );
		});
	});
} )( jQuery );