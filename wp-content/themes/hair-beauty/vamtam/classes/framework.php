<?php

/**
 * Vamtam Theme Framework base class
 *
 * @author Nikolay Yordanov <me@nyordanov.com>
 * @package wpv
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * This is the first loaded framework file
 *
 * WpvFramework does the following ( in this order ):
 *  - sets constants for the frequently used paths
 *  - loads translations
 *  - loads the plugins bundled with the theme
 *  - loads some functions and helpers used in various places
 *  - sets the custom post types
 *  - loads the shortcode library for the framework
 *  - if this is wp-admin, load admin files
 *
 * This class also loads the custom widgets and sets what the theme supports ( + custom menus )
 */

class WpvFramework {

	/**
	 * Cache the result of some operations in memory
	 *
	 * @var array
	 */
	private static $cache = array();

	/**
	 * Post types with double sidebars
	 */
	public static $complex_layout = array( 'page', 'post', 'jetpack-portfolio', 'product' );

	/**
	 * Initialize the Vamtam framework
	 * @param array $options framework options
	 */
	public function __construct( $options ) {
		// Autoload classes on demand
		if ( function_exists( '__autoload' ) )
			spl_autoload_register( '__autoload' );
		spl_autoload_register( array( $this, 'autoload' ) );

		$this->set_constants( $options );
		$this->load_languages();
		$this->load_functions();
		$this->load_shortcodes();
		$this->load_admin();

		add_action( 'after_setup_theme', array( &$this, 'theme_supports' ) );
		add_action( 'widgets_init', array( &$this, 'load_widgets' ) );
		add_filter( 'wpv_purchase_code', create_function( '', 'return get_option( "vamtam-envato-license-key" );' ) );

		WpvLoadMore::get_instance();
		WpvHideWidgets::get_instance();
	}

	/**
	 * Autoload classes when needed
	 *
	 * @param  string $class class name
	 */
	public function autoload( $class ) {
		$class = strtolower( preg_replace( '/([a-z])([A-Z])/', '$1-$2', str_replace( '_', '', $class ) ) );

		if ( strpos( $class, 'wpv-' ) === 0 ) {
			$path = trailingslashit( get_template_directory() ) . 'vamtam/classes/';
			$file = str_replace( 'wpv-', '', $class ) . '.php';

			if ( is_readable( $path . $file ) ) {
				include_once( $path . $file );
				return;
			}

			if ( is_admin() ) {
				$admin_path = WPV_ADMIN . 'classes/';

				if ( is_readable( $admin_path . $file ) ) {
					include_once( $admin_path . $file );
					return;
				}
			}
		}

	}

	/**
	 * Get the theme version
	 *
	 * @return string theme version as defined in style.css
	 */
	public static function get_version() {
		if ( isset( self::$cache['version'] ) )
			return self::$cache['version'];

		$the_theme = wp_get_theme();
		if ( $the_theme->parent() ) {
			$the_theme = $the_theme->parent();
		}

		self::$cache['version'] = $the_theme->get( 'Version' );

		return self::$cache['version'];
	}

	/**
	 * Defines constants used by the theme
	 *
	 * @param array $options framework options
	 */
	private function set_constants( $options ) {
		define( 'THEME_NAME', $options['name'] );
		define( 'THEME_SLUG', $options['slug'] );

		// theme dir and uri
		define( 'THEME_DIR', get_template_directory() . '/' );
		define( 'THEME_URI', get_template_directory_uri() .'/' );

		// framework dir and uri
		define( 'WPV_DIR', THEME_DIR . 'vamtam/' );
		define( 'WPV_URI', THEME_URI . 'vamtam/' );

		// theme-specific assets dir and uri
		define( 'WPV_THEME_DIR', THEME_DIR . 'wpv_theme/' );
		define( 'WPV_THEME_URI', THEME_URI . 'wpv_theme/' );

		// common assets dir and uri
		define( 'WPV_ASSETS_DIR', WPV_DIR . 'assets/' );
		define( 'WPV_ASSETS_URI', WPV_URI . 'assets/' );

		// common file paths
		define( 'WPV_HELPERS', WPV_DIR . 'helpers/' );
		define( 'WPV_IMAGES', WPV_ASSETS_URI . 'images/' );
		define( 'WPV_PLUGINS', WPV_DIR . 'plugins/' );
		define( 'WPV_PLUGINS_URI', WPV_URI . 'plugins/' );
		define( 'WPV_SHORTCODES_GENERATOR', WPV_DIR . 'shortcodes-generator/' );
		define( 'WPV_SWF', WPV_ASSETS_URI . 'swf/' );
		define( 'WPV_WIDGETS', WPV_DIR . 'widgets/' );
		define( 'WPV_WIDGETS_ASSETS', WPV_WIDGETS . 'assets/' );
		define( 'WPV_WIDGETS_TPL', WPV_WIDGETS . 'tpl/' );
		define( 'WPV_FONTS_URI', WPV_ASSETS_URI . 'fonts/' );
		define( 'WPV_INCLUDES', WPV_ASSETS_URI . 'includes/' );
		define( 'WPV_JS', WPV_ASSETS_URI . 'js/' );
		define( 'WPV_OPTIONS', WPV_DIR . 'options/' );

		// theme-specific file paths
		define( 'WPV_THEME_ASSETS_DIR', WPV_THEME_DIR . 'assets/' );
		define( 'WPV_THEME_ASSETS_URI', WPV_THEME_URI . 'assets/' );
		define( 'WPV_THEME_HELPERS', WPV_THEME_DIR . 'helpers/' );
		define( 'WPV_THEME_METABOXES', WPV_THEME_DIR . 'metaboxes/' );
		define( 'WPV_THEME_IMAGES', WPV_THEME_ASSETS_URI . 'images/' );
		define( 'WPV_THEME_IMAGES_DIR', WPV_THEME_ASSETS_DIR . 'images/' );
		define( 'WPV_THEME_CSS', WPV_THEME_ASSETS_URI . 'css/' );
		define( 'WPV_THEME_CSS_DIR', WPV_THEME_ASSETS_DIR . 'css/' );

		// sample content
		define( 'WPV_SAMPLES_DIR', THEME_DIR . 'samples/' );
		define( 'WPV_SAMPLES_URI', THEME_URI . 'samples/' );
		define( 'WPV_SAVED_OPTIONS', WPV_SAMPLES_DIR . 'saved_skins/' );
		define( 'WPV_SAVED_OPTIONS_URI', WPV_SAMPLES_URI . 'saved_skins/' );
		define( 'WPV_THEME_SAMPLE_CONTENT', WPV_SAMPLES_DIR . 'content.xml' );
		define( 'WPV_THEME_SAMPLE_WIDGETS', WPV_SAMPLES_DIR . 'sidebars' );

		// cache
		define( 'WPV_CACHE_DIR', THEME_DIR . 'cache/' );
		define( 'WPV_CACHE_URI', THEME_URI . 'cache/' );

		// admin
		define( 'WPV_ADMIN', WPV_DIR . 'admin/' );
		define( 'WPV_ADMIN_URI', WPV_URI . 'admin/' );
		define( 'WPV_ADMIN_AJAX', WPV_ADMIN_URI . 'ajax/' );
		define( 'WPV_ADMIN_AJAX_DIR', WPV_ADMIN . 'ajax/' );
		define( 'WPV_ADMIN_ASSETS_URI', WPV_ADMIN_URI . 'assets/' );
		define( 'WPV_ADMIN_HELPERS', WPV_ADMIN . 'helpers/' );
		define( 'WPV_ADMIN_CGEN', WPV_ADMIN_HELPERS . 'config_generator/' );
		define( 'WPV_ADMIN_METABOXES', WPV_ADMIN . 'metaboxes/' );
		define( 'WPV_ADMIN_OPTIONS', WPV_ADMIN . 'options/' );
		define( 'WPV_ADMIN_TEMPLATES', WPV_ADMIN . 'templates/' );
	}

	/**
	 * Register theme support for various features
	 */
	public function theme_supports() {
		global $wpv_post_formats, $content_width;

		define( 'WPV_RESPONSIVE', apply_filters( 'vamtam-theme-responsive-mode', true ) );

		/**
		 * the max content width the css is built for should equal the actual content width,
		 * for example, the width of the text of a page without sidebars
		 */
		if ( ! isset( $content_width ) ) $content_width = rd_wpv_get_option( 'site-max-width' );

		$wpv_post_formats = apply_filters( 'wpv_post_formats', array( 'aside', 'link', 'image', 'video', 'audio', 'quote', 'gallery' ) );

		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_theme_support( 'post-formats', $wpv_post_formats );
		add_theme_support( 'title-tag' );

		add_theme_support( 'wpv-ajax-siblings' );
		add_theme_support( 'wpv-page-title-style' );
		add_theme_support( 'wpv-centered-text-divider' );

		add_theme_support( 'customize-selective-refresh-widgets' );

		if ( class_exists( 'Jetpack_Portfolio' ) ) {
			add_post_type_support( Jetpack_Portfolio::CUSTOM_POST_TYPE, 'excerpt' );
		}

		if ( function_exists( 'register_nav_menus' ) ) {
			register_nav_menus(
				array(
					'menu-header' => esc_html__( 'Menu Header', 'hair-beauty' ),
					'menu-top'    => esc_html__( 'Menu Top', 'hair-beauty' ),
				)
			);
		}

		add_image_size( 'posts-widget-thumb', 60, 60, true );
		add_image_size( 'posts-widget-thumb-small', 43, 43, true );

		$size_names = array( 'theme-single', 'theme-loop' );
		$size_info  = array();

		$wth = get_option( 'vamtam_featured_images_ratio', array(
			'theme-loop'   => 1.3,
			'theme-single' => 1.3,
		) );

		foreach ( $size_names as $name ) {
			$size_info[ $name ] = (object) array(
				'wth' => abs( floatval( $wth[ $name ] ) ),
				'crop' => true,
			);
		}

		$width = $content_width - 2 * 60;

		$single_sizes     = array( 'theme-single' );
		$columnated_sizes = array( 'theme-loop' );

		foreach ( $single_sizes as $name ) {
			$height = $size_info[ $name ]->wth ? $width / $size_info[ $name ]->wth : false;
			add_image_size( $name, $width, $height, $size_info[ $name ]->crop );
		}

		for ( $num_columns = 1; $num_columns <= 4; $num_columns++ ) {
			$small_width = ( $width + 30 ) / $num_columns - 30;
			$small_width = ( $width + 30 ) / $num_columns - 30;

			add_image_size( 'theme-normal-' . $num_columns, $small_width, 0 ); // special case where we always use the original proportions
			add_image_size( 'theme-normal-featured-' . $num_columns, $small_width * 2, 0 ); // same, but double width

			foreach ( $columnated_sizes as $name ) {
				$col_width = ( $width + 30 ) / $num_columns - 30;
				$height    = $size_info[ $name ]->wth ? $col_width / $size_info[ $name ]->wth : false;

				add_image_size( $name . '-' . $num_columns, $col_width, $height, $size_info[ $name ]->crop );
				add_image_size( $name . '-featured-' . $num_columns, $col_width * 2, $height * 2, $size_info[ $name ]->crop );
			}
		}
	}

	/**
	 * Load interface translations
	 */
	private function load_languages() {
		load_theme_textdomain( 'hair-beauty', THEME_DIR . 'languages' );
	}

	/**
	 * Loads the main php files used by the framework
	 */
	private function load_functions() {
		global $wpv_defaults, $wpv_fonts;
		$wpv_defaults = include WPV_SAMPLES_DIR . 'default-options.php';
		$wpv_fonts    = include WPV_HELPERS . 'fonts.php';

		require_once WPV_HELPERS . 'init.php';

		$custom_fonts = get_option( 'vamtam_custom_font_families', '' );
		if ( ! empty( $custom_fonts ) ) {
			$custom_fonts = explode( "\n", $custom_fonts );

			$wpv_fonts['-- Custom fonts --'] = array( 'family' => '' );

			foreach ( $custom_fonts as $font ) {
				$font = preg_replace( '/["\']+/', '', trim( $font ) );

				$wpv_fonts[ $font ] = array(
					'family' => '"' . $font . '"',
					'weights' => array( '300', '300 italic', 'normal', 'italic', '600', '600 italic', 'bold', 'bold italic', '800', '800 italic' ),
				);
			}
		}

		require_once WPV_HELPERS . 'woocommerce-integration.php';
		require_once WPV_HELPERS . 'megamenu-integration.php';

		require_once WPV_HELPERS . 'icons.php';

		require_once WPV_HELPERS . 'file.php';

		WpvFormatFilter::actions();

		require_once WPV_HELPERS . 'base.php';
		require_once WPV_HELPERS . 'template.php';
		require_once WPV_HELPERS . 'css.php';

		WpvOverrides::filters();
		WpvEnqueues::actions();
	}

	/**
	 * Load shortcodes
	 */
	private function load_shortcodes() {
		add_action( 'template_redirect', array( $this, 'shortcode_preview' ) );
	}

	public function shortcode_preview() {
		if ( isset( $_GET['vamtam_shortcode_preview'] ) ) {
			require_once WPV_ADMIN_AJAX_DIR . 'shortcode-preview.php';

			exit;
		}
	}

	/**
	 * Load widgets
	 */
	public function load_widgets() {
		$wpv_sidebars = WpvSidebars::get_instance();

		$wpv_sidebars->register_sidebars();

		$widgets = apply_filters( 'wpv-enabled-widgets', array(
			'authors',
			'icon-link',
			'posts',
			'subpages',
		) );

		foreach ( $widgets as $name ) {
			require_once WPV_WIDGETS . "/$name.php";
		}
	}

	/**
	 * Loads the theme administration code
	 */
	private function load_admin() {
		if ( ! is_admin() ) return;

		require_once WPV_DIR . 'classes/plugin-activation.php';
		require_once WPV_SAMPLES_DIR . 'dependencies.php';

		WpvAdmin::actions();
	}
}
