<?php
/**
 * Sidebar helpers
 *
 * @package wpv
 */
/**
 * class WpvSidebars
 *
 * register right/left, header and footer sidebars
 * also provides a function which outputs the correct right/left sidebar
 */
class WpvSidebars {

	/**
	 * List of widget areas
	 * @var array
	 */
	private $sidebars = array();

	/**
	 * List of sidebar placements
	 * @var array
	 */
	private $places = array();

	/**
	 * Singleton instance
	 * @var WpvSidebars
	 */
	private static $instance;

	/**
	 * Set the available widgets area
	 */
	public function __construct() {
		$this->sidebars = array(
			'page' => esc_html__( 'Main Widget Area', 'hair-beauty' ),
		);

		if ( wpv_has_woocommerce() )
			$this->sidebars['wpv-woocommerce'] = esc_html__( 'WooCommerce Widget Area', 'hair-beauty' );

		$this->places = array( 'left', 'right' );
	}

	/**
	 * Get singleton instance
	 * @return WpvSidebars singleton instance
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * Register sidebars
	 */
	public function register_sidebars() {
		unregister_sidebar( 'sidebar-event' );

		foreach ( $this->sidebars as $id => $name ) {
			foreach ( $this->places as $place ) {
				register_sidebar( array(
					'id'            => $id.'-'.$place,
					'name'          => $name . " ( $place )",
					'description'   => $name . " ( $place )",
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => apply_filters( 'wpv_before_widget_title', '<h4 class="widget-title">', 'body' ),
					'after_title'   => apply_filters( 'wpv_after_widget_title', '</h4>', 'body' ),
				) );
			}
		}

		for ( $i = 1; $i <= 8; $i++ ) {
			register_sidebar(
				array(
					'id'            => "footer-sidebars-$i",
					'name'          => "Footer widget area $i",
					'description'   => "Footer widget area $i",
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => apply_filters( 'wpv_before_widget_title', '<h4 class="widget-title">', 'footer' ),
					'after_title'   => apply_filters( 'wpv_after_widget_title', '</h4>', 'footer' ),
				)
			);
		}

		for ( $i = 1; $i <= 8; $i++ ) {
			register_sidebar(
				array(
					'id'            => "header-sidebars-$i",
					'name'          => "Body Top Widget Area $i",
					'description'   => "Body top widget area $i",
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => apply_filters( 'wpv_before_widget_title', '<h4 class="widget-title">', 'header' ),
					'after_title'   => apply_filters( 'wpv_after_widget_title', '</h4>', 'header' ),
				)
			);
		}
	}

	private function get_sidebar_name( $place = 'left' ) {
		global $post;

		if ( wpv_has_woocommerce() && is_woocommerce() ) {
			$sidebar = 'wpv-woocommerce';
		}

		if ( isset( $sidebar ) ) {
			return $sidebar . '-' . $place;
		}

		return 'page-' . $place;
	}

	/**
	 * Output the correct sidebar
	 *
	 * @uses dynamic_sidebar()
	 *
	 * @param  string $place one of $this->placements
	 * @return bool          result of dynamic_sidebar()
	 */
	public function get_sidebar( $place = 'left' ) {
		$name = $this->get_sidebar_name( $place );

		dynamic_sidebar( $name );
	}

	/**
	 * Check if we should show a sidebar
	 *
	 * @uses is_active_sidebar()
	 *
	 * @param  string $place one of $this->placements
	 * @return bool          result of dynamic_sidebar()
	 */
	public function has_sidebar( $place = 'left' ) {
		$name = $this->get_sidebar_name( $place );

		return is_active_sidebar( $name );
	}
};
