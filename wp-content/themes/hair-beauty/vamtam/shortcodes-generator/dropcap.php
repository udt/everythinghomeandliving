<?php
return array(
	'name'    => esc_html__( 'Drop Cap', 'hair-beauty' ),
	'value'   => 'dropcap',
	'options' => array(
		array(
			'name'    => esc_html__( 'Type', 'hair-beauty' ),
			'id'      => 'type',
			'default' => '1',
			'type'    => 'select',
			'options' => array(
				'1' => esc_html__( 'Type 1', 'hair-beauty' ),
				'2' => esc_html__( 'Type 2', 'hair-beauty' ),
			),
		) ,
		array(
			'name'    => esc_html__( 'Letter', 'hair-beauty' ),
			'id'      => 'letter',
			'default' => '',
			'type'    => 'text',
		) ,
		array(
			'name'    => esc_html__( 'Text', 'hair-beauty' ),
			'id'      => 'text',
			'default' => '',
			'type'    => 'text',
		) ,
	),
);
