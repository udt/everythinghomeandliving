<?php

return array(
	'name'    => esc_html__( 'Styled List', 'hair-beauty' ),
	'value'   => 'list',
	'options' => array(
		array(
			'name'    => esc_html__( 'Style', 'hair-beauty' ),
			'id'      => 'style',
			'default' => '',
			'type'    => 'icons',
		) ,
		array(
			'name'    => esc_html__( 'Color', 'hair-beauty' ),
			'id'      => 'color',
			'default' => '',
			'options' => array(
				'accent1' => esc_html__( 'Accent 1', 'hair-beauty' ),
				'accent2' => esc_html__( 'Accent 2', 'hair-beauty' ),
				'accent3' => esc_html__( 'Accent 3', 'hair-beauty' ),
				'accent4' => esc_html__( 'Accent 4', 'hair-beauty' ),
				'accent5' => esc_html__( 'Accent 5', 'hair-beauty' ),
				'accent6' => esc_html__( 'Accent 6', 'hair-beauty' ),
				'accent7' => esc_html__( 'Accent 7', 'hair-beauty' ),
				'accent8' => esc_html__( 'Accent 8', 'hair-beauty' ),
			),
			'type' => 'select',
		) ,
		array(
			'name'    => esc_html__( 'Content', 'hair-beauty' ),
			'desc'    => esc_html__( 'Please insert a valid HTML unordered list', 'hair-beauty' ),
			'id'      => 'content',
			'type'    => 'textarea',
			'default' => wp_kses_post( __( '<ul>
                <li>list item</li>
                <li>another item</li>
            </ul>', 'hair-beauty' ) ),
		) ,
	),
);
