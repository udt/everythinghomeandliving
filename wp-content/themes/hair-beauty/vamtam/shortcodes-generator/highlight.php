<?php

return array(
	'name'    => esc_html__( 'Highlight', 'hair-beauty' ),
	'value'   => 'highlight',
	'options' => array(
		array(
			'name'    => esc_html__( 'Type', 'hair-beauty' ),
			'id'      => 'type',
			'default' => '',
			'type'    => 'select',
			'options' => array(
				'light' => esc_html__( 'light', 'hair-beauty' ),
				'dark'  => esc_html__( 'dark', 'hair-beauty' ),
			),
		) ,
		array(
			'name'    => esc_html__( 'Content', 'hair-beauty' ),
			'id'      => 'content',
			'default' => '',
			'type'    => 'textarea',
		) ,
	),
);
