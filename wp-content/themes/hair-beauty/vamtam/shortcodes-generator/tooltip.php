<?php
return 	array(
	'name'    => 'Tooltip',
	'value'   => 'tooltip',
	'options' => array(
		array(
			'name'    => esc_html__( 'Tooltip content', 'hair-beauty' ),
			'id'      => 'tooltip_content',
			'default' => '',
			'type'    => 'textarea',
		),
		array(
			'name'    => esc_html__( 'Tooltip trigger', 'hair-beauty' ),
			'id'      => 'content',
			'default' => '',
			'type'    => 'textarea',
		),
	),
);
