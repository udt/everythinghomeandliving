<?php

return array(
	'name'    => esc_html__( 'Icon', 'hair-beauty' ),
	'value'   => 'icon',
	'options' => array(
		array(
			'name'    => esc_html__( 'Name', 'hair-beauty' ),
			'id'      => 'name',
			'default' => '',
			'type'    => 'icons',
		) ,
		array(
			'name'    => esc_html__( 'Color (optional)', 'hair-beauty' ),
			'id'      => 'color',
			'default' => '',
			'prompt'  => '',
			'type'    => 'select',
			'options' => array(
				'accent1' => esc_html__( 'Accent 1', 'hair-beauty' ),
				'accent2' => esc_html__( 'Accent 2', 'hair-beauty' ),
				'accent3' => esc_html__( 'Accent 3', 'hair-beauty' ),
				'accent4' => esc_html__( 'Accent 4', 'hair-beauty' ),
				'accent5' => esc_html__( 'Accent 5', 'hair-beauty' ),
				'accent6' => esc_html__( 'Accent 6', 'hair-beauty' ),
				'accent7' => esc_html__( 'Accent 7', 'hair-beauty' ),
				'accent8' => esc_html__( 'Accent 8', 'hair-beauty' ),
			),
		) ,
		array(
			'name'    => esc_html__( 'Size', 'hair-beauty' ),
			'id'      => 'size',
			'type'    => 'range',
			'default' => 16,
			'min'     => 8,
			'max'     => 100,
		),
		array(
			'name'    => esc_html__( 'Style', 'hair-beauty' ),
			'id'      => 'style',
			'default' => '',
			'prompt'  => esc_html__( 'Default', 'hair-beauty' ),
			'type' => 'select',
			'options' => array(
				'border' => esc_html__( 'Border', 'hair-beauty' ),
			),
		) ,
	),
);
