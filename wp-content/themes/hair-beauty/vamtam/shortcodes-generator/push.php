<?php
return array(
	'name'    => esc_html__( 'Vertical Blank Space', 'hair-beauty' ),
	'value'   => 'push',
	'options' => array(
		array(
			'name'    => esc_html__( 'Height', 'hair-beauty' ),
			'id'      => 'h',
			'default' => 30,
			'min'     => -200,
			'max'     => 200,
			'type'    => 'range',
		) ,
		array(
			'name'    => esc_html__( 'Hide on Low Resolutions', 'hair-beauty' ),
			'id'      => 'hide_low_res',
			'default' => false,
			'type'    => 'toggle',
		) ,
		array(
			'name'    => esc_html__( 'Class', 'hair-beauty' ),
			'id'      => 'class',
			'default' => '',
			'type'    => 'text',
		) ,
	),
);
