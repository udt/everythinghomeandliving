<?php
return array(
	'name' => esc_html__( 'Divider', 'hair-beauty' ),
	'value' => 'inline_divider',
	'options' => array(
		array(
			'name'    => esc_html__( 'Type', 'hair-beauty' ),
			'desc'    => wp_kses_post( __( '"Clear floats" is just a div element with <em>clear:both</em> styles. Although it is safe to say that unless you already know how to use it, you will not need this, you can <a href="https://developer.mozilla.org/en-US/docs/CSS/clear">click here for a more detailed description</a>.', 'hair-beauty' ) ),
			'id'      => 'type',
			'default' => 1,
			'options' => array(
				1       => esc_html__( 'Divider line (1)', 'hair-beauty' ),
				2       => esc_html__( 'Divider line (2)', 'hair-beauty' ),
				3       => esc_html__( 'Divider line (3)', 'hair-beauty' ),
				'clear' => esc_html__( 'Clear floats', 'hair-beauty' ),
			),
			'type'  => 'select',
			'class' => 'add-to-container',
		) ,
	),
);
