<?php
/**
 * TinyMCE-based shortcode generator
 *
 * @package wpv
 * @subpackage hair
 */

return array(
	'button',
	'dropcap',
	'highlight',
	'icon',
	'inline_divider',
	'list',
	'push',
	'tooltip',
);
