<?php
/**
 * Vamtam Post Options
 *
 * @package wpv
 * @subpackage hair
 */

return array(

array(
	'name' => esc_html__( 'General', 'hair-beauty' ),
	'type' => 'separator',
),

array(
	'name'    => esc_html__( 'Cite', 'hair-beauty' ),
	'id'      => 'testimonial-author',
	'default' => '',
	'type'    => 'text',
) ,

array(
	'name'    => esc_html__( 'Link', 'hair-beauty' ),
	'id'      => 'testimonial-link',
	'default' => '',
	'type'    => 'text',
) ,

array(
	'name'    => esc_html__( 'Rating', 'hair-beauty' ),
	'id'      => 'testimonial-rating',
	'default' => 5,
	'type'    => 'range',
	'min'     => 0,
	'max'     => 5,
) ,

array(
	'name'    => esc_html__( 'Summary', 'hair-beauty' ),
	'id'      => 'testimonial-summary',
	'default' => '',
	'type'    => 'text',
) ,

);
