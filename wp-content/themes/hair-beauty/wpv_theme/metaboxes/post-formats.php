<?php
/**
 * Vamtam Post Format Options
 *
 * @package wpv
 * @subpackage hair
 */

return array(

array(
	'name' => esc_html__( 'Standard', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-0',
),

array(
	'name' => esc_html__( 'How do I use standard post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Just use the editor below.', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

// --

array(
	'name' => esc_html__( 'Aside', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-aside',
),

array(
	'name' => esc_html__( 'How do I use aside post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Just use the editor below. The post title will not be shown publicly.', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

// --

array(
	'name' => esc_html__( 'Link', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-link',
),

array(
	'name' => esc_html__( 'How do I use link post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Use the editor below for the post body, put the link in the option below.', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

array(
	'name' => esc_html__( 'Link', 'hair-beauty' ),
	'id' => 'wpv-post-format-link',
	'type' => 'text',
),

// --

array(
	'name' => esc_html__( 'Image', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-image',
),

array(
	'name' => esc_html__( 'How do I use image post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Use the standard Featured Image option.', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

// --

array(
	'name' => esc_html__( 'Video', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-video',
),

array(
	'name' => esc_html__( 'How do I use video post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Put the url of the video below. You must use an oEmbed provider supported by WordPress or a file supported by the [video] shortcode which comes with WordPress.', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

array(
	'name' => esc_html__( 'Link', 'hair-beauty' ),
	'id' => 'wpv-post-format-video-link',
	'type' => 'text',
),

// --

array(
	'name' => esc_html__( 'Audio', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-audio',
),

array(
	'name' => esc_html__( 'How do I use auido post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Put the url of the audio below. You must use an oEmbed provider supported by WordPress or a file supported by the [audio] shortcode which comes with WordPress.', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

array(
	'name' => esc_html__( 'Link', 'hair-beauty' ),
	'id' => 'wpv-post-format-audio-link',
	'type' => 'text',
),

// --

array(
	'name' => esc_html__( 'Quote', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-quote',
),

array(
	'name' => esc_html__( 'How do I use quote post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Simply fill in author and link fields', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

array(
	'name' => esc_html__( 'Author', 'hair-beauty' ),
	'id' => 'wpv-post-format-quote-author',
	'type' => 'text',
),

array(
	'name' => esc_html__( 'Link', 'hair-beauty' ),
	'id' => 'wpv-post-format-quote-link',
	'type' => 'text',
),

// --

array(
	'name' => esc_html__( 'Gallery', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-gallery',
),

array(
	'name' => esc_html__( 'How do I use gallery post format?', 'hair-beauty' ),
	'desc' => esc_html__( 'Use the "Add Media" in a text/image block element to create a gallery.This button is also found in the top left side of the visual and text editors.', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

// --

array(
	'name' => esc_html__( 'Status', 'hair-beauty' ),
	'type' => 'separator',
	'tab_class' => 'wpv-post-format-status',
),

array(
	'name' => esc_html__( 'How do I use this post format?', 'hair-beauty' ),
	'desc' => esc_html__( '...', 'hair-beauty' ),
	'type' => 'info',
	'visible' => true,
),

);
