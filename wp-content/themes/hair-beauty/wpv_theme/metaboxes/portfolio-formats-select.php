<?php
/**
 * Vamtam Project Format Selector
 *
 * @package wpv
 * @subpackage hair
 */

return array(

array(
	'name' => esc_html__( 'Project Format', 'hair-beauty' ),
	'type' => 'separator',
),

array(
	'name' => esc_html__( 'Project Data Type', 'hair-beauty' ),
	'desc' => wp_kses_post( __('Image - uses the featured image (default)<br />
				  Gallery - use the featured image as a title image but show additional images too<br />
				  Video/Link - uses the "portfolio data url" setting<br />
				  Document - acts like a normal post<br />
				  HTML - overrides the image with arbitrary HTML when displaying a single project.
				', 'hair-beauty') ),
	'id'      => 'portfolio_type',
	'type'    => 'radio',
	'options' => array(
		'image'    => esc_html__( 'Image', 'hair-beauty' ),
		'gallery'  => esc_html__( 'Gallery', 'hair-beauty' ),
		'video'    => esc_html__( 'Video', 'hair-beauty' ),
		'link'     => esc_html__( 'Link', 'hair-beauty' ),
		'document' => esc_html__( 'Document', 'hair-beauty' ),
		'html'     => esc_html__( 'HTML', 'hair-beauty' ),
	),
	'default' => 'image',
),

array(
	'name'    => esc_html__( 'Featured Project', 'hair-beauty' ),
	'id'      => 'featured-project',
	'type'    => 'checkbox',
	'default' => false,
),

);
