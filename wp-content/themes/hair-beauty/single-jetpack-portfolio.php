<?php
/**
 * Single portfolio template
 *
 * @package wpv
 * @subpackage hair
 */

if ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && 'XMLHttpRequest' === $_SERVER['HTTP_X_REQUESTED_WITH'] && have_posts() ) :
	the_post();

	if ( function_exists( 'sharing_add_header' ) ) {
		sharing_add_header();
	}

	extract( wpv_get_portfolio_options() );
?>

	<h1 class="ajax-portfolio-title textcenter"><?php the_title() ?></h1>
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'full ' . $type ); ?>>
		<div class="page-content">
			<?php include locate_template( 'single-jetpack-portfolio-content.php' ); ?>
		</div>
	</article>

<?php

	if ( function_exists( 'sharing_add_footer' ) ) {
		sharing_add_footer();
	}

	print_late_styles();

?>
	<script> try { twttr.widgets.load(); } catch(e) {} </script>
<?php

	exit;
endif;

get_header();
?>
	<div class="row page-wrapper">
		<?php WpvTemplates::$in_page_wrapper = true; ?>

		<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
		?>
				<?php
					extract( wpv_get_portfolio_options() );

					list( $terms_slug, $terms_name ) = wpv_get_portfolio_terms();
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class( WpvTemplates::get_layout() . ' ' . $type ); ?>>
					<div class="page-content">
						<?php
							global $wpv_has_header_sidebars;
							if ( $wpv_has_header_sidebars ) {
								WpvTemplates::header_sidebars();
							}

							$column_width = wpv_get_central_column_width();
							$size = $column_width;
						?>

						<?php include locate_template( 'single-jetpack-portfolio-content.php' ); ?>

						<div class="clearboth">
							<?php comments_template(); ?>
						</div>
					</div>
				</article>
			<?php endwhile ?>
		<?php endif ?>

		<?php get_template_part( 'sidebar' ) ?>
	</div>

	<?php if ( ( rd_wpv_get_optionb( 'show-related-portfolios' ) || is_customize_preview() ) && class_exists( 'WPV_Projects' ) && WPV_Projects::in_category( $terms_slug ) > 1 ) : ?>
		<?php if ( ! class_exists( 'WPV_Columns' ) || WPV_Columns::had_limit_wrapper() ) :  ?>
			</div>
		<?php endif ?>
		<div class="related-portfolios row vamtam-related-content" <?php WpvTemplates::display_none( rd_wpv_get_optionb( 'show-related-portfolios' ) ) ?>>
			<div class="clearfix limit-wrapper">
				<div class="grid-1-1">
					<?php echo wp_kses_post( apply_filters( 'wpv_related_portfolios_title', '<h2 class="related-content-title">' . rd_wpv_get_option( 'related-portfolios-title' ) . '</h2>' ) ); ?>
					<?php echo WPV_Projects::shortcode( array( // xss ok
						'column'       => 4,
						'type'         => $terms_slug,
						'ids'          => '',
						'max'          => 8,
						'height'       => 400,
						'show_title'   => 'below',
						'desc'         => true,
						'more'         => esc_html__( 'View', 'hair-beauty' ),
						'nopaging'     => 'true',
						'group'        => 'true',
						'layout'       => 'scrollable',
						'post__not_in' => get_the_ID(),
					) ); ?>
				</div>
			</div>
		</div>
		<?php if ( ! class_exists( 'WPV_Columns' ) || WPV_Columns::had_limit_wrapper() ) :  ?>
			<div class="limit-wrapper">
		<?php endif ?>
	<?php endif ?>
<?php get_footer();
