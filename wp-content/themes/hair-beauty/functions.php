<?php

/**
 * Theme functions. Initializes the Vamtam Framework.
 *
 * @package  wpv
 */

require_once get_template_directory() . '/vamtam/classes/framework.php';

new WpvFramework( array(
	'name' => 'hair',
	'slug' => 'hair',
) );

// TODO remove next line when the editor is fully functional, to be packaged as a standalone module with no dependencies to the theme
define( 'VAMTAM_EDITOR_IN_THEME', true ); include_once THEME_DIR . 'vamtam-editor/editor.php';

// only for one page home demos
function wpv_onepage_menu_hrefs( $atts, $item, $args ) {
	if ( 'custom' === $item->type && 0 === strpos( $atts['href'], '/#' ) ) {
		$atts['href'] = $GLOBALS['wpv_inner_path'] . $atts['href'];
	}
	return $atts;
}

if ( ( $path = parse_url( get_home_url(), PHP_URL_PATH ) ) !== null ) {
	$GLOBALS['wpv_inner_path'] = untrailingslashit( $path );
	add_filter( 'nav_menu_link_attributes', 'wpv_onepage_menu_hrefs', 10, 3 );
}

remove_action( 'admin_head', 'jordy_meow_flattr', 1 );

require_once WPV_DIR . 'customizer/setup.php';

// require_once WPV_DIR . 'redux/setup.php';
require_once WPV_DIR . 'customizer/preview.php';

function vamtam_ninja_starter_form( $contents ) {
	return wpv_silent_get_contents( WPV_SAMPLES_DIR . 'ninja-forms/1.nff' );
}
add_filter( 'ninja_forms_starter_form_contents', 'vamtam_ninja_starter_form' );

