<?php
/**
 * Search results template
 *
 * @package wpv
 * @subpackage hair
 */

$wpv_title = sprintf( esc_html__( 'Search Results for: %s', 'hair-beauty' ), '<span>' . get_search_query() . '</span>' );

get_header(); ?>

<div class="row page-wrapper">
	<?php if ( have_posts() ) : the_post(); ?>
		<?php WpvTemplates::$in_page_wrapper = true; ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class( WpvTemplates::get_layout() ); ?>>
			<?php
			global $wpv_has_header_sidebars;
			if ( $wpv_has_header_sidebars ) {
				WpvTemplates::header_sidebars();
			}
			?>
			<div class="page-content">
				<?php
				rewind_posts();
				get_template_part( 'loop', 'category' );
				get_template_part( 'templates/share' );
				?>
			</div>
		</article>
		<?php get_template_part( 'sidebar' ) ?>
	<?php else : ?>
	<article>
		<h1 style="text-align: center;margin-top: 35px;"><?php esc_html_e( 'Sorry, nothing found', 'hair-beauty' ) ?></h1>
	</article>
	<?php endif ?>
</div>

<?php get_footer(); ?>
