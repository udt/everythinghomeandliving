<?php

/**
 * Single portfolio content template
 * @package wpv
 */

$client = get_post_meta( get_the_id(), 'portfolio-client', true );
$logo   = get_post_meta( get_the_id(), 'portfolio-logo',   true );

$client = preg_replace( '@</\s*([^>]+)\s*>@', '</$1>', $client );

$content = get_the_content();

$portfolio_options = wpv_get_portfolio_options();
if ( 'gallery' === $portfolio_options['type'] ) {
	list( , $content ) = WpvPostFormats::get_first_gallery( $content );
}

$content = apply_filters( 'the_content',$content );

$has_right_column  = ! empty( $logo ) || ! empty( $client );
$left_column_width = $has_right_column ? 'grid-4-5' : 'grid-1-1 last';

$project_types = get_the_terms( get_the_id(), Jetpack_Portfolio::CUSTOM_TAXONOMY_TYPE );
$project_tags  = get_the_terms( get_the_id(), Jetpack_Portfolio::CUSTOM_TAXONOMY_TAG );

?>

<div class="clearfix">
	<?php if ( 'document' !== $type ) : ?>
		<div class="portfolio-image-wrapper fullwidth-folio">
			<?php
				if ( 'gallery' === $type ) :
					list( $gallery, ) = WpvPostFormats::get_first_gallery( get_the_content(), null, 'single-portfolio' );
					echo do_shortcode( $gallery );
				elseif ( 'video' === $type ) :
					global $wp_embed;
					echo do_shortcode( $wp_embed->run_shortcode( '[embed width="'.$size.'"]'.$href.'[/embed]' ) );
				elseif ( 'html' === $type ) :
					echo do_shortcode( get_post_meta( get_the_ID(), 'portfolio-top-html', true ) );
				else :
					the_post_thumbnail( 'theme-single' );
				endif;
			?>
		</div>
	<?php endif ?>
</div>

<div class="portfolio-text-content limit-wrapper">
	<div class="row portfolio-content">
		<div class="<?php echo esc_attr( $left_column_width ) ?> project-left-column">
			<?php echo $content; // xss ok ?>
		</div>

		<?php if ( $has_right_column ) : ?>
			<div class="grid-1-5 last project-right-column">

				<?php if ( ! empty( $logo ) ) : ?>
					<div class="cell">
						<img src="<?php echo esc_url( $logo ) ?>" alt="<?php the_title_attribute() ?>"/>
					</div>
				<?php endif ?>

				<div class="cell">
					<div  class="meta-title"><?php esc_html_e( 'Date', 'hair-beauty' ) ?></div>
					<p class="meta"><?php the_date() ?></p>
				</div>

				<?php if ( ! empty( $client ) ) : ?>
					<div class="cell">
						<div  class="meta-title"><?php esc_html_e( 'Client', 'hair-beauty' ) ?></div>
						<p class="client-details"><?php echo wp_kses_post( $client ) ?></p>
					</div>
				<?php endif ?>

				<?php if ( ! empty( $project_types ) && ! is_wp_error( $project_types ) ) : ?>
					<div class="cell">
						<div  class="meta-title"><?php esc_html_e( 'Types', 'hair-beauty' ) ?></div>
						<p class="meta"><?php echo wp_kses_post( implode( ', ', WpvTemplates::project_tax( Jetpack_Portfolio::CUSTOM_TAXONOMY_TYPE ) ) ) ?></p>
					</div>
				<?php endif ?>

				<?php if ( ! empty( $project_tags ) && ! is_wp_error( $project_tags ) ) : ?>
					<div class="cell">
						<div  class="meta-title"><?php esc_html_e( 'Tags', 'hair-beauty' ) ?></div>
						<p class="meta"><?php echo wp_kses_post( implode( ', ', WpvTemplates::project_tax( Jetpack_Portfolio::CUSTOM_TAXONOMY_TAG ) ) ) ?></p>
					</div>
				<?php endif ?>

				<?php get_template_part( 'templates/share' ); ?>
			</div>
		<?php endif ?>
	</div>
</div>
