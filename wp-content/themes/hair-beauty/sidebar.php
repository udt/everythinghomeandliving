<?php

$layout_type = WpvTemplates::get_layout();

if ( 'left-only' === $layout_type || 'left-right' === $layout_type ) : ?>
	<aside class="<?php echo esc_attr( apply_filters( 'wpv_left_sidebar_class', 'left', $layout_type ) ) ?>">
		<?php WpvSidebars::get_instance()->get_sidebar( 'left' ); ?>
	</aside>
<?php endif;

if ( 'right-only' === $layout_type || 'left-right' === $layout_type ) : ?>
	<aside class="<?php echo esc_attr( apply_filters( 'wpv_right_sidebar_class', 'right', $layout_type ) ) ?>">
		<?php WpvSidebars::get_instance()->get_sidebar( 'right' ); ?>
	</aside>
<?php endif;

WpvTemplates::$in_page_wrapper = false;
