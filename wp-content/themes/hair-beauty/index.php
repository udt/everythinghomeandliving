<?php
/**
 * Catch-all template
 *
 * @package wpv
 * @subpackage hair
 */



$format = get_query_var( 'format_filter' );
$wpv_title = $format ? sprintf( esc_html__( 'Post format: %s', 'hair-beauty' ), $format ) : esc_html__( 'Blog', 'hair-beauty' );
get_header();
?>
<div class="row page-wrapper">

	<article <?php post_class( WpvTemplates::get_layout() ) ?>>
		<?php
		global $wpv_has_header_sidebars;
		if ( $wpv_has_header_sidebars ) {
			WpvTemplates::header_sidebars();
		}
		?>
		<div class="page-content">
			<?php get_template_part( 'loop', 'index' ); ?>
		</div>
	</article>

	<?php get_template_part( 'sidebar' ) ?>
</div>
<?php get_footer(); ?>
