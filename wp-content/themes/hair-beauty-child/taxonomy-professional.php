<?php
/**
 * Template Name: Professional Profile
 *
 * Description: This is profile Page Template.
 */
 	
$term_id = get_queried_object()->term_id;
$profession = get_woocommerce_term_meta( $term_id, 'product_profession', true );
$tel = get_woocommerce_term_meta( $term_id, 'professional_tel', true );
$email = get_woocommerce_term_meta( $term_id, 'professional_email', true );
$site = get_woocommerce_term_meta( $term_id, 'professional_website', true );
$address = get_woocommerce_term_meta( $term_id, 'professional_address', true );
$facebook 	= get_woocommerce_term_meta( $term_id, 'professional_facebook', true );
$twitter 	= get_woocommerce_term_meta( $term_id, 'professional_twitter', true );
$instagram 	= get_woocommerce_term_meta( $term_id, 'professional_instagram', true );
$current_services = get_woocommerce_term_meta( $term_id, 'professional_service', true );
$thumbnail_id 	= get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );
$thumbnail = wp_get_attachment_url( $thumbnail_id );

get_header();

?>

<div id="main" role="main" class="wpv-main layout-right-only  ">
						
	<div class="limit-wrapper">
						
		<div class="row page-wrapper">
		
			<article id="post-9488" class="right-only post-9488 page type-page status-publish hentry">
				<div class="page-content">
					<div class="row ">
						<div class="wpv-grid grid-1-1  wpv-first-level grid-1-1 first unextended no-extended-padding" style="padding-top:0.05px;padding-bottom:0.05px;" id="wpv-column-7cc66ec60b8e6054d16f01efaf559532">
							<img src="<?php echo $thumbnail ?>" class="pull-left professional-image" ?>
							<?php echo term_description( $term_id ); ?>
						</div>
					</div>
					<div class="limit-wrapper">	
					<h2 class="text-divider-double">Portfolio</h2>
					<div class="sep"></div>
					<div class="row ">
						<?php 
						$args = array(
						    'post_type'             => 'product',
						    'post_status'           => 'publish',
						    'ignore_sticky_posts'   => 1,
						    'posts_per_page'        => '4',
						    'tax_query'             => array(
						        array(
						            'taxonomy'      => 'professional',
						            'field' 		=> 'term_id',
						            'terms'         => $term_id,
						            'operator'      => 'IN'
						        )
						    )
						);
						$products = new WP_Query($args);
						$ids = 0;					
						$counter = 1;
						while ( $products->have_posts() ) : $products->the_post();
							if($counter > 4) return;
							global $product;
							if($counter == 1){
								$ids = $product->get_id();
							} else {
								$ids .= ', ' . $product->get_id();
							}
							$counter++;
						endwhile;
						if($ids != 0) echo do_shortcode('[products ids="'.$ids.'"]');
						wp_reset_query(); ?>
					</div>
				</div>
				<?php if($instagram) { ?>
					<?php echo do_shortcode('[instagram-feed id="'.$instagram.'"]'); ?>
				<?php } ?>
				<div class="row ">
					<div class="wpv-grid grid-1-1  wpv-first-level grid-1-1 first unextended has-extended-padding" style="padding-top:0.05px;padding-bottom:0.05px;">
						<div class="row ">
							<div class="wpv-grid grid-1-1 unextended has-extended-padding" style="padding-top:0.05px;padding-bottom:0.05px;">
								<div class="row ">
									<?php if($facebook) { ?>
									<div class="wpv-grid grid-1-3 first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top:0.05px;padding-bottom:0.05px;">
										<div data-target="_blank" class="linkarea clearfix   background-transparent">
											<a href="<?php echo $facebook ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i>
											<h4 style="text-align: center;">Follow us on Facebook</h4></a>
										</div>
									</div>
									<?php } ?>
									<?php if($twitter) { ?>
									<div class="wpv-grid grid-1-3 first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top:0.05px;padding-bottom:0.05px;">
										<div data-target="_blank" class="linkarea clearfix text-center  background-transparent">
											<a href="<?php echo $twitter ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>
											<h4 style="text-align: center;">Follow us on Twitter</h4></a>
										</div>
									</div>
									<?php } ?>									
								</div>
							</div>

						</div>
					</div>
				</div>
			<div class="limit-wrapper">
			</div>
		</article>

		<aside class="right">
			<section id="text-26" class="widget widget_text">
				<h4 class="widget-title">PRICE:</h4>
					<div class="textwidget">
						<table class="vamtam-styled">
							<tbody>
								<?php foreach ($current_services as $current_service) { ?>
									<tr>
										<td>
											<p class="p1"><?php echo $current_service['service'] ?></p>
										</td>										
										<td>
											<p class="p1" style="text-align: right;"><strong>£ <?php echo $current_service['price'] ?></strong></p>
										</td>
									</tr>
								<?php } ?>								
							</tbody>
						</table>
					</div>
				</section>
				<section id="text-27" class="widget widget_text">
				<h4 class="widget-title">CONTACTS</h4>
					<div class="textwidget">
						<?php if($profession) { ?>
							<p>Profession: <?php echo $profession ?></p>
						<?php } ?>
						<?php if($tel) { ?>
							<p>Tel: <?php echo $tel ?></p>
						<?php } ?>
						<?php if($email) { ?>
							<p>Email: <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p>
						<?php } ?>
						<?php if($site) { ?>
							<p>Website: <a href="<?php echo $site ?>"><?php echo $site ?></a></p>
						<?php } ?>
						<?php if($address) { ?>
							<p>Address: <?php echo $address ?></p>
						<?php } ?>
					</div>
				</section>
				<section id="text-28" class="widget widget_text">
					<h4 class="widget-title">REVIEWS</h4>
					<?php
						$comments = get_comments( array( 'number' => $number, 'status' => 'approve', 'post_status' => 'publish', 'post_type' => 'product', 'posts_per_page' => 4 ) );

					if ( $comments ) {
						echo '<ul class="product_list_widget">';

						foreach ( (array) $comments as $comment ) {							

							$_product = wc_get_product( $comment->comment_post_ID );

							//var_dump($_product);
							if( !has_term( $term_id, 'professional', $_product->id ) ){
								continue;
							}

							$rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );

							$rating_html = wc_get_rating_html( $rating );

							echo '<li><a href="' . esc_url( get_comment_link( $comment->comment_ID ) ) . '">';

							echo $_product->get_image() . wp_kses_post( $_product->get_name() ) . '</a>';

							echo '<p>' . substr($comment->comment_content, 0, 30) . '...</p>';

							echo $rating_html;

							/* translators: %s: review author */
							echo '<span class="reviewer">' . sprintf( esc_html__( 'by %s', 'woocommerce' ), get_comment_author() ) . '</span>';

							echo '</li>';
						}

						echo '</ul>';
					} else {
						echo 'No comments yet';
					}
					?>
				</section>
			</aside>
		</div>
	</div> <!-- .limit-wrapper -->	
</div>

<?php get_footer(); ?>