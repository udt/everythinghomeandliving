<?php

function child_styles() {
	wp_enqueue_style( 'my-child-theme-style', get_stylesheet_directory_uri() . '/style.css', array('front-all'), false, 'all' );
	wp_enqueue_script( 'my-child-theme-script', get_stylesheet_directory_uri() . '/js/scripts.js' );
	wp_enqueue_script( 'owl-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js' );
	wp_enqueue_style( 'owl-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.css' );
}
add_action('wp_enqueue_scripts', 'child_styles', 11);

add_action( 'woocommerce_register_taxonomy', 'init_taxonomy' );

add_action( 'admin_enqueue_scripts', 'prof_admin_scripts' );
add_action( 'professional_add_form_fields', 'add_thumbnail_field' );
add_action( 'professional_add_form_fields', 'add_prof_fields' );
add_action( 'professional_edit_form_fields', 'edit_thumbnail_field', 10, 2 );
add_action( 'professional_edit_form_fields', 'edit_prof_fields', 10, 2 );
add_action( 'created_term', 'thumbnail_field_save', 10, 3 );
add_action( 'edit_term', 'thumbnail_field_save', 10, 3 );
add_action( 'woocommerce_single_product_summary', 'product_profession', 15);
add_action( 'woocommerce_single_product_summary', 'product_name', 19);

add_action( 'woocommerce_product_tabs', 'woocommerce_new_product_tabs');

add_filter( 'manage_edit-professional_columns', 'prof_admin_columns' );
add_filter( 'manage_professional_custom_column', 'prof_admin_column', 10, 3);

function init_taxonomy() {
	global $woocommerce;

	$shop_page_id = wc_get_page_id( 'shop' );

	$base_slug = $shop_page_id > 0 && get_page( $shop_page_id ) ? get_page_uri( $shop_page_id ) : 'shop';

	$category_base = get_option('woocommerce_prepend_shop_page_to_urls') == "yes" ? trailingslashit( $base_slug ) : '';

	register_taxonomy( 'professional',
		array('product'),
		apply_filters( 'register_taxonomy_product_brand', array(
			'hierarchical'          => true,
			'update_count_callback' => '_update_post_term_count',
			'label'                 => __( 'Professionals', 'wc_brands'),
			'labels'                => array(
					'name'              => __( 'Professionals', 'wc_brands' ),
					'singular_name'     => __( 'Professional', 'wc_brands' ),
					'search_items'      => __( 'Search Professionals', 'wc_brands' ),
					'all_items'         => __( 'All Professionals', 'wc_brands' ),
					'parent_item'       => __( 'Parent Professional', 'wc_brands' ),
					'parent_item_colon' => __( 'Parent Professional:', 'wc_brands' ),
					'edit_item'         => __( 'Edit Professional', 'wc_brands' ),
					'update_item'       => __( 'Update Professional', 'wc_brands' ),
					'add_new_item'      => __( 'Add New Professional', 'wc_brands' ),
					'new_item_name'     => __( 'New Professional Name', 'wc_brands' )
			),

			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'capabilities'      => array(
				'manage_terms' => 'manage_product_terms',
				'edit_terms'   => 'edit_product_terms',
				'delete_terms' => 'delete_product_terms',
				'assign_terms' => 'assign_product_terms'
			),

			'rewrite' => array( 'slug' => $category_base . __( 'professional', 'wc_brands' ), 'with_front' => true, 'hierarchical' => true )
		) )
	);
}

function prof_admin_scripts() {
	$screen = get_current_screen();

	if ( in_array( $screen->id, array( 'edit-professional' ) ) ) {
		wp_enqueue_media();
	}
}

function prof_admin_columns( $columns ) {
	if ( empty( $columns ) ) {
		return;
	}
	
	$new_columns = array();
	$new_columns['cb'] = $columns['cb'];
	$new_columns['thumb'] = __('Image', 'wc_brands');
	unset( $columns['cb'] );
	$columns = array_merge( $new_columns, $columns );
	return $columns;
}

function prof_admin_column( $columns, $column, $id ) {
	if ( $column == 'thumb' ) {
		global $woocommerce;

		$image        = '';
		$thumbnail_id = get_woocommerce_term_meta( $id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image = wp_get_attachment_url( $thumbnail_id );
		}
		if ( empty( $image ) ) {
			$image = wc_placeholder_img_src();
		}

		$columns .= '<img src="' . $image . '" alt="Thumbnail" class="wp-post-image" height="48" width="48" />';

	}
	return $columns;
}

function add_thumbnail_field() {
	global $woocommerce;
	?>
	<div class="form-field">
		<label><?php _e( 'Thumbnail', 'wc_brands' ); ?></label>
		<div id="product_cat_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo wc_placeholder_img_src(); ?>" width="60px" height="60px" /></div>
		<div style="line-height:60px;">
			<input type="hidden" id="product_cat_thumbnail_id" name="product_cat_thumbnail_id" />
			<button type="button" class="upload_image_button button"><?php _e('Upload/Add image', 'wc_brands'); ?></button>
			<button type="button" class="remove_image_button button"><?php _e('Remove image', 'wc_brands'); ?></button>
		</div>
		<script type="text/javascript">

			jQuery(function(){
				 // Only show the "remove image" button when needed
				 if ( ! jQuery('#product_cat_thumbnail_id').val() ) {
					 jQuery('.remove_image_button').hide();
				 }

				// Uploading files
				var file_frame;

				jQuery(document).on( 'click', '.upload_image_button', function( event ){

					event.preventDefault();

					// If the media frame already exists, reopen it.
					if ( file_frame ) {
						file_frame.open();
						return;
					}

					// Create the media frame.
					file_frame = wp.media.frames.downloadable_file = wp.media({
						title: '<?php _e( 'Choose an image', 'wc_brands' ); ?>',
						button: {
							text: '<?php _e( 'Use image', 'wc_brands' ); ?>',
						},
						multiple: false
					});

					// When an image is selected, run a callback.
					file_frame.on( 'select', function() {
						attachment = file_frame.state().get('selection').first().toJSON();

						jQuery('#product_cat_thumbnail_id').val( attachment.id );
						jQuery('#product_cat_thumbnail img').attr('src', attachment.url );
						jQuery('.remove_image_button').show();
					});

					// Finally, open the modal.
					file_frame.open();
				});

				jQuery(document).on( 'click', '.remove_image_button', function( event ){
					jQuery('#product_cat_thumbnail img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
					jQuery('#product_cat_thumbnail_id').val('');
					jQuery('.remove_image_button').hide();
					return false;
				});
			});

		</script>
		<div class="clear"></div>
	</div>
	<div class="form-field">
		<label><?php _e( 'Title Background', 'wc_brands' ); ?></label>
		<div id="product_cat_title_bg" style="float:left;margin-right:10px;"><img src="<?php echo wc_placeholder_img_src(); ?>" width="60px" height="60px" /></div>
		<div style="line-height:60px;">
			<input type="hidden" id="product_cat_title_bg_id" name="product_cat_title_bg_id" />
			<button type="button" class="upload_tbg_button button"><?php _e('Upload/Add image', 'wc_brands'); ?></button>
			<button type="button" class="remove_tbg_button button"><?php _e('Remove image', 'wc_brands'); ?></button>
		</div>
		<script type="text/javascript">

			jQuery(function(){
				 // Only show the "remove image" button when needed
				 if ( ! jQuery('#product_cat_title_bg_id').val() ) {
					 jQuery('.remove_tbg_button').hide();
				 }

				// Uploading files
				var file_frame;

				jQuery(document).on( 'click', '.upload_tbg_button', function( event ){

					event.preventDefault();

					// If the media frame already exists, reopen it.
					if ( file_frame ) {
						file_frame.open();
						return;
					}

					// Create the media frame.
					file_frame = wp.media.frames.downloadable_file = wp.media({
						title: '<?php _e( 'Choose an image', 'wc_brands' ); ?>',
						button: {
							text: '<?php _e( 'Use image', 'wc_brands' ); ?>',
						},
						multiple: false
					});

					// When an image is selected, run a callback.
					file_frame.on( 'select', function() {
						attachment = file_frame.state().get('selection').first().toJSON();

						jQuery('#product_cat_title_bg_id').val( attachment.id );
						jQuery('#product_cat_title_bg img').attr('src', attachment.url );
						jQuery('.remove_tbg_button').show();
					});

					// Finally, open the modal.
					file_frame.open();
				});

				jQuery(document).on( 'click', '.remove_tbg_button', function( event ){
					jQuery('#product_cat_title_bg img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
					jQuery('#product_cat_title_bg_id').val('');
					jQuery('.remove_tbg_button').hide();
					return false;
				});
			});

		</script>
		<div class="clear"></div>
	</div>
	<?php
}

function edit_thumbnail_field( $term, $taxonomy ) {
	global $woocommerce;

	$image = $title_img	= '';
	$thumbnail_id 	= get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
	if ($thumbnail_id) {
		$image = wp_get_attachment_url( $thumbnail_id );
	}
	if ( empty( $image ) ) {
		$image = wc_placeholder_img_src();
	};
	$title_bg_id 	= get_woocommerce_term_meta( $term->term_id, 'title_bg_id', true );
	if ($title_bg_id) {
		$title_img = wp_get_attachment_url( $title_bg_id );
	}
	if ( empty( $title_img ) ) {
		$title_img = wc_placeholder_img_src();
	};
	?>
	<tr class="form-field">
		<th scope="row" valign="top"><label><?php _e('Thumbnail', 'wc_brands'); ?></label></th>
		<td>
			<div id="product_cat_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo $image; ?>" width="60px" height="60px" /></div>
			<div style="line-height:60px;">
				<input type="hidden" id="product_cat_thumbnail_id" name="product_cat_thumbnail_id" value="<?php echo $thumbnail_id; ?>" />
				<button type="button" class="upload_image_button button"><?php _e('Upload/Add image', 'wc_brands'); ?></button>
				<button type="button" class="remove_image_button button"><?php _e('Remove image', 'wc_brands'); ?></button>
			</div>
			<script type="text/javascript">

				jQuery(function(){

					 // Only show the "remove image" button when needed
					 if ( ! jQuery('#product_cat_thumbnail_id').val() )
						 jQuery('.remove_image_button').hide();

					// Uploading files
					var file_frame;

					jQuery(document).on( 'click', '.upload_image_button', function( event ){

						event.preventDefault();

						// If the media frame already exists, reopen it.
						if ( file_frame ) {
							file_frame.open();
							return;
						}

						// Create the media frame.
						file_frame = wp.media.frames.downloadable_file = wp.media({
							title: '<?php _e( 'Choose an image', 'wc_brands' ); ?>',
							button: {
								text: '<?php _e( 'Use image', 'wc_brands' ); ?>',
							},
							multiple: false
						});

						// When an image is selected, run a callback.
						file_frame.on( 'select', function() {
							attachment = file_frame.state().get('selection').first().toJSON();

							jQuery('#product_cat_thumbnail_id').val( attachment.id );
							jQuery('#product_cat_thumbnail img').attr('src', attachment.url );
							jQuery('.remove_image_button').show();
						});

						// Finally, open the modal.
						file_frame.open();
					});

					jQuery(document).on( 'click', '.remove_image_button', function( event ){
						jQuery('#product_cat_thumbnail img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
						jQuery('#product_cat_thumbnail_id').val('');
						jQuery('.remove_image_button').hide();
						return false;
					});
				});

			</script>
			<div class="clear"></div>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label><?php _e('Title background', 'wc_brands'); ?></label></th>
		<td>
			<div id="product_cat_title_bg" style="float:left;margin-right:10px;"><img src="<?php echo $title_img; ?>" width="60px" height="60px" /></div>
			<div style="line-height:60px;">
				<input type="hidden" id="product_cat_title_bg_id" name="product_cat_title_bg_id" value="<?php echo $title_bg_id; ?>" />
				<button type="button" class="upload_tbg_button button"><?php _e('Upload/Add image', 'wc_brands'); ?></button>
				<button type="button" class="remove_tbg_button button"><?php _e('Remove image', 'wc_brands'); ?></button>
			</div>
			<script type="text/javascript">

				jQuery(function(){

					 // Only show the "remove image" button when needed
					 if ( ! jQuery('#product_cat_title_bg_id').val() )
						 jQuery('.remove_tbg_button').hide();

					// Uploading files
					var file_frame;

					jQuery(document).on( 'click', '.upload_tbg_button', function( event ){

						event.preventDefault();

						// If the media frame already exists, reopen it.
						if ( file_frame ) {
							file_frame.open();
							return;
						}

						// Create the media frame.
						file_frame = wp.media.frames.downloadable_file = wp.media({
							title: '<?php _e( 'Choose an image', 'wc_brands' ); ?>',
							button: {
								text: '<?php _e( 'Use image', 'wc_brands' ); ?>',
							},
							multiple: false
						});

						// When an image is selected, run a callback.
						file_frame.on( 'select', function() {
							attachment = file_frame.state().get('selection').first().toJSON();

							jQuery('#product_cat_title_bg_id').val( attachment.id );
							jQuery('#product_cat_title_bg img').attr('src', attachment.url );
							jQuery('.remove_tbg_button').show();
						});

						// Finally, open the modal.
						file_frame.open();
					});

					jQuery(document).on( 'click', '.remove_tbg_button', function( event ){
						jQuery('#product_cat_title_bg img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
						jQuery('#product_cat_title_bg_id').val('');
						jQuery('.remove_tbg_button').hide();
						return false;
					});
				});

			</script>
			<div class="clear"></div>
		</td>
	</tr>
	<?php
}

function add_prof_fields() {
	global $woocommerce;
	?>
	<script type="text/javascript">
		jQuery(document).ready(function(){			
			jQuery("#procedure_treatment").chosen({no_results_text: "Hit enter to add"});			
		});
		function add_pro_service_field() {
			var index = parseInt(jQuery("#pro_service_index").val(), 10);
			jQuery("#professional_service_list tbody").append(''+
			'<tr>'+
				'<td><input class="service-field required-field" type="text" name="professional_service['+index+'][service]" value="" /></td>'+
				'<td><input class="service-field required-field" type="text" name="professional_service['+index+'][category]" value="" /></td>'+
				'<td><input class="price-field required-field" type="text" name="professional_service['+index+'][price]" value="" /></td>'+
				'<td><input type="button" onclick="jQuery(this).closest(\'tr\').remove();return false;" class="hide-form" value="X" /></td>'+
			'</tr>'+
			'');
			index++;
			jQuery("#pro_service_index").val(index);
			return index - 1;
		}
		function add_pro_service_list() {
			var rows, row, index;
			rows = jQuery("#add_multiple_services_content").val().split("\n")
			for(var i = 0; i < rows.length; i++)
			{
				index = add_pro_service_field();
				row = rows[i].split(';');
				if(row[0] !== undefined)
				{
					jQuery('.service-field[name="professional_service['+index+'][service]"]').val(row[0]);
				}
				if(row[1] !== undefined)
				{
					jQuery('.service-field[name="professional_service['+index+'][category]"]').val(row[1]);
				}
				if(row[2] !== undefined)
				{
					jQuery('.price-field[name="professional_service['+index+'][price]"]').val(row[2]);
				}
			}
			jQuery("#add_multiple_services_content").val('');
		}
	</script>
	<div class="form-field">
		<label for="product_profession"><?php _e( 'Profession', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="product_profession" name="product_profession" />
		</div>
		<div class="clear"></div>
		<label for="professional_email"><?php _e( 'Email', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="professional_email" name="professional_email" />
		</div>
		<div class="clear"></div>
		<label for="professional_website"><?php _e( 'Website', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="professional_website" name="professional_website" />
		</div>
		<div class="clear"></div>
		<label for="professional_tel"><?php _e( 'Tel', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="professional_tel" name="professional_tel" />
		</div>
		<div class="clear"></div>
		<label for="professional_address"><?php _e( 'Address', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="professional_address" name="professional_address" />
		</div>
		<div class="clear"></div>
		<label for="professional_facebook"><?php _e( 'Facebook url', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="professional_facebook" name="professional_facebook" />
		</div>
		<div class="clear"></div>
		<label for="professional_twitter"><?php _e( 'Twitter url', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="professional_twitter" name="professional_twitter" />
		</div>
		<div class="clear"></div>
		<label for="professional_instagram"><?php _e( 'Instagram url', 'wc_brands' ); ?></label>
		<div>
			<input type="text" id="professional_instagram" name="professional_instagram" />
		</div>
		<div class="clear"></div>
	</div>
	<?php
}

function edit_prof_fields( $term, $taxonomy ) {
	global $woocommerce;
	$profession 	= get_woocommerce_term_meta( $term->term_id, 'product_profession', true );
	$email 	= get_woocommerce_term_meta( $term->term_id, 'professional_email', true );
	$tel 	= get_woocommerce_term_meta( $term->term_id, 'professional_tel', true );
	$address 	= get_woocommerce_term_meta( $term->term_id, 'professional_address', true );
	$website 	= get_woocommerce_term_meta( $term->term_id, 'professional_website', true );
	$facebook 	= get_woocommerce_term_meta( $term->term_id, 'professional_facebook', true );
	$twitter 	= get_woocommerce_term_meta( $term->term_id, 'professional_twitter', true );
	$instagram 	= get_woocommerce_term_meta( $term->term_id, 'professional_instagram', true );
	$current_services = get_woocommerce_term_meta( $term->term_id, 'professional_service', true );

	?>
	<script type="text/javascript">
		jQuery(document).ready(function(){			
			jQuery("#procedure_treatment").chosen({no_results_text: "Hit enter to add"});			
		});
		function add_pro_service_field() {
			var index = parseInt(jQuery("#pro_service_index").val(), 10);
			jQuery("#professional_service_list tbody").append(''+
			'<tr>'+
				'<td><input class="service-field required-field" type="text" name="professional_service['+index+'][service]" value="" /></td>'+			
				'<td><input class="price-field required-field" type="text" name="professional_service['+index+'][price]" value="" /></td>'+
				'<td><input type="button" onclick="jQuery(this).closest(\'tr\').remove();return false;" class="hide-form" value="X" /></td>'+
			'</tr>'+
			'');
			index++;
			jQuery("#pro_service_index").val(index);
			return index - 1;
		}
		function add_pro_service_list() {
			var rows, row, index;
			rows = jQuery("#add_multiple_services_content").val().split("\n")
			for(var i = 0; i < rows.length; i++)
			{
				index = add_pro_service_field();
				row = rows[i].split(';');
				if(row[0] !== undefined)
				{
					jQuery('.service-field[name="professional_service['+index+'][service]"]').val(row[0]);
				}
				if(row[1] !== undefined)
				{
					jQuery('.price-field[name="professional_service['+index+'][price]"]').val(row[2]);
				}
			}
			jQuery("#add_multiple_services_content").val('');
		}
	</script>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="product_profession"><?php _e('Profession', 'wc_brands'); ?></label></th>
		<td>
			<div>
				<input type="text" id="product_profession" name="product_profession" value="<?php echo $profession ?>" />
			</div>
			<div class="clear"></div>
		</td>		
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="professional_email"><?php _e( 'Email', 'wc_brands' ); ?></label></th>
		<td>
			<div>
				<input type="text" id="professional_email" name="professional_email" value="<?php echo $email ?>" />
			</div>
			<div class="clear"></div>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="professional_website"><?php _e( 'Website', 'wc_brands' ); ?></label></th>
		<td>
			<div>
				<input type="text" id="professional_website" name="professional_website" value="<?php echo $website ?>" />
			</div>
			<div class="clear"></div>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="professional_tel"><?php _e( 'Tel', 'wc_brands' ); ?></label></th>
		<td>
			<div>
				<input type="text" id="professional_tel" name="professional_tel" value="<?php echo $tel ?>" />
			</div>
			<div class="clear"></div>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="professional_address"><?php _e( 'Address', 'wc_brands' ); ?></label></th>
		<td>
			<div>
				<input type="text" id="professional_address" name="professional_address" value="<?php echo $address ?>" />
			</div>
			<div class="clear"></div>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="professional_facebook"><?php _e( 'Facebook', 'wc_brands' ); ?></label></th>
		<td>
			<div>
				<input type="text" id="professional_facebook" name="professional_facebook" value="<?php echo $facebook ?>" />
			</div>
			<div class="clear"></div>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="professional_twitter"><?php _e( 'Twitter', 'wc_brands' ); ?></label></th>
		<td>
			<div>
				<input type="text" id="professional_twitter" name="professional_twitter" value="<?php echo $twitter ?>" />
			</div>
			<div class="clear"></div>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="professional_instagram"><?php _e( 'Instagram', 'wc_brands' ); ?></label></th>
		<td>
			<div>
				<input type="text" id="professional_instagram" name="professional_instagram" value="<?php echo $instagram ?>" />
			</div>
			<div class="clear"></div>
		</td>
	</tr>
	<tr>
		<th><label for="procedure_treatment">Service List<br />Invalid rows will not be saved</label></th>
		<td>
			<div id="professional_services" class="treatments profile_details_form_class">
				<input type="button" onclick="add_pro_service_field();return false;" value="Add New Row" class="add-service-button pink-submit-button" />
				<input type="button" onclick="jQuery('#add_multiple_services').toggle();return false;" value="Add Multiple Rows At Once" class="add-service-button pink-submit-button" />
				<div id="add_multiple_services" style="display: none; margin-top: 3px;">
					<textarea id="add_multiple_services_content" placeholder="Add one service per line, separate service name, category and price with ; Example: My Service;Skin treatment;15.50" style="min-height: 150px;"></textarea>
					<input type="button" onclick="add_pro_service_list();return false;" style="display: block;" value="Add Services" class="add-service-button pink-submit-button" />
				</div>
				<table id="professional_service_list" style="width: 85%">
					<thead>
						<tr>
							<th>Treatment/Product Name (cannot be empty)</th>
							<th width="160px;">Service price in &pound; (must be number)</th>
							<th width="32px;">Del</th>
						</tr>
					</thead>
					<tbody>
				<?php 				
				$index = 0;
				if(is_array($current_services))
				{
					foreach ($current_services as $service)
					{
					?>
						<tr>
							<td><input class="service-field required-field" type="text" name="professional_service[<?php echo $index; ?>][service]" value="<?php echo $service['service']; ?>" /></td>							
							<td><input class="price-field required-field" type="text" name="professional_service[<?php echo $index; ?>][price]" value="<?php echo $service['price']; ?>" /></td>
							<td><input type="button" onclick="jQuery(this).closest('tr').remove();return false;" class="hide-form" value="X" /></td>
						</tr>
					<?php
						$index++;
					}
				}
				?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4">
								<input type="hidden" id="pro_service_index" value="<?php echo $index; ?>" />
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</td>
	</tr>
	<?php
}

function thumbnail_field_save( $term_id, $tt_id, $taxonomy ) {

	if($taxonomy != "professional") return;

	if ( isset( $_POST['product_cat_thumbnail_id'] ) ) {
		update_woocommerce_term_meta( $term_id, 'thumbnail_id', absint( $_POST['product_cat_thumbnail_id'] ) );
	}
	if ( isset( $_POST['product_cat_title_bg_id'] ) ) {
		update_woocommerce_term_meta( $term_id, 'title_bg_id', absint( $_POST['product_cat_title_bg_id'] ) );
	}
	if ( isset( $_POST['product_profession'] ) ) {
		update_woocommerce_term_meta( $term_id, 'product_profession', $_POST['product_profession'] );
	}
	if ( isset( $_POST['professional_email'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_email', $_POST['professional_email'] );
	}
	if ( isset( $_POST['professional_tel'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_tel', $_POST['professional_tel'] );
	}
	if ( isset( $_POST['professional_facebook'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_facebook', $_POST['professional_facebook'] );
	}
	if ( isset( $_POST['professional_twitter'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_twitter', $_POST['professional_twitter'] );
	}
	if ( isset( $_POST['professional_instagram'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_instagram', $_POST['professional_instagram'] );
	}
	if ( isset( $_POST['professional_service'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_service', $_POST['professional_service'] );
	}
	if ( isset( $_POST['professional_address'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_address', $_POST['professional_address'] );
	}
	if ( isset( $_POST['professional_website'] ) ) {
		update_woocommerce_term_meta( $term_id, 'professional_website', $_POST['professional_website'] );
	}
}

function product_profession(){
	global $product;

	$terms = wp_get_post_terms( $product->id, 'professional' );
	if($terms){
		echo '<div><p>Professional: ';
		foreach ($terms as $term) {
			echo '<a href="'.get_term_link($term->term_id).'">'.$term->name.'</a> ';
		}
		echo '</p></div>';
	}
}

function product_name(){
	global $product;

	echo '<div><h2>' . $product->name . '</h2></div>';
}

function woocommerce_new_product_tabs( $tabs = array() ) {
	global $product, $post;

	$terms = wp_get_post_terms( $product->id, 'professional' );

	if ( $terms ) {
		$tabs['about_brand'] = array(
			'title'    => __( 'About The Brand', 'woocommerce' ),
			'priority' => 40,
			'callback' => 'about_brand_tab',
		);
	}
	if(get_post_meta( $post->ID, 'product_ingridients', true )){
		$tabs['product_ingridients'] = array(
			'title'    => __( 'Ingridients', 'woocommerce' ),
			'priority' => 45,
			'callback' => 'product_ingridients_tab',
		);
	}
	if(get_post_meta( $post->ID, 'features_benefits', true )){
		$tabs['features_benefits'] = array(
			'title'    => __( 'Features & Benefits', 'woocommerce' ),
			'priority' => 45,
			'callback' => 'features_benefits_tab',
		);
	}

	return $tabs;
}

function about_brand_tab($terms) {
	global $product, $post;
	$terms = wp_get_post_terms( $product->id, 'professional' );
	if($terms){
		foreach ($terms as $term) {
			echo '<h3>'.$term->name.'</h3> ';
			echo '<p>'.$term->description.'</p> ';
		}
	}	
}

function product_ingridients_tab(){
	global $product, $post;
	echo get_post_meta( $post->ID, 'product_ingridients', true );
}

function features_benefits_tab(){
	global $product, $post;
	echo get_post_meta( $post->ID, 'features_benefits', true );
}

add_action( 'add_meta_boxes', 'add_product_meta_boxes' );

function add_product_meta_boxes() {

	add_meta_box(
		'product-ingridients',
		esc_html__( 'Product Ingridients and Features', '' ),
		'product_ingridients_meta_box',
		'product',
		'normal',
		'default'
	);

}

function product_ingridients_meta_box($post){
	wp_nonce_field( basename( __FILE__ ), 'smashing_post_class_nonce' );
	?>

  	<p>
	    <label for="product_ingridients"><?php _e( "Ingridients", 'example' ); ?></label>
	    <br />
	    <textarea class="widefat" name="product_ingridients" id="product_ingridients" /><?php echo esc_html( get_post_meta( $post->ID, 'product_ingridients', true ) ); ?></textarea>
  	</p>

  	<p>
	    <label for="features_benefits"><?php _e( "Features & Benefits", 'example' ); ?></label>
	    <br />
	    <textarea class="widefat" name="features_benefits" id="features_benefits" /><?php echo esc_html( get_post_meta( $post->ID, 'features_benefits', true ) ); ?></textarea>
  	</p>

	<?php
}

add_action( 'save_post', 'product_ingridients_save_post_class_meta', 10, 2 );

function product_ingridients_save_post_class_meta( $post_id, $post ) {

  if ( !isset( $_POST['smashing_post_class_nonce'] ) || !wp_verify_nonce( $_POST['smashing_post_class_nonce'], basename( __FILE__ ) ) )
    return $post_id;

  $post_type = get_post_type_object( $post->post_type );

  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;

  $product_ingridients = ( isset( $_POST['product_ingridients'] ) ? esc_html( $_POST['product_ingridients'] ) : '' );
  $features_benefits = ( isset( $_POST['features_benefits'] ) ? esc_html( $_POST['features_benefits'] ) : '' );

  update_post_meta( $post_id, 'product_ingridients', $product_ingridients );
  update_post_meta( $post_id, 'features_benefits', $features_benefits );

}


function wooc_extra_register_fields() {?>
    <p class="form-row form-row-first">
        <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>
    <p class="form-row form-row-last">
        <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>
    <p class="form-row form-row-wide">
        <label for="reg_billing_address_2"><?php _e( '	Your Home Town', 'woocommerce' ); ?></label>
        <input type="text" class="input-text" name="billing_address_2" id="reg_billing_address_2" value="<?php esc_attr_e( $_POST['billing_address_2'] ); ?>" />
    </p>
    <p class="form-row form-row-wide">
        <label for="reg_billing_city"><?php _e( 'Your Home City', 'woocommerce' ); ?></label>
        <input type="text" class="input-text" name="billing_city" id="reg_billing_city" value="<?php esc_attr_e( $_POST['billing_city'] ); ?>" />
    </p>
    <p class="form-row form-row-wide">
        <label for="reg_billing_postcode"><?php _e( 'Your Postcode', 'woocommerce' ); ?></label>
        <input type="text" class="input-text" name="billing_postcode" id="reg_billing_postcode" value="<?php esc_attr_e( $_POST['billing_postcode'] ); ?>" />
    </p>
    <div class="clear" style="margin-bottom:20px;"></div>
    <?php
}
add_action( 'woocommerce_register_form', 'wooc_extra_register_fields' );

/**
 * To validate WooCommerce registration form custom fields.
 */
function text_domain_woo_validate_reg_form_fields($username, $email, $validation_errors) {
    if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {
        $validation_errors->add('billing_first_name_error', __('<strong>Error</strong>: First name is required!'));
    }

    if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {
        $validation_errors->add('billing_last_name_error', __('<strong>Error</strong>: Last name is required!.'));
    }
    return $validation_errors;
}

add_action('woocommerce_register_post', 'text_domain_woo_validate_reg_form_fields', 10, 3);

/**
 * To save WooCommerce registration form custom fields.
 */
function text_domain_woo_save_reg_form_fields($customer_id) {
    //First name field
    if (isset($_POST['billing_first_name'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']));
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
    }
    //Last name field
    if (isset($_POST['billing_last_name'])) {
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']));
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
    }
    if (isset($_POST['billing_address_2'])) {
        update_user_meta($customer_id, 'billing_address_2', sanitize_text_field($_POST['billing_address_2']));
    }
    if (isset($_POST['billing_city'])) {
        update_user_meta($customer_id, 'billing_city', sanitize_text_field($_POST['billing_city']));
    }
    if (isset($_POST['billing_postcode'])) {
        update_user_meta($customer_id, 'billing_postcode', sanitize_text_field($_POST['billing_postcode']));
    }

}

add_action('woocommerce_created_customer', 'text_domain_woo_save_reg_form_fields');

add_filter( 'wp_nav_menu_items', 'my_nav_menu_profile_link');
function my_nav_menu_profile_link($menu) {

    $array = explode('<ul class="mega-sub-menu">', $menu);
    $count = 0;
    foreach ($array as $main_key => $line){
        $cell = explode('</ul>', $line);
        if(count($cell) > 1){

            $sub_menu_items = explode('</li>', $cell[0]);
            if(count($sub_menu_items) > 1) {
                if(count($sub_menu_items) >= 4) {
                    $count_in_column = ceil(count($sub_menu_items) / 4);
                }else{
                    $count_in_column = 1;
                }
                $sub_count = 0;
                foreach ($sub_menu_items as $key => $item) {
                    $sub_count++;
                    if($sub_count == 1 && !empty($item)){
                        if($key == 0){
                            $container = '<div class="col-sm-3 col">';
                        }else{
                            $container = '</div><div class="col-sm-3 col">';
                        }
                        $sub_menu_items[$key] = $container . $item;
                    }
                    if($sub_count == $count_in_column){
                        $sub_count = 0;
                    }
                }
                $result = implode('</li>', $sub_menu_items);
                $result .= '</div>';
                $cell[0] = $result;
            }

        }
        $count++;
        $array[$main_key] = implode('</div></ul>', $cell);
    }
    $menu = implode('<ul class="mega-sub-menu"><div class="wrap-sub-menu">', $array);

    return $menu;
}
