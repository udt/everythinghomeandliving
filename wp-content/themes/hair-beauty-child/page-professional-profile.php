<?php
/**
 * Template Name: Professional Profile
 *
 * Description: This is profile Page Template.
 */
 
	get_currentuserinfo();
 	$current_user_id = $current_user->ID;
	$current_page_url = get_permalink($post->ID);

	global $wp_query;
	if(isset($_GET['profile']))
		$requestedUserId = (int)$_GET['profile'];
		$user_id = $requestedUserId;
	
	if(is_user_logged_in()){
		if($requestedUserId > 0 && isset($_GET['add_to_favorite_professional'])){
							
			$user_id = $requestedUserId;
								
			$professional_favorite_id = array();
			if(get_user_meta($current_user_id, 'my_favorite_professional', true)){
				
				$current_users_favorite = get_user_meta($current_user_id, 'my_favorite_professional', true);
				
			} else{
				
				$current_users_favorite = array();
				
			}
			
			$professional_favorite_id[] = $user_id;
			$result = array_merge($current_users_favorite, $professional_favorite_id);
			
			if(!in_array( $user_id, $current_users_favorite )){
				
				update_user_meta($current_user_id, 'my_favorite_professional', $result);
				$params_updated = array( 'user_id' => $user_id, 'messageboardtype' => 'success',  'message' => 'Favorite updated.');
				$updated_message_url = add_query_arg( $params_updated, $current_page_url );
				header("Location: ".$updated_message_url);
				exit;
				
			}	
		}
		
		if($requestedUserId > 0 && isset($_GET['remove_to_favorite_professional'])){
							
			$user_id = $requestedUserId;
								
			$professional_favorite_id = array();
			if(get_user_meta($current_user_id, 'my_favorite_professional', true)){
				
				$current_users_favorite = get_user_meta($current_user_id, 'my_favorite_professional', true);
				
			} else{
				
				$current_users_favorite = array();
				
			}
			
			$professional_favorite_id[] = $user_id;
			$result = array_diff($current_users_favorite, $professional_favorite_id);
			
			if($result){
				
				update_user_meta($current_user_id, 'my_favorite_professional', $result);
				$params_updated = array( 'user_id' => $user_id, 'messageboardtype' => 'success',  'message' => 'Favorite deleted.');
				$updated_message_url = add_query_arg( $params_updated, $current_page_url );
				header("Location: ".$updated_message_url);
				exit;
				
			} else{
			 
			 delete_user_meta($current_user_id, 'my_favorite_professional');
			 $params_updated = array( 'user_id' => $user_id, 'messageboardtype' => 'success',  'message' => 'Favorite deleted.');
			 $updated_message_url = add_query_arg( $params_updated, $current_page_url );
			 header("Location: ".$updated_message_url);
			 exit;
			 
		 }	
		}
	}

	//get professional user role
	if($requestedUserId > 0){
		$professional_details = new WP_User( $requestedUserId );
		$this_professional_role = $professional_details->roles[0];	
		
		
		
		//review settings hidden value
		$reviewfor = $requestedUserId;
		$reviewtype = 'professional';
		$reviewby = $current_user_id;
		$review_status = 'approved';
		$review_count = 0;
		if(function_exists('count_rm_reviews')) 
			$review_count = count_rm_reviews( $reviewfor, $reviewtype, $review_status );		
		
	}
	
	$userifo = get_userdata( $requestedUserId );
	$profession = html_entity_decode(trim(mm_get_the_professional_value('profession', $requestedUserId)));
	$isForProducts = $profession == 'Beauty Products';
	
	//add custom meta title and description
	global $_ec_meta_title;
	$afterName = $isForProducts ? '' : ', '.mm_get_the_professional_value('city', $requestedUserId);
	$_ec_meta_title = strip_tags(get_the_author_meta( 'display_name', $requestedUserId )).$afterName.' - Everything Cosmetic';
	$description = get_the_author_meta( 'description', $requestedUserId );
	global $_ec_meta_description;
	$_ec_meta_description = substr(strip_tags($description), 0, 160);
	
	get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>
	<div class="row page-wrapper">
		<?php WpvTemplates::$in_page_wrapper = true; ?>

		<article id="post-<?php the_ID(); ?>" class="right-only page type-page status-publish hentry">
			<?php
			global $wpv_has_header_sidebars;
			if ( $wpv_has_header_sidebars ) {
				WpvTemplates::header_sidebars();
			}	?>
			<div class="page-content">
				<div class="container clearfix">
			        <?php get_template_part( 'banner-connect' ); ?>
			        <?php $user_id = $requestedUserId;?>
					<div id="profile_main_info">		    
		                <div class="profile">
		                    <div class="photo holdleft">
								<?php 								
								$pro_img_url = get_professional_profile_image_url(get_avatar( $user_id, 242 ));
								//check if it is local avater, uploaded by user
								if (strpos($pro_img_url,'/wp-content/')) {	?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/timthumb.php?src=<?php echo $pro_img_url; ?>&h=242&w=242" alt=""><?php 
								} else {
									//default avater
									echo get_avatar( $user_id, 141 );
								}
								?>
								
								<?php if(!$isForProducts){ ?>
								
									<?php 
									$professional_map_address .= mm_get_the_professional_value('address', $user_id); 
									$professional_map_address .= ', '.mm_get_the_professional_value('city', $user_id); 
									//$professional_display_name_share = $userifo->display_name;
									$current_page_url = ecos_get_curPageURL();
									//$professional_description_share = strip_tags(get_the_author_meta( 'description', $user_id ));
									
									} ?>						
		                    </div> <!-- photo -->

	                    	<?php echo wpautop( $description ); ?>		                    

		                </div><!-- profile -->
		                <?php if(!$isForProducts){ ?>
		                	<?php if(get_the_author_meta( 'my_map_location', $user_id )) {?>
								<p class="map">
									<iframe id="existing_map" width='740' height='320' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0' src='http://maps.google.com/maps?q=<?php echo urlencode(get_the_author_meta( 'my_map_location', $user_id )); ?>&amp;output=embed'></iframe>
								</p>
								<a href="http://maps.google.com/maps?q=<?php echo urlencode(get_the_author_meta( 'my_map_location', $user_id )); ?>&amp;output=embed" class="loadmapiframe">Large View</a>	
							<?php } else {?>						
								<p class="map">
									<iframe width='240' height='120' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0' src='http://maps.google.com/maps?q=<?php echo urlencode($professional_map_address); ?>&amp;output=embed'>									
									</iframe>
									<a href="http://maps.google.com/maps?q=<?php echo urlencode($professional_map_address); ?>&amp;output=embed" class="loadmapiframe"></a>	
							<?php } ?>	
						<?php } ?>
		 
		                <?php 
						$current_services = pro_get_professional_services(mm_get_the_professional_value('id', $user_id));
						//var_dump(mm_get_the_professional_value('id', $user_id));
						if(is_array($current_services) && count($current_services) > 0){
							$categorized_services = array();
							foreach($current_services as $service){
								if(!isset($categorized_services[$service["category"]]))
									$categorized_services[$service["category"]] = array();
								
								$categorized_services[$service["category"]][] = $service;
							}
						?>
						<?php if(!$isForProducts){ 
						$hasHeader = false;	
						?>
						<?php for($vanue_c = 1; $vanue_c <= 6; $vanue_c++){
						
							if(get_the_author_meta( 'my_venue_image_'.$vanue_c, $user_id ) && get_the_author_meta( 'my_vanu_'.$vanue_c.'_name', $user_id ) && get_the_author_meta( 'my_vanu_'.$vanue_c.'_address', $user_id )){
							if(!$hasHeader)	$hasHeader = true;	?>
								<h2 class="subtitle-profile">Venues I Work from</h2>
								<ul class="venues">
							<?php }	?>
								<li>
									<a href="javascript:void(0)">
										<p class="photo">
											<?php 								
												$venue_img_url = get_the_author_meta( 'my_venue_image_'.$vanue_c, $user_id );
												//check if already exist
												if ($venue_img_url) {
													?><img src="<?php echo get_stylesheet_directory_uri(); ?>/timthumb.php?src=<?php echo $venue_img_url; ?>&h=92&w=92" alt=""><?php 
												}else{
													//default avatar
													?><img src="<?php echo get_stylesheet_directory_uri() ?>/photos/venue.jpg" alt="" /><?php
												}
											?>
										</p>
										<div class="info">
											<h3><?php echo get_the_author_meta( 'my_vanu_'.$vanue_c.'_name', $user_id ); ?></h3>
											<p><?php echo get_the_author_meta( 'my_vanu_'.$vanue_c.'_address', $user_id ); ?></p>
										</div>
									</a>
								</li> 
								
								<?php } ?>
							<?php } 
							if($hasHeader) {
							?> 
		                    </ul>
							<?php } ?>
		            
		                <?php 
						if($requestedUserId > 0){

						$args_my_deal = array(
							'post_type' => 'voucher',
							'meta_key' => 'voucher_user_id',
							'orderby' => 'meta_value_num',
							'posts_per_page' => 6,
							'caller_get_posts' => 1,
							'order' => 'DESC',
							'order_by' => 'ID',
							'paged' => $paged,
							'meta_query' => array(
							   array(
								   'key' => 'voucher_user_id',
								   'value' => $user_id,
								   'compare' => '=',
							   )
							)
						);
			 
				 		$query = query_posts($args_my_deal);
						if($query){
						while ( have_posts() ) : the_post();?>

						<?php 				
								
							if ( has_post_thumbnail() )
								$offer_pic = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); 
							else
								$offer_pic = get_stylesheet_directory_uri().'/photos/offer.jpg';						
								
							$deal_id = get_the_ID();		
							$deal_page_url = get_permalink(54);
							$params_deal = array( 'deal_id' => $deal_id);
							$deal_detail_page_url = add_query_arg( $params_deal, $deal_page_url );							
						?>			
					
						<div class="item-offer">
			                <h2>
			                	<a href="<?php echo $deal_detail_page_url; ?>"><strong><?php the_title();?></strong></a> <?php echo get_post_meta(get_the_ID(), 'voucher_complete_address' , true);?>
			                </h2>
			                <p class="photo">
			                	<img src="<?php echo get_stylesheet_directory_uri(); ?>/timthumb.php?src=<?php echo $offer_pic; ?>" alt="">
			                	<a href="<?php echo $deal_detail_page_url; ?>">See venue info &gt;&gt;</a>
			                </p>
			                <div class="info">
			                    <ul>
			                        <li class="price-discount">
			                            <span class="price">&pound; <?php echo get_post_meta(get_the_ID(), 'pay_amount' , true);?></span>
			                            <span class="discount">Discount <strong><?php echo get_post_meta(get_the_ID(), 'discount_amount' , true);?></strong></span>
			                        </li>
			                        <li class="expiry">
			                            <span></span>Offer Expires: <strong>
										<?php $date_dis = get_post_meta(get_the_ID(), 'expire_date' , true);
										$newDate = date("d M Y", strtotime($date_dis));
										echo $newDate;
										?>
										</strong>
			                        </li>
			                        <li class="view-offer">
			                            <a href="<?php echo $deal_detail_page_url; ?>">View Offer</a>
			                        </li>
			                    </ul>
			                </div>
			            </div>
						
			            <?php endwhile;
						if (function_exists("pagination")) { pagination($wp_query->max_num_pages, $page_deal); } 
						wp_reset_query();
						} else {
							//echo 'No Special Offers At The Moment Check Back Soon';
						}
							}
							else
							{
								echo 'Not available';
							}
						?>

						<?php						
		             
						$the_query = new WP_Query( 
						  array( 
						  'posts_per_page' => '100', 
						  'meta_key' => 'UserID',
						  'meta_value' => $user_id,
						  'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1) 
						) );
						
						while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<div class="btb_conent_box">
							<h2><?php the_title(); ?></h2>
							<h2><?php the_subtitle(); ?></h2>
						
							<a href="<?php the_permalink() ?>">
							<span>
							<?php the_post_thumbnail('medium'); ?>
							</span>
							</a>
							<?php the_excerpt( ); ?>
						</div>						
						
						<?php endwhile; 
						wp_reset_postdata();?>

		                <?php
							if($requestedUserId > 0){
							$args_current = array(
								'post_type' => 'beforeandafter',
								'meta_key' => 'before_after_user_id',
								'orderby' => 'meta_value_num',
								'posts_per_page' => -1,
								'order' => 'ASC',
								'orderby' => 'ID',
								'meta_query' => array(
								   array(
									   'key' => 'before_after_user_id',
									   'value' => $user_id,
									   'compare' => '=',
								   )
								)
							);

						$query_current_images = new WP_Query( $args_current );
						$counter = 1;
						// The Loop
						while ( $query_current_images->have_posts() ) :
							$query_current_images->the_post();
							
							$block_class = '';
							if(($counter % 2) == 0)
								$block_class = 'right_block';
							?>
		                     <div class="before-after <?php echo $block_class; ?>">
		                     <a href="#">
		                            <p class="photo"><?php if ( has_post_thumbnail() ) { $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); } ?>
		                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/timthumb.php?src=<?php echo $image[0]; ?>&w=349" alt="">
		                            </p>
		                            <h2><?php echo get_the_title(); ?></h2>
		                        </a>
		                      </div>
							<?php
						$counter++;	
						endwhile;
					wp_reset_postdata();
					}
							if($requestedUserId > 0){
							$args_my_photo_video = array(
								'post_type' => 'additionalimage',
								'meta_key' => 'additional_images_user_id',
								'orderby' => 'meta_value_num',
								'posts_per_page' => -1,
								'order' => 'ASC',
								'orderby' => 'ID',
								'meta_query' => array(
								   array(
									   'key' => 'additional_images_user_id',
									   'value' => $user_id,
									   'compare' => '=',
								   )
								)
							);

						$query_current_photovideo = new WP_Query( $args_my_photo_video );
						$counter = 1;
						// The Loop
						while ( $query_current_photovideo->have_posts() ) :
							$query_current_photovideo->the_post();
							
							$block_class = '';
							if(($counter % 2) == 0)
								$block_class = 'right_block';
							?>
		                     <div class="before-after <?php echo $block_class; ?>">
		                     <a href="#">
		                            <p class="photo"><?php if ( has_post_thumbnail() ) { $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); } ?>
		                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/timthumb.php?src=<?php echo $image[0]; ?>&w=349" alt="">
		                            </p>
		                            <h2><?php echo get_the_title(); ?></h2>
		                        </a>
		                      </div>
							<?php
						$counter++;	
						endwhile;

						$instagram_id = get_the_author_meta('instagram', $user_id);
						if ($instagram_id) {
							echo do_shortcode('[instagram-feed id="' . $instagram_id . '"]');
						}
						wp_reset_postdata();
					}
					
					?>            
		        </div>
		    </div>
		</div>

		<?php //comments_template( '', true ); ?>
        <div class="info">
          
			<div class="row ">
				<div class="wpv-grid grid-1-1  wpv-first-level grid-1-1 first unextended has-extended-padding" style="padding-top:0.05px;padding-bottom:0.05px;" id="wpv-column-9339de71c523b43d99f030f0954d9ffc">
					<div class="row ">
						<?php if(get_the_author_meta( 'facebook', $user_id ) != ''){ ?>
							<div class="wpv-grid grid-1-3  grid-1-3 first unextended has-extended-padding" style="padding-top:0.05px;padding-bottom:0.05px;" id="wpv-column-31c33036d3224a23bde1b913191a6678">
								<div class="row ">
									<div class="wpv-grid grid-1-1  grid-1-1 first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top:0.05px;padding-bottom:0.05px;" id="wpv-column-de04ba7a9e31b20b75143c2a237a04cc">
										<a class="post_facebook" title="Facebook" href="<?php echo get_the_author_meta( 'facebook', $user_id ); ?>" target="_blank">
											<div class="first">
												<span class="icon shortcode   use-hover" style="font-size:40px !important;"></span>
											</div>
											<div class="last">
												<h4 style="text-align: center;">Follow us on Facebook</h4>
											</div>
										</a>
									</div>
								</div>
							</div>
						<?php } ?>
						<?php if(get_the_author_meta( 'twitter', $user_id ) != ''){ ?>
							<div class="wpv-grid grid-1-3  grid-1-3 unextended has-extended-padding" style="padding-top:0.05px;padding-bottom:0.05px;" id="wpv-column-785fb0ce56b9c819a87bfbc68523a16e">
								<div class="row ">
									<div class="wpv-grid grid-1-1  grid-1-1 first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top:0.05px;padding-bottom:0.05px;" id="wpv-column-b6f38aa431169d6d81a14f3936d49853">
										<a class="post_twitter" title="Twitter" href="https://twitter.com/<?php echo get_the_author_meta( 'twitter', $user_id ); ?>" target="_blank">
											<div class="first">
												<span class="icon shortcode theme  use-hover" style="font-size:40px !important;"></span>
											</div>
											<div class="last">
												<h4 style="text-align: center;">Follow us on&nbsp;Twitter</h4>
											</div>
										</a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>                    									
		</div> <!-- info -->
		</article>
		<aside class="right">
			<section id="text-26" class="widget widget_text">
				<h4 class="subtitle-profile">Price:</h4>
				<div class="textwidget">
					<table class="vamtam-styled">
						<tbody>
							<?php
							foreach($categorized_services as $category => $cat_services){
							$index = 0;
							foreach ($cat_services as $service){ ?>     
								<tr>
									<td>
										<p class="p1"><?php echo $service["service"]; ?></p>
									</td>
									<td>
										<p class="p1" style="text-align: right;"><strong>&pound;<?php echo $service["price"]; ?></strong></p>
									</td>
								</tr>                 
							<?php $index++; }} ?>   
						</tbody>
					</table>
				<?php } ?>
				</div>
			</section>
			<section id="text-27" class="widget widget_text">						
				<h4 class="widget-title">CONTACTS</h4>
				<div class="textwidget">
					<h4>Address</h4>
                    <p><?php echo mm_get_full_formatted_professional_address($user_id);?></p>
                    <h4>Contact Number</h4>
                    <p><?php echo mm_get_the_professional_value('phone_home', $user_id);?><br />
					<?php echo mm_get_the_professional_value('phone_office', $user_id);?></p>
					<h4>Website</h4>
                    <p><a target="_blank" href="<?php echo get_the_author_meta( 'user_url', $user_id ); ?>"><?php echo str_replace(array("http://", "https://"), "", get_the_author_meta( 'user_url', $user_id )); ?></a></p>
                    <h4>Profession</h4>
                    <p><?php echo mm_get_the_professional_value('profession', $user_id);?></p>
                    <?php if(!$isForProducts){ ?>
	                    <h4>Opening Times</h4>
	                    <p><?php echo nl2br(get_the_author_meta( 'pro_opening_time', $user_id )); ?></p>
	                    <h4>Year Qualified</h4>
	                    <p><?php echo get_the_author_meta( 'year_quality', $user_id ); ?></p>
					<?php } ?>
				</div>
			</section>
			<section class="widget widget_text links-profiles">
											
					<?php
						$current_page_url = ecos_get_curPageURL();
						$params = array( 'current_tab' => 'tab_reviews');
						$redirect_tab_url = add_query_arg( $params, $current_page_url );
					if($this_professional_role == 'member_gold'){ ?>
						<div class="mail last-button">
							<a class="button" href="<?php echo get_stylesheet_directory_uri() ?>/popup-contents/email-to-consultant.php?profile_id=<?php echo $user_id; ?>"
							>Contact Me</a>
						</div>
					<?php } 
					if(is_user_logged_in()){
						
						$params_professional_favorite = array('user_id' => $user_id, 'add_to_favorite_professional' =>'yes');
						$link_professional_mark_as_favorite_url = add_query_arg( $params_professional_favorite, $current_page_url );
						
						$params_professional_favorite_remove = array('user_id' => $user_id, 'remove_to_favorite_professional' =>'yes');
						$link_professional_unmark_as_favorite_url = add_query_arg( $params_professional_favorite_remove, $current_page_url );
						
						if(get_user_meta($current_user_id, 'my_favorite_professional', true)){
							
							$current_users_favorite = get_user_meta($current_user_id, 'my_favorite_professional', true);
							
							if(in_array($requestedUserId, $current_users_favorite)){ ?>
							
								<div>
									<a class="button" href="<?php echo $link_professional_unmark_as_favorite_url; ?>">My favorite</a>
								</div>
								
							<?php
							} else{ ?>
							
								<div>
									<a class="button" href="<?php echo $link_professional_mark_as_favorite_url; ?>">Mark as favorite</a>
								</div>
								
							<?php
							}
						} else{ ?>										
							<div>
								<a class="button" href="<?php echo $link_professional_mark_as_favorite_url; ?>" >Mark as favorite</a>
							</div>
							
						<?php
						}
					}
					?>
					<?php if($this_professional_role == '' && !$isForProducts){ ?>
						<div class="got-consultation" >
							<a class="button" href="<?php echo get_stylesheet_directory_uri() ?>/popup-contents/email-to-consultant.php?profile_id=<?php echo $user_id; ?>"></a>
						</div>
                	<?php } ?>									
                </ul>            
            </section><!-- links profiles -->
            <?php	echo do_shortcode('[rm_review_form review_for='.$reviewfor.' review_type='.$reviewtype.' review_by='.$reviewby.'][/rm_review_form]');
					?>
		</aside>

	</div>
<?php endif;

get_footer(); ?>